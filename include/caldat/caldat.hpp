#ifndef CALDAT_HPP
#define CALDAT_HPP
#include <ctime>
#include <ostream>

class Date
{
	public:
	/**
	 * @brief Constructor
	 */
	Date();
	
	/**
	 * @brief Constructor
	 * @param[in] time Number of seconds since 1970-01-01
	 */
	Date(const time_t time);
	
	/**
	 * @brief Constructor
	 * @param[in] ptm pointer of time structure (difined in ctime)
	 */
	Date(const tm* ptm);
	
	/**
	 * @brief Constructor
	 * @param[in] julday julian day
	 */
	Date(const double julday);
	
	/**
	 * @brief Constructor
	 */
	Date(const int year, const int month, const int day, const int hour=0, const int minute=0, const double second=0.);
	
	/**
	 * @brief Returns the year.
	 * @return the year.
	 */
	const int& year() const;
	
	/**
	 * @brief Returns the month.
	 * @return the month.
	 */
	const int& month() const;
	
	/**
	 * @brief Returns the day.
	 * @return the day.
	 */
	const int& day() const;
	
	/**
	 * @brief Returns the hour.
	 * @return the hour.
	 */
	const int& hour() const;
	
	/**
	 * @brief Returns the minute.
	 * @return the minute.
	 */
	const int& minute() const;
	
	/**
	 * @brief Returns the second.
	 * @return the second.
	 */
	const double& second() const;
	
	/**
	 * @brief Returns the julian day.
	 * @return the julian day.
	 */
	const double& julday() const;
	
	/**
	 * @brief Returns the number of days spent since 1st of January of the current year at 00:00.
	 * 
	 * ex: For 7th of January at 18:00, it will return 7.75.
	 * @return the day of the year.
	 */
	double yday() const;
	
	/**
	 * @brief Returns the decimal hour.
	 * @return the decimal hour.
	 */
	double dec_hour() const;
	
	/**
	 * @brief Returns the Date at the given the longitude.
	 * @param lon longitude of the point where you want to have the local hour.
	 * @param current longitude of the point where you currently are (default is UTC)
	 * @return the decimal hour.
	 */
	Date local(const double lon, const double current = 0.) const;
	
	/**
	 * @brief Returns the numbers of seconds since January 1, 1970.
	 * @param[out] time Pointer to a time_t object where the value will be stored if not NULL.
	 * @return the numbers of seconds since January 1, 1970.
	 */
	time_t time(time_t* time = NULL) const;
	
	/**
	 * @brief Returns the julian day of the given date.
	 * @return the julian day of the given date.
	 */
	static double julday(int year, int month, int day, int hour=0, int minute=0, double second=0.);
	
	/**
	 * @brief Returns the date given the julian day.
	 * @return  the date given the julian day.
	 */
	static void caldat(double julian, int* year, int* month, int* day=NULL, int* hour=NULL, int* minute=NULL, double* second=NULL);
	
	Date operator+(const Date& date) const;
	Date operator-(const Date& date) const;
	Date operator+(const double day) const;
	Date operator-(const double day) const;
	bool operator==(const Date& date) const;
	bool operator!=(const Date& date) const;
	bool operator>(const Date& date) const;
	bool operator<(const Date& date) const;
	bool operator>=(const Date& date) const;
	bool operator<=(const Date& date) const;
	Date& operator+=(const Date& date);
	Date& operator-=(const Date& date);
	Date& operator+=(const double day);
	Date& operator-=(const double day);
	
	operator const double&() const; //!< Cast operator.
	operator time_t() const; //!< Cast operator.
	
	protected:
	double m_julday; //!< Julian day.
	
	int m_year; //!< Year.
	int m_month; //!< Month.
	int m_day; //!< Day.
	int m_hour; //!< Hour.
	int m_minute; //!< Minute.
	double m_second; //!< Second.
	
};

namespace std
{
	std::ostream& operator<<(std::ostream& out, const Date& date); //!< std::ostream::operator<<() overload to print Date.
}

#endif
