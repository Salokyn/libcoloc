#ifndef NAVIGATIONGEO_HPP
#define NAVIGATIONGEO_HPP

#include <geo/geo.hpp>

#define NAVCONV 1./65536.

/**
 * @brief Regroups NavigationGeo classes.
 */
namespace navgeo
{
	const double geoAltitude = 6.610739*geo::req_earth; //!< Altitude of geostationnary satellites.
	
	/**
	 * @brief NavigationGeo class
	 * 
	 * Functions geo2pix and pix2geo are taken from A.S.M Gieske et al. "PROCESSING OF MSG-1 SEVIRI DATA IN THE THERMAL INFRARED - ALGORITHM DEVELOPMENT WITH THE USE OF THE SPARC2004 DATA SET".
	 * 
	 * Scaling coefficients are provided by ICARE.
	 */
	class NavigationGeo
	{
		public:
		/**
		 * @brief Constructor
		 * @param[in] cfac Scaling coefficient in rad-1
		 * @param[in] rfac Scaling coefficient in rad-1
		 * @param[in] coff Column offset
		 * @param[in] roff Row offset
		 * @param[in] ssp Sub-satellite point longitude
		 * @param[in] rotated Tells if the image is rotated or not
		 */
		NavigationGeo(const long cfac, const long rfac, const long coff, const long roff, const double ssp, const bool rotated=false);
		
		/**
		 * @brief Calculates the column and the row of a point in a Geostationary image given its Coordinate.
		 * @param[in] coord Coordinate of the point.
		 * @param[out] col Column of the point in the image.
		 * @param[out] row Row of the point in the image.
		 */
		void geo2pix(const Coordinate& coord, size_t& col, size_t& row) const;
		
		/**
		 * @brief Calculates the Coordinate of a point given its row and column in a Geostationary image.
		 * @param[in] col Column of the point in the image.
		 * @param[in] row Row of the point in the image.
		 * @return Coordinate of the point.
		 */
		Coordinate pix2geo(const int col, const int row) const;
		
		/**
		 * @brief Returns the sub-satellite point longitude.
		 * @param[in] u Unit of the longitude (default: geo::DEGREE)
		 * @return the sub-satellite point longitude.
		 */
		double ssp(const geo::unit_angle u=geo::DEGREE) const;
		
		/**
		 * @brief Sets the sub-satellite point longitude.
		 * @param[in] ssp Sub-satellite point longitude.
		 * @param[in] u Unit of the longitude (default: geo::DEGREE)
		 */
		void setSsp(double ssp, const geo::unit_angle u=geo::DEGREE);
		
		protected:
		long m_cfac; //!< Scaling coefficient in rad-1
		long m_rfac; //!< Scaling coefficient in rad-1
		long m_coff; //!< Column offset
		long m_roff; //!< Row offset
		double m_ssp; //!< Sub-satellite point longitude in degrees
		bool m_rotated; //!< Tells if the image is rotated or not
	};
	
	class SEVIRI : public NavigationGeo
	{
		public:
		SEVIRI(const double ssp = default_ssp, bool rotated=false); //!< Constructor
		
		static constexpr long default_cfac = 781648343; //!< Default scaling coefficient in rad-1
		static constexpr long default_rfac = 781648343; //!< Default scaling coefficient in rad-1
		static constexpr long default_coff = 1856; //!< Default column offset
		static constexpr long default_roff = 1856; //!< Default row offset
		static constexpr double default_ssp = 0.; //!< Default sub-satellite point longitude
	};
	
	class GOES : public NavigationGeo
	{
		public:
		GOES(const double ssp, const bool rotated=false); //!< Constructor
		
		static constexpr long default_cfac = 585142831; //!< Default scaling coefficient in rad-1
		static constexpr long default_rfac = 585142831; //!< Default scaling coefficient in rad-1
		static constexpr long default_coff = 1403; //!< Default column offset
		static constexpr long default_roff = 1403; //!< Default row offset
	};
	
	class GOES10 : public GOES
	{
		public:
		
		GOES10(const bool rotated=false); //!< Constructor
		
		static constexpr double default_ssp = -60.; //!< Default sub-satellite point longitude
	};
	
	class GOES11 : public GOES
	{
		public:
		
		GOES11(const bool rotated=false); //!< Constructor
		
		static constexpr double default_ssp = -135.; //!< Default sub-satellite point longitude
	};
	
	class GOES12 : public GOES
	{
		public:
		
		GOES12(const bool rotated=false); //!< Constructor
		
		static constexpr double default_ssp = -75.; //!< Default sub-satellite point longitude
	};
	
	class MTSAT : public NavigationGeo
	{
		public:
		MTSAT(const bool rotated=false); //!< Constructor
		
		static constexpr long default_cfac = 586315046; //!< Default scaling coefficient in rad-1
		static constexpr long default_rfac = 586315046; //!< Default scaling coefficient in rad-1
		static constexpr long default_coff = 1375; //!< Default column offset
		static constexpr long default_roff = 1375; //!< Default row offset
		static constexpr double default_ssp = 140.; //!< Default sub-satellite point longitude
	};
	
	class MVIRI : public NavigationGeo
	{
		public:
		MVIRI(const double ssp = default_ssp, const bool rotated=false); //!< Constructor
		
		static constexpr long default_cfac = 521518905; //!< Default scaling coefficient in rad-1
		static constexpr long default_rfac = 521518905; //!< Default scaling coefficient in rad-1
		static constexpr long default_coff = 1250; //!< Default column offset
		static constexpr long default_roff = 1250; //!< Default row offset
		static constexpr double default_ssp = 57.; //!< Default sub-satellite point longitude
	};
	
	class FY2C : public NavigationGeo
	{
		public:
		FY2C(const bool rotated=false); //!< Constructor
		
		static constexpr long default_cfac = 468114253; //!< Default scaling coefficient in rad-1
		static constexpr long default_rfac = 468114253; //!< Default scaling coefficient in rad-1
		static constexpr long default_coff = 1144; //!< Default column offset
		static constexpr long default_roff = 1144; //!< Default row offset
		static constexpr double default_ssp = 104.5; //!< Default sub-satellite point longitude
	};
}

#endif
