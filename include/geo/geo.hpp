#ifndef GEO_HPP
#define GEO_HPP

#include <cstdlib>
#include <ostream>
#include <caldat/caldat.hpp>

#define M_RADEG (180./M_PI)

namespace geo
{
	/**
	 * @brief Unit angle
	 */
	enum unit_angle { DEGREE, RADIAN };
	
	static const double req_earth = 6378136.500; //!< Earth equatorial radius.
	static const double rpol_earth = 6356751.847; //!< Earth polar radius.
	static const double r_earth = 6371000.300; //!< Volumetric radius of the Earth. \f[ r_{earth} = (req_{earth}^2 \cdot rpol_{earth})^{(1/3)} \f]
	static const double f_earth = 1.-rpol_earth/req_earth; //!< Flattening of the Earth. \f[ f_{earth} = 1-\frac{rpol_{earth}}{req_{earth}} \f]
}

/**
 * @brief Coordinate class.
 */
class Coordinate
{
	public:
	// Constructeurs
	Coordinate(); //!< Default constructor.
	/**
	 * @brief Constructor
	 * 
	 * Create a Coordinate with given longitude and latitude.
	 * Longitude is automatically set in [-180°; +180°].
	 * Latitude is automatically limited in [+90°; -90°].
	 * @param[in] lon is the longitude.
	 * @param[in] lat is the latitude.
	 * @param[in] u is the unit of the angle.
	 */
	Coordinate(const double lon, const double lat, const geo::unit_angle u = geo::DEGREE);
	
	~Coordinate(); //!< Destructor.
	
	// Getteurs
	/**
	 * @brief Returns longitude
	 * 
	 * Longitude is given in [-180°; +180°]
	 * @param[in] u is the unit of the angle
	 * @return the longitude of the Coordinate.
	 */
	double lon(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns longitude
	 * 
	 * Longitude is given in [-180°; +180°]
	 * @param[in] u is the unit of the angle
	 * @return the longitude of the Coordinate.
	 */
	double relativeLon(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns longitude
	 * 
	 * Longitude is given in [+0°; +360°]
	 * @param[in] u is the unit of the angle
	 * @return the longitude of the Coordinate.
	 */
	double absoluteLon(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns latitude
	 * 
	 * Latitude is given in [+90°; -90°]
	 * @param[in] u is the unit of the angle
	 * @return the longitude of the Coordinate.
	 */
	double lat(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns co-latitude
	 * 
	 * Co-latitude is given in [+0°; +180°]
	 * @param[in] u is the unit of the angle
	 * @return the longitude of the Coordinate.
	 */
	double colat(const geo::unit_angle u = geo::DEGREE) const;
	
	// Setteurs
	/**
	 * @brief Sets longitude.
	 * 
	 * Longitude is automatically set in [-180°; +180°].
	 * @param[in] lon is the longitude.
	 * @param[in] u is the unit of the angle.
	 */
	void set_lon(double lon, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Sets latitude.
	 * 
	 * Latitude is automatically limited in [+90°; -90°].
	 * @param[in] lat is the latitude.
	 * @param[in] u is the unit of the angle.
	 */
	void set_lat(double lat, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Sets colatitude
	 * 
	 * Colatitude is converted in latitude, then it is automatically limited in [+90°; -90°].
	 * @param[in] colat is the colatitude.
	 * @param[in] u is the unit of the angle.
	 */
	void set_colat(const double colat, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Sets longitude and latitude.
	 * 
	 * Longitude is automatically set in [-180°; +180°].
	 * Latitude is automatically limited in [+90°; -90°].
	 * @param[in] lon is the longitude.
	 * @param[in] lat is the latitude.
	 * @param[in] u is the unit of the angle.
	 */
	void set(const double lon, const double lat, const geo::unit_angle u = geo::DEGREE);
	
	// Opérateurs
	/**
	 * @brief Adds longitude and latitude.
	 * 
	 * @param[in] coord is the Coordinate to be added.
	 * @return the sum of two Coordinates.
	 */
	Coordinate operator+(const Coordinate& coord) const;
	
	/**
	 * @brief Substracts longitude and latitude.
	 * 
	 * @param[in] coord is the Coordinate to be substracted.
	 * @return the difference between two Coordinates.
	 */
	Coordinate operator-(const Coordinate& coord) const;
	
	/**
	 * @brief Adds longitude and latitude.
	 * 
	 * @param[in] coord is the Coordinate to be added.
	 * @return the sum of two Coordinates.
	 */
	Coordinate& operator+=(const Coordinate& coord);
	
	/**
	 * @brief Substracts longitude and latitude.
	 * 
	 * @param[in] coord is the Coordinate to be substracted.
	 * @return the difference between two Coordinates.
	 */
	Coordinate& operator-=(const Coordinate& coord);
	
	/**
	 * @brief Compares two Coordinates.
	 * 
	 * @param[in] coord Coordinate to be compared.
	 * @return true if longitudes and latitudes are the same.
	 */
	bool operator==(const Coordinate& coord) const;
	
	/**
	 * @brief Compares two Coordinates.
	 * 
	 * @param[in] coord Coordinate to be compared.
	 * @return true if longitudes or latitudes are different.
	 */
	bool operator!=(const Coordinate& coord) const;
	
	protected:
	// Attributs
	double m_lon; //!< longitude in degrees.
	double m_lat; //!< latitude in degrees.
};

/**
 * The main part of the equations in this namespace are taken from Michel Capderou's book "Satellites, Orbits and missions".
 * 
 * In the following documention, as in the cited book, we will use the following letters :
 * 
 * \f$ \lambda \f$ : longitude
 * 
 * \f$ \theta \f$ : latitude
 * 
 * \f$ \alpha \f$ : angle at the center of the earth
 * 
 * <var>f</var> : swath angle
 * 
 * \f$ \zeta \f$ : zenithal angle
 * 
 * \f$ \beta \f$ : azimuth angle
 * 
 * "S" suffix is added to values concerning sun.
 * 
 * @image html zen.png "Zenithal angles"
 * @image html azi.png "Azimuthal angles"
 */
namespace geo
{
	static const unit_angle default_unit = DEGREE; //!< Default unit angle.
	
	/**
	 * @brief Calculates the cosinus of the angle at the center of the Earth between two points.
	 * 
	 * This value is calulated from the longitude (\f$ \lambda \f$) and the latitude (\f$ \phi \f$) of the two points :
	 * \f[ \cos\alpha = \sin\phi_A \cdot \sin\phi_B + \cos\phi_A \cdot \cos\phi_B \cdot \cos(\lambda_A-\lambda_B) \f]
	 * 
	 * @param[in] coord0 Coordinate of the observer.
	 * @param[in] coord1 Coordinate of the observed point.
	 * @return cosinus of the angle at the center of the Earth.
	 */
	double cosalpha(const Coordinate& coord0, const Coordinate& coord1);
	
	/**
	 * @brief Calculates the distortion factors of a pixel.
	 * 
	 * According to the swath angle (<var>f</var>) and the relative altitude (\f$ \eta \f$) of the satellite, the footprint size of a pixel may increase.
	 * This method calculate the distortion factors in the along-viewing-azimuth (<var>K</var>) and then cross-viewing-azimuth (<var>L</var>) directions.
	 *
	 * \f[ K =  \frac{1}{\eta -1} \cdot \frac{\eta^2-2 \eta \cos \alpha + 1}{\eta \cos \alpha -1} \f]
	 *
	 * \f[ L = \frac{1}{\eta -1} \cdot \sqrt{\eta^2-2 \eta \cos \alpha + 1} \f]
	 * 
	 * @param[in] swath swath angle in radians.
	 * @param[in] eta relative altitude of the satellite.
	 * @param[out] K along-scan distortion factor.
	 * @param[out] L cross-scan distortion factor.
	 */
	void distortion(const double swath, const double eta, double& K, double& L);
	
	/**
	 * @brief Calculates the along-swath distortion factor <var>K</var> of a pixel.
	 * 
	 * \f[ K =  \frac{1}{\eta -1} \cdot \frac{\eta^2-2 \eta \cos \alpha + 1}{\eta \cos \alpha -1} \f]
	 * 
	 * @param[in] cosalpha cosine of the angle at the center of the earth.
	 * @param[in] eta relative altitude of the satellite.
	 * @return the along-swath distortion factor.
	 */
	double distortionK(const double cosalpha, const double eta);
	
	/**
	 * @brief Calculates the cross-swath distortion factor <var>L</var> of a pixel.
	 * 
	 * \f[ L = \frac{1}{\eta -1} \cdot \sqrt{\eta^2-2 \eta \cos \alpha + 1} \f]
	 * 
	 * @param[in] cosalpha cosine of the angle at the center of the earth.
	 * @param[in] eta relative altitude of the satellite.
	 * @return the cross-swath distortion factor.
	 */
	double distortionL(const double cosalpha, const double eta);
	
	/**
	 * @brief Calculates the distance and azimuth between two points on earth.
	 * 
	 * Calculates the distance (<var> d </var>) and azimuth (\f$ \beta \f$) between two points of Coordinate (\f$ \lambda_A \f$, \f$ \phi_A \f$) and (\f$ \lambda_B \f$, \f$ \phi_B \f$) respectively.
	 * By default, the distance is given for planet earth. The azimuth is given from North in the trigo direction.
	 * If the two points have the same Coordinate or are antipodal, the azimuth is indertermined, the arbitrary value 0. is returned.
	 * 
	 * \f[ \cos \alpha = \sin \phi_A \cdot \sin \phi_B + \cos \phi_A \cdot \cos \phi_B \cdot \cos(\lambda_A - \lambda_B) \f]
	 * \f[ d = r_{earth} \cdot \alpha \f]
	 * \f[ \tan \beta = \frac{\sin(\lambda_A - \lambda_B) \cdot \cos \phi_B}{\cos \phi_A \cdot \sin \phi_B - \sin \phi_A \cdot \cos \phi_B \cdot \cos(\lambda_A - \lambda_B)} \f]
	 * 
	 * @param[in] coord0 Coordinate of the origin point.
	 * @param[in] coord1 Coordinate of the observed point.
	 * @param[out] azi pointer of double to get the azimuth angle between points in radians.
	 * @param[in] u azimuth angle unit.
	 * @param[in] radius of the planet (default is earth).
	 * @return the distance between points on earth
	 */
	double map_2points(const Coordinate& coord0, const Coordinate& coord1, double* azi=NULL, const unit_angle u=default_unit, const double radius=r_earth);
	
	/**
	 * @brief Calculates the distance and azimuth between two points on earth.
	 * 
	 * Calculates the distance and azimuth between two points of coordinate (lon0, lat0) and (lon1, lat1) respectively.
	 * By default, the distance is given for planet earth.
	 * @see map_2points(const Coordinate& coord0, const Coordinate& coord1, double* azi=NULL);
	 */
	double map_2points(const double lon0, const double lat0, const double lon1, const double lat1, double* azi=NULL, const unit_angle u=default_unit, const double radius=r_earth);
	
	/**
	 * @brief Rotate a point.
	 *
	 * Rotate a point (xin; yin) with angle and returns (xout; yout). Rotation center is (0;0)
	 * 
	 * @param[in] xin x composant of the point coordinate.
	 * @param[in] yin y composant of the point coordinate.
	 * @param[in] angle Angle of rotation in radians.
	 * @param[out] xout x composant of the rotated point coordinate.
	 * @param[out] yout y composant of the rotated point coordinate.
	 */
	void rotate(const double xin, const double yin, const double angle, double& xout, double& yout);
	
	/**
	 * @brief Calculates the zenithal angle between a point and a satellite.
	 * 
	 * Calculates the zenithal angle (\f$ \zeta \f$), between a point at Coordinate coord0 and a satellite at coord1 and at relative altitude (\f$ \eta \f$).
	 * If satellite is over the point, the zenith angle is 0. If the point and the satellite are antipodal, the zenith angle is \f$ \pi \f$.
	 * 
	 * \f[ \tan\zeta = \frac{\sin\alpha}{\cos\alpha - 1/\eta} \f]
	 * 
	 * @param[in] cosalpha cosine of the angle at the center of the earth.
	 * @param[in] eta relative altitude of the observed point.
	 * @param[in] u viewing zenith angle unit.
	 * @return the zenithal angle between a point and a satellite.
	 */
	double zenangle(const double cosalpha, const double eta, const unit_angle u);
	
	/**
	 * @brief Calculates the zenithal angle between a point and a satellite.
	 * 
	 * @param[in] coord0 Coordinate of the point on the surface.
	 * @param[in] coord1 Coordinate of the observed point at relative altitude eta.
	 * @param[in] eta relative altitude of the observed point.
	 * @param[in] u zentih angle unit.
	 * @return the zenithal angle between a point and a satellite.
	 */
	double zenangle(const Coordinate& coord0, const Coordinate& coord1, const double eta, const unit_angle u=default_unit);
	
	/**
	 * @brief Calculates the zenithal angle between a point and a satellite.
	 * @see zenangle(const Coordinate& coord, double eta, unit_angle u=DEGREE)
	 */
	double zenangle(const double lon0, const double lat0, const double lon1, const double lat1, const double eta, const unit_angle u=default_unit);
	
	/**
	 * @brief Calculates the swath angle between a point and a satellite.
	 * 
	 * Calculates the swath angle (<var>f</var>), between a point at Coordinate coord0 and a satellite at coord1 and at relative altitude (\f$ \eta \f$).
	 * If the point is under the satellite, the swath angle is 0.
	 * 
	 * \f[ \tan f = \frac{\sin \alpha}{\eta-\cos \alpha} \f]
	 * 
	 * @param[in] cosalpha cosine of the angle at the center of the earth.
	 * @param[in] eta relative altitude of the observed point.
	 * @param[in] u swath angle unit.
	 * @return the swath angle between a point and a satellite.
	 */
	double swath(const double cosalpha, const double eta, const unit_angle u);
	
	/**
	 * @brief Calculates the swath angle between a point and a satellite.
	 * 
	 * @param[in] coord0 Coordinate of the point on the surface.
	 * @param[in] coord1 Coordinate of the observed point at relative altitude eta.
	 * @param[in] eta relative altitude of the satellite.
	 * @param[in] u swath angle unit.
	 * @return the swath angle between a point and a satellite.
	 */
	double swath(const Coordinate& coord0, const Coordinate& coord1, const double eta, const unit_angle u=default_unit);
	
	/**
	 * @brief Calculates the swath angle between a point and a satellite.
	 * 
	 * @see swath(const Coordinate& coord, double eta, unit_angle u=DEGREE)
	 */
	double swath(const double lon0, const double lat0, const double lon1, const double lat1, const double eta, const unit_angle u=default_unit);
	
	/**
	 * @brief Calculates the declination of the sun.
	 * 
	 * Calculates the declination (\f$ \delta \f$) of the sun given the day (<var>J</var>) of the year.
	 * 
	 * \f[ \sin \delta(J) = \underbrace{\sin \epsilon}_{0.397775} \cdot \sin \left [ \frac{2 \pi}{365} \cdot (J-82)+0.033161 \cdot \sin \left( \frac{2 \pi}{365} \cdot (J-3) \right) \right ] \f]
	 * where \f$ \epsilon \f$ is the earth obliquity (=23.44°)
	 * 
	 * @param[in] day day of the year (1 = January the 1st).
	 * @return declination of the sun in radians.
	 */
	double declination(const int day);
	
	/**
	 * @brief Calculates the solar zenithal angle and solar azimuth angle.
	 * 
	 * Calculates the solar zenithal angle (\f$ \zeta_S \f$) and solar azimuth angle (\f$ \beta_S \f$) from latitude (\f$ \phi \f$), declination (\f$ \delta \f$) and local decimal hour (<var>H</var>).
	 * 
	 * \f[ \cos \zeta_S = sin \phi \cdot \sin \delta + \cos \phi \cdot \cos \delta \cos H \f]
	 * \f[ \cos \beta'_S = - \frac{\cos \phi \cdot \sin \delta + \sin \phi \cdot \cos \delta \cdot \cos H}{\sin \zeta_S} \f]
	 * \f$ \beta'_S \f$ is in [0;\f$ \pi \f$] relative to South. To have \f$ \beta_S \f$ in [-\f$ \pi \f$;\f$ \pi \f$] relative to North :
	 * \f[ \beta_S = -\beta'_S \cdot sign(H) \pm \pi \f]
	 * 
	 * @param[in] date Date.
	 * @param[in] coord Coordinate of the point.
	 * @param[out] azi solar azimuthal angle.
	 * @param[in] u unit of the angle for zen and azi.
	 * @return solar zenithal angle.
	 */
	double sol_zenangle(const Date& date, const Coordinate& coord, double* azi=NULL, const unit_angle u=default_unit);
	
	/**
	 * @brief Calculates the solar zenithal angle and solar azimuth angle.
	 * 
	 * @param[in] lat latitude of the point in degrees N.
	 * @param[in] dec declination of the sun in radians.
	 * @param[in] heureloc decimal local hour.
	 * @param[out] azi solar azimuthal angle.
	 * @param[in] u unit of the angle for zen and azi.
	 * 
	 * @return solar zenithal angle.
	 */
	double sol_zenangle(const double lat, const double dec, const double heureloc, double* azi=NULL, const unit_angle u=default_unit);
	
	/**
	 * @brief Calculates a radius length on an ellipse.
	 * 
	 * Calculates the length (<var>r</var>) of an ellipse radius given its angle (\f$ \theta \f$) and the dimensions (<var>a</var>, <var>b</var>) of the ellipse.
	 * 
	 * \f[ r = \frac{|a \cdot b| }{\sqrt{a^2-(a^2-b^2) \cdot \cos^2 \theta }} \f]
	 * 
	 * @image html ellipse.png "Ellipse"
	 * 
	 * @param[in] a semimajor axis of the ellipse.
	 * @param[in] b semiminor axis of the ellipse.
	 * @param[in] t angle of the radius.
	 * @param[in] u unit of the angle.
	 */
	double radius_ellipse(const double a, const double b, double t, const geo::unit_angle u=default_unit);
	
	/**
	 * Expresses the longitude in the range [-180°, 180°]
	 * @param[in,out] lon Longitude to modify.
	 * @return lon Modified longitude.
	 */
	double to_180_180(double lon);
	
	/**
	 * Expresses the longitude in the range [0°, 360°]
	 * @param[in,out] lon Longitude to modify.
	 * @return lon Modified longitude.
	 */
	double to_0_360(double lon);
	
	/**
	 * @brief Converts a geocentric latitide to geodetic latitude.
	 * 
	 * \f[ \tan D = \frac{\tan C}{(1-f)^2} \f]
	 * 
	 * @param[in] gc Geocentric latitude.
	 * @param[in] u Unit of latitude (input and ouput).
	 * @param[in] f Flattening (default: earth's).
	 * @return Geodetic latitude.
	 */
	double to_geodetic(double gc, unit_angle u=default_unit, double f=f_earth);
	
	/**
	 * @brief Converts a geodetic latitide to geocentric latitude.
	 * 
	 * \f[ \tan C = \tan D \cdot (1-f)^2 \f]
	 * 
	 * @param[in] gd Geodetic latitude.
	 * @param[in] u Unit of latitude (input and ouput).
	 * @param[in] f Flattening (default: earth's).
	 * @return Geocentric latitude.
	 */
	double to_geocentric(double gd, unit_angle u=default_unit, double f=f_earth);
	
	/**
	 * @brief Calculates Coordinate of point of an equidistant circle of given center.
	 * 
	 * @param[in] center Coordinate of the center of the circle.
	 * @param[in] alpha Angle at the center of the sphere.
	 * @param[in] azimuth Azimuth that describe the circle towards North.
	 * @param[in] u Unit angle of alpha and azimuth.
	 */
	Coordinate equidistantCircle(const Coordinate& center, double alpha, double azimuth, unit_angle u=default_unit);
}

namespace std
{
	std::ostream& operator<<(std::ostream& out, const Coordinate& coord); //!< std::ostream::operator<<() overload to print coordinates.
}

#endif
