#ifndef PIXEL_HPP
#define PIXEL_HPP

#include <geo/geo.hpp>
#include <vector>
#include <complex>
#include <caldat/caldat.hpp>

/**
 * @brief Abstract class defining a pixel
 */
class Pixel
{
	public:
	
	/**
	 * @brief Scan mode of the instrument
	 *
	 * Used to calculate Pixel azimuthAngle with viewing azimuthangle.
	 */
	enum ScanMode
	{
		GEO, //!< Geostationary image. Pixel azimuthAngle is to 0.
		XT, //!< Cross Track scan mode. Pixel azimuthAngle is orthogonal to viewing azimuthAngle.
		CONICAL //!< Conical scan mode. Pixel azimuthAngle is equal to viewing azimuthAngle.
	};
	
	// Constructeurs
	
	Pixel(); //!< Default constructor
	
	/**
	 * @brief Constructor
	 *
	 * Create a Pixel and initialize its geocentric Coordinate.
	 * @param[in] coord Geocentric Coordinate of the Pixel.
	 */
	Pixel(const Coordinate& coord);
	
	/**
	 * @brief Constructor
	 *
	 * Create a Pixel and initialize its geocentric Coordinate.
	 * @param[in] coord Geocentric Coordinate of the Pixel.
	 * @param[in] satCoord Geocentric Coordinate of the satellite.
	 */
	Pixel(const Coordinate& coord, const Coordinate& satCoord);
	
	/**
	 * @brief Constructor
	 * 
	 * Create a Pixel and initialize its Date and geocentric Coordinate.
	 *
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Geocentric Coordinate of the Pixel.
	 */
	Pixel(const double time, const Coordinate& coord);
	
	/**
	 * @brief Constructor
	 * 
	 * Create a Pixel and initialize its Date and geocentric Coordinate.
	 *
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Geocentric Coordinate of the Pixel.
	 * @param[in] satCoord Geocentric Coordinate of the satellite.
	 */
	Pixel(const double time, const Coordinate& coord, const Coordinate& satCoord);
	
	/**
	 * @brief Returns a pointer to a copy of the Pixel.
	 *
	 * This method should be used to copy the current Pixel (ie to copy an Orbit)
	 * @return a pointer to a copy of the Pixel.
	 */
	virtual Pixel* clone() const = 0; // Constructeur vituel

	// Destructeur
	virtual ~Pixel(); //!< Destructor

	// Getteurs
	/**
	 * @brief Returns the m_value vector.
	 *
	 * The m_value vector is public, it can be accessed directly.
	 * @return the vector that contains the values given to the Pixel.
	 */
	const std::vector<double>& value() const;

	/**
	 * @brief Returns the i-th value of the m_value vector.
	 *
	 * The m_value vector is public, it can be accessed directly.
	 * @return the i-th element of the vector that contains the values given to the Pixel.
	 */
	const double& value(const size_t i) const;

	/**
	 * @brief Returns the time of the Pixel.
	 *
	 * The time should be in julian day in order to the night() method to work.
	 * @see night()
	 * @return the time of the the Pixel.
	 */
	const double& time() const;

	/**
	 * @brief Returns the longitude of the Pixel.
	 *
	 * The longitude is given in [-180°; +180°].
	 * @return the longitude of the the Pixel in the given unit.
	 * @param[in] u unit of the angle (default is degree)
	 */
	double lon(const geo::unit_angle u = geo::DEGREE) const;

	/**
	 * @brief Returns the latitude of the Pixel.
	 *
	 * The longitude is given in [+90°; -90°].
	 * @return the latitude of the the Pixel in the given unit.
	 * @param[in] u unit of the angle (default is degree)
	 */
	double lat(const geo::unit_angle u = geo::DEGREE) const;

	/**
	 * @brief Returns the coordinate of the Pixel.
	 *
	 * @return the coordinate of the the Pixel.
	 */
	const Coordinate& coord() const;
	
	/**
	 * @brief Returns the cosine of the angle at the center of the earth between the Pixel and the sub-satellite point.
	 */
	const double& cosalpha() const;
	
	/**
	 * @brief Tells if Pixel is distortable.
	 */
	const bool& distortable() const;
	
	/**
	 * @brief Tells if Pixel's PSF has a central symmetry.
	 */
	const bool& symmetric() const;
	
	/**
	 * @brief Returns the viewing viewing zenith angle of the Pixel.
	 *
	 * This method returns the VZA of the Pixel.
	 * @return the  viewing viewing zenith angle of the Pixel.
	 * @param[in] u unit of the angle (default is degree)
	 */
	double viewingZenithAngle(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns the viewing azimuth angle of the Pixel.
	 *
	 * This method returns the VAA of the Pixel.
	 * It does not calculate the angle.
	 * @return the viewing azimuthangle of the Pixel.
	 * @param[in] u unit of the angle to return (default is degree)
	 */
	double viewingAzimuthAngle(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns the azimuthangle of the Pixel.
	 *
	 * This method returns the azimuth angle (orientation) of the Pixel toward North.
	 * It does not calculate the angle.
	 * @return the azimuth angle of the Pixel.
	 * @param[in] u unit of the angle to return (default is degree)
	 */
	double azimuthAngle(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns the K distortion factor of the Pixel.
	 *
	 * This method returns the along-viewing-azimuthAngle direction distortion factor of the Pixel.
	 * @return the K distortion factor of the Pixel.
	 */
	double K() const;

	/**
	 * @brief Returns the L distortion factor of the Pixel.
	 *
	 * This method returns the cross-viewing-azimuthAngle direction distortion factor of the Pixel.
	 * @return the K distortion factor of the Pixel.
	 */
	double L() const;

	/**
	 * @brief Returns the relative altitude of the satellite viewing the Pixel.
	 *
	 * @return the relative altitude of the satellite viewing the Pixel.
	 */
	const double& relativeAltitude() const;

	/**
	 * @brief Returns the altitude of the satellite viewing the Pixel.
	 * 
	 * \f[ altitude = (\eta-1) \cdot r \f]
	 * 
	 * @return the altitude of the satellite viewing the Pixel according to the given radius.
	 * @param[in] r equatorial radius of the planet (default is Earth).
	 */
	double altitude(const double r = geo::req_earth) const;

	/**
	 * @brief Returns the swathangle of the Pixel.
	 *
	 * This method returns the swathangle of the Pixel.
	 * @return the swathangle of the Pixel.
	 */
	double swathAngle(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns the distance between the center of the Pixel and the satellite.
	 *
	 * \f[ d = R \cdot \sqrt{\eta^2 - 2\cdot\eta\cos\alpha + 1} \f]
	 * 
	 * @return the distance between the center of the Pixel and the satellite.
	 */
	double satDistance(const double r = geo::req_earth) const;

	/**
	 * @brief Returns the longitude of the satellite.
	 *
	 * The longitude is given in [-180°; +180°].
	 * @return the longitude of the the satellite viewing the Pixel in the given unit.
	 * @param[in] u unit of the angle (default is degree)
	 */
	double satLon(const geo::unit_angle u = geo::DEGREE) const;

	/**
	 * @brief Returns the latitude of the satellite.
	 *
	 * The longitude is given in [+90°; -90°].
	 * @return the latitude of the the satellite viewing the Pixel in the given unit.
	 * @param[in] u unit of the angle (default is degree)
	 */
	double satLat(const geo::unit_angle u = geo::DEGREE) const;

	/**
	 * @brief Returns the Coordinate of the satellite.
	 *
	 * @return the Coordinate of the the satellite viewing the Pixel.
	 */
	const Coordinate& satCoord() const;
	
	/**
	 * @brief Returns the scan mode of the Pixel.
	 *
	 * @return the scan mode of the Pixel.
	 */
	const ScanMode& scanMode() const;
	
	/**
	 * @brief Tells if the Pixel must be colocated.
	 * 
	 * @return true if Pixel must be colocated, else false.
	 */
	const bool& proceed() const;
	
	/**
	 * @brief Returns the weight of the Pixel.
	 * 
	 * @return the weight of the Pixel.
	 */
	const double& weight() const;

	// Méthodes
	/**
	 * @brief Calculates the PSF of the pixel at the given Coordinates.
	 * 
	 * @return the proportion in [0-1] of energy seen by the pixel at the given Coordinate.
	 * @param[in] coord coordinate where to calculate PSF
	 */
	virtual double psf(const Coordinate& coord) const = 0;

	/**
	 * @brief Returns the radius of the circle in which the pixel at nadir is included in meters.
	 *
	 * Outside this radius, the PSF must be 0.
	 * @return the radius, in meters, of the circle in which the pixel at nadir is included.
	 */
	virtual double radius() const = 0;
	
	/**
	 * @brief Returns the surface of the pixel in square meters.
	 *
	 * @return the surface of the pixel in square meters.
	 */
	virtual double surface() const = 0;
	
	/**
	 * @brief Returns the solar viewing zenith angle of the Pixel.
	 *
	 * The time of the pixel must be given in julian days to make this method to work.
	 * @return a the solar zenith angle of the Pixel.
	 * @param[in] u unit of the viewing zenith angle to be returned.
	 * @param[out] solarAzimuthAngle (optional) solar azimuth angle.
	 */
	double solarZenithAngle(const geo::unit_angle u = geo::DEGREE, double* solarAzimuthAngle=NULL) const;
	
	/**
	 * @brief Returns the solar azimuth angle of the Pixel.
	 *
	 * The time of the pixel must be given in julian days to make this method to work.
	 * @return a the solar azimuth angle of the Pixel.
	 * @param[in] u unit of the azimuth angle to be returned.
	 * @param[out] solarZenithAngle (optional) solar zenith angle.
	 */
	double solarAzimuthAngle(const geo::unit_angle u = geo::DEGREE, double* solarZenithAngle=NULL) const;
	
	/**
	 * @brief Returns the relative azimuth angle of the Pixel.
	 *
	 * \f[ RAA = \beta - \beta_S - \pi \f]
	 *
	 * The time of the pixel must be given in julian days to make this method to work.
	 * @return a the relative azimuth angle of the Pixel.
	 * @param[in] u unit of the azimuth angle to be returned.
	 */
	double relativeAzimuthAngle(const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Tells if the pixel is in night side of the earth.
	 *
	 * The time of the pixel must be given in julian days to make this method to work.
	 * @return a boolean telling if the pixel is in night side of the earth.
	 * @param[in] threshold solar zenith angle up to the pixel is considered at day.
	 * @param[in] u unit angle for threshold.
	 * The solar viewingZenithangle is calculated. If it is greater than threshold, pixel is considered at night
	 */
	bool night(const double threshold = 80., const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Tells if the pixel is seen within sunglint.
	 *
	 * The time of the pixel must be given in julian days to make this method work.
	 * @return a boolean telling if the pixel is seen within sunglint.
	 * @param[in] threshold solar scattering angle.
	 * @param[in] u unit angle for threshold.
	 */
	bool sunglint(double threshold = 16., const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Returns the scatter angle between this Pixel and the given one.
	 * 
	 * The scattering angle (\f$ \gamma \f$) is defined from viewing zenithal angle (\f$ \zeta \f$) and viewing azimutal angle (\f$ \beta \f$) this way :
	 * \f[ \cos(\gamma) =  \cos \zeta_A \cdot \cos \zeta_B + \sin \zeta_A \cdot \sin \zeta_B \cdot \cos(\beta_A - \beta_B) \f]
	 * @image html cone.png "The scattering angle (γ) must be smaller than the threshold (θ)."
	 * 
	 * @param[in] pixel Pixel to be considered.
	 * @param[in] u unit angle of the returned scatter angle.
	 * @return the scatter angle between this Pixel and the given one.
	 */
	double scatterAngle(const Pixel *const pixel, const geo::unit_angle u = geo::DEGREE) const;
	
	/**
	 * @brief Calculates the cosine of the angle at the center of the earth between the Pixel and the sub-satellite point.
	 * @return the cosine of the angle at the center of the earth between the Pixel and the sub-satellite point.
	 */
	const double& updateCosalpha();
	
	/**
	 * @brief Calculates the viewing azimuth angle between the Pixel and the sub-satellite point and store it.
	 * @return the viewing azimuth angle between the Pixel and the sub-satellite point.
	 */
	double updateViewingAzimuthAngle(const geo::unit_angle u=geo::DEGREE);
	
	/**
	 * @brief Calculates the azimuth angle toward North of the Pixel and update attribute m_azimuthAngle.
	 * 
	 * If scanMode is XT, then the azimuth angle is orthogonal to the swathAngle.
	 * 
	 * If scanMode is CONICAL, then the azimuth angle is equal to viewing azimuthangle.
	 *
	 * If scanMode is GEO, then the azimuth angle is set to 0.
	 *
	 * @return the updated azimuth angle in degrees.
	 */
	double updateAzimuthAngle(const geo::unit_angle u=geo::DEGREE);
	
	/**
	 * @brief Calculates distortion factors and store them.
	 */
	void updateDistortion();
	
	/**
	 * @brief Resizes m_value vector.
	 * 
	 * @param[in] i size to give to m_value.
	 */
	void resizeValue(const size_t i);
	
	/**
	 * @brief Clears m_value vector.
	 */
	void clearValue();


	// Setteurs
	/**
	 * @brief Sets the i-th value of the m_value vector.
	 *
	 * m_value will be resized if needed.
	 * The m_value vector is public, it can be accessed directly.
	 * @param[in] i index of the element of the vector to be set.
	 * @param[in] value value to be set.
	 */
	void setValue(const double value, const size_t i=0);
	
	/**
	 * @brief Sets the time of the pixel.
	 *
	 * @param[in] time must be given in julian days.
	 */
	void setTime(const double time);
	
	/**
	 * @brief Sets the time of the pixel.
	 *
	 * @param[in] date Date of the Pixel.
	 */
	void setTime(const Date& date);
	
	/**
	 * @brief Sets the coordinate of the pixel.
	 *
	 * @param[in] coord Coordinate of the Pixel.
	 */
	void setCoord(const Coordinate& coord);
	
	/**
	 * @brief Sets the coordinate of the pixel.
	 *
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	void setCoord(const Coordinate& coord, const Coordinate& satCoord);
	
	/**
	 * @brief Sets the azimuthangle of the Pixel.
	 *
	 * This method sets the azimuthangle of the Pixel toward North.
	 *
	 * Warning: This value is overwritten if a call of setCoord() or setSatCoord() or setViewingAzimuthAngle() occurs.
	 * @param[in] azimuthAngle azimuthangle.
	 * @param[in] u unit of the given angle.
	 */
	void setAzimuthAngle(double azimuthAngle, const geo::unit_angle u = geo::DEGREE);

	/**
	 * @brief Sets the relative altitude of the satellite.
	 *
	 * @param[in] relativeAltitude relative altitude of the satellite from which the pixel is viewed.
	 */
	void setRelativeAltitude(const double relativeAltitude);

	/**
	 * @brief Sets the altitude of the satellite.
	 *
	 * This method sets the altitude of the satellite and calls update() method.
	 * The Pixel class converts given altitude into relative altitude (\f$ \eta \f$).
	 *
	 * \f[ \eta = \frac{altitude+r}{r} \f]
	 *
	 * @param[in] altitude altitude of the satellite from which the pixel is viewed.
	 * @param[in] r equatorial radius of the planet.
	 */
	void setAltitude(const double altitude, const double r = geo::req_earth);

	/**
	 * @brief Sets the Coordinate of the satellite.
	 *
	 * This method sets the Coordinate of the satellite and calls the update() method.
	 * @param[in] coord Coordinate of the satellite from which the pixel is viewed.
	 */
	void setSatCoord(const Coordinate& coord);
	
	/**
	 * @brief Sets if Pixel is distortable
	 * 
	 * If the Pixel is not distortable, K() and L() methods will always return 1.
	 * 
	 * @param[in] b true if Pixel is distortable else false.
	 */
	void setDistortable(const bool b);
	
	/**
	 * @brief Sets the scan mode of the instrument.
	 * 
	 * This will call updateAzimuthAngle() if VZA is not 0.
	 * 
	 * @param[in] m Scan mode to be set.
	 */
	void setScanMode(const ScanMode& m);
	
	/**
	 * @brief Sets the proceed attribute.
	 * 
	 * @param[in] proceed boolean to tell if the Pixel must be colocated.
	 */
	void setProceed(const bool proceed);
	
	/**
	 * @brief Sets the weight of the Pixel.
	 * 
	 * @param[in] weight weight to be given to the Pixel.
	 */
	void setWeight(const double weight);
	
	// Opérateur
	/**
	 * @brief Greater operator.
	 * 
	 * This operator tells which Pixel has the greater time, then longitude and finally latitude if previous are equal. It is useful to sort an Orbit in order to speed-up searches.
	 * @param[in] pixel Pixel to compare.
	 * @return true if the left hand Pixel is older.
	 */
	bool operator>(const Pixel& pixel) const;
	
	/**
	 * @brief Lower operator.
	 * 
	 * This operator tells which Pixel has the lower time, then longitude if and finally latitude if previous are equal. It is useful to sort an Orbit in order to speed-up searches.
	 * @param[in] pixel Pixel to compare.
	 * @return true if the left hand Pixel is earlier.
	 */
	bool operator<(const Pixel& pixel) const;

	// Attribut
	/**
	 * @brief vector to store whatever double you want.
	 */
	std::vector<double> m_value; // oui c'est mal de le mettre en public, mais c'est pour y mettre ce qu'on veut.

	protected:
	// Methods
	/**
	 * @brief Apply distortion factors to a point and project it in the Pixel referential.
	 * 
	 * This function is to be used just before calculating PSF.
	 * 
	 * The coordinates are used to calculate the distance (<var>d</var>) and the azimuthAngle (\f$ \beta \f$) between the Pixel center and the given point.
	 * Then the point is projected in the Pixel's referencial (rotation + distortion).
	 * 
	 * \f[ a = \frac{d \cdot - \sin(\beta - azimuthAngle())}{K()} \f]
	 * \f[ c = \frac{d \cdot \cos(\beta - azimuthAngle())}{L()} \f]
	 * 
	 * @param[in,out] z Complex coordinate in the Pixel referential.
	 * @return the position of the point once distorded in the Pixel referential (relative to the Pixel center, in meters).
	 */
	std::complex<double>& rotate_and_distort(std::complex<double>& z) const;
	
	// Attributs
	double m_time;  //!< Time in julian days.
	Coordinate m_coord; //!< Coordinate of the Pixel. For better accuracy, the latitude should be geocentric.
	double m_relativeAltitude; //!< Relative altitude of the Satellite.
	Coordinate m_satCoord; //!< Coordinate of the Satellite. For better accuracy, the latitude should be geocentric.
	double m_cosalpha; //!< Cosine of the angle at the center of the earth between the Pixel and the sub-satellite point.
	double m_viewingAzimuthAngle; //!< Viewing Azimuth Angle.
	double m_K; //!< Along-VAA distortion coefficient.
	double m_L; //!< Cross-VAA distortion coefficient.
	
	/**
	 * @brief azimuthAngle of the Pixel toward North in degrees.
	 * 
	 * If scanning mode is XT, azimuthAngle is orthogonal to swathAngle or parallel to orbit.
	 * 
	 * If scanning mode is conical, azimuthAngle is equal to viewing azimuthAngle.
	 */
	double m_azimuthAngle;
	
	ScanMode m_scanMode; //!< Scan mode of the satellite
	
	bool m_proceed; //!< Tells if the Pixel must be colocated.
	double m_weight; //!< Weight to give to the pixel during colocation.
	bool m_distortable; //!< Tells if the Pixel must be distorted.
	bool m_symmetric; //!< Tells if Pixel's PSF has a central symmetry
}; // Pixel


/**
 * @brief Square box
 * 
 * This class defines a square box of the given size. Its distortion factors are set to 1.
 */
class PixelBox : public Pixel
{
	public:
	// Constructeurs
	/**
	 * @brief Constructor. Calls Pixel::Pixel(), sets default attributes.
	 * 
	 * @param[in] size size of the square Box.
	 */
	PixelBox(const double size);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel(), sets default attributes.
	 * 
	 * @param[in] size size of the square Box.
	 * @param[in] coord Coordinate of the Pixel.
	 */
	PixelBox(const double size, const Coordinate& coord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel(), sets default attributes.
	 * 
	 * @param[in] size size of the square Box.
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	PixelBox(const double size, const Coordinate& coord, const Coordinate& satCoord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel(), sets default attributes.
	 * 
	 * @param[in] size size of the square Box.
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Coordinate of the Pixel.
	 */
	PixelBox(const double size, const double time, const Coordinate& coord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel(), sets default attributes.
	 * 
	 * @param[in] size size of the square Box.
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	PixelBox(const double size, const double time, const Coordinate& coord, const Coordinate& satCoord);
	
	virtual PixelBox* clone() const; // Constructeur vituel
	
	// Méthodes
	/**
	 * @brief PixelCircle PSF function
	 * 
	 * \f[ psf(\lambda, \phi) = \left\{ 
	 * \begin{array}{l l}
	 * 1 & \quad \mbox{if } |lon()-\lambda| \leq m\_size()/2 \mbox{ and } |lat()-\phi| \leq m\_size()/2 \\
	 * 0 & \quad \mbox{else} \\
	 * \end{array} \right. \f]
	 *
	 */
	virtual double psf(const Coordinate& coord) const;
	virtual double radius() const; // Renvoie le rayon du disque dans lequel le pixel au Nadir est inscrit, c'est une constante propre au pixel
	virtual double surface() const;
	
	// Attribute
	static const ScanMode default_scanMode = GEO; //!< Default scan mode.
	static const bool default_symmetric = false; //!< Default symmetry.
	
	protected:
	// Attributs
	double m_size; //!< Box size.
};


/**
 * @brief Circular Pixel
 * 
 * This class defines a circle circle of the given size.
 */
class PixelCircle : public Pixel
{
	public:
	// Constructeurs
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and set default attributes.
	 * 
	 * @param[in] radius radius of the Circle.
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 */
	PixelCircle(const double radius, const double relativeAltitude);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and set default attributes.
	 * 
	 * @param[in] radius radius of the Circle.
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 * @param[in] coord Coordinate of the Pixel.
	 */
	PixelCircle(const double radius, const double relativeAltitude, const Coordinate& coord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and set default attributes.
	 * 
	 * @param[in] radius radius of the Circle.
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	PixelCircle(const double radius, const double relativeAltitude, const Coordinate& coord, const Coordinate& satCoord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and set default attributes.
	 * 
	 * @param[in] radius radius of the Circle.
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Coordinate of the Pixel.
	 */
	PixelCircle(const double radius, const double relativeAltitude, const double time, const Coordinate& coord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and set default attributes.
	 * 
	 * @param[in] radius radius of the Circle.
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	PixelCircle(const double radius, const double relativeAltitude, const double time, const Coordinate& coord, const Coordinate& satCoord);
	
	virtual PixelCircle* clone() const; // Constructeur vituel

	// Méthodes
	/**
	 * @brief PixelCircle PSF function
	 * 
	 * \f[ psf(r) = \left\{ 
	 * \begin{array}{l l}
	 * 1 & \quad \mbox{if } r \leq radius() \\
	 * 0 & \quad \mbox{else} \\
	 * \end{array} \right. \f]
	 *
	 */
	virtual double psf(const Coordinate& coord) const;
	virtual double radius() const; // Renvoie le rayon du disque dans lequel le pixel au Nadir est inscrit, c'est une constante propre au pixel
	virtual double surface() const;
	
	// Attribute
	static const ScanMode default_scanMode = GEO; //!< Default scan mode.
	
	protected:
	// Attributs
	double m_radius; //!< Circle size.
};


/**
 * @brief Pinch-like Pixel
 */
class PixelPinchCos : public PixelCircle
{
	public:
	// Constructeurs
	/**
	 * @brief Constructor. Calls PixelCircle::PixelCircle() and set default attributes.
	 * 
	 * @param[in] radius radius of the Pixel (= FWMH).
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 */
	PixelPinchCos(const double radius, const double relativeAltitude);
	
	/**
	 * @brief Constructor. Calls PixelCircle::PixelCircle() and set default attributes.
	 * 
	 * @param[in] radius radius of the Pixel (= FWMH).
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 * @param[in] coord Coordinate of the Pixel.
	 */
	PixelPinchCos(const double radius, const double relativeAltitude, const Coordinate& coord);
	
	/**
	 * @brief Constructor. Calls PixelCircle::PixelCircle() and set default attributes.
	 * 
	 * @param[in] radius radius of the Pixel (= FWMH).
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	PixelPinchCos(const double radius, const double relativeAltitude, const Coordinate& coord, const Coordinate& satCoord);
	
	/**
	 * @brief Constructor. Calls PixelCircle::PixelCircle() and set default attributes.
	 * 
	 * @param[in] radius radius of the Pixel (= FWMH).
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Coordinate of the Pixel.
	 */
	PixelPinchCos(const double radius, const double relativeAltitude, const double time, const Coordinate& coord);
	
	/**
	 * @brief Constructor. Calls PixelCircle::PixelCircle() and set default attributes.
	 * 
	 * @param[in] radius radius of the Pixel (= FWMH).
	 * @param[in] relativeAltitude relative altitude of the satellite.
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	PixelPinchCos(const double radius, const double relativeAltitude, const double time, const Coordinate& coord, const Coordinate& satCoord);
	
	virtual PixelPinchCos* clone() const; // Constructeur vituel

	// Méthodes
	/**
	 * @brief Pinch-like PSF function
	 * 
	 * Note that radius() = FWMH.
	 * 
	 * \f[ psf(r) = \left\{ 
	 * \begin{array}{l l}
	 * \frac{1}{2} \cdot \cos \left(\frac{\pi \cdot r}{radius()} \right)+1 & \quad \mbox{if } r \leq radius() \\
	 * 0 & \quad \mbox{else} \\
	 * \end{array} \right. \f]
	 * 
	 * @image html pinch_psf.png "PixelPinchCos point spread function."
	 */
	virtual double psf(const Coordinate& coord) const;
	virtual double surface() const;
	
	// Attribute
	static const ScanMode default_scanMode = GEO; //!< Default scan mode.
};


#endif
