#ifndef ORBIT_HPP
#define ORBIT_HPP

#include <coloc/pixel.hpp>
#include <coloc/image.hpp>
#include <vector>

/**
 * @brief Pixel container with no particular order.
 */
class Orbit : public std::vector<Pixel*>
{
	public:
	// Constructeurs
	/**
	 * @brief Default constructor.
	 *
	 * Creates an empty Orbit
	 */
	Orbit();
	
	/**
	 * @brief Copy constructor.
	 *
	 * Copies every Pixel in a new Orbit.
	 * @param[in] copy is the Orbit to be copied.
	 */
	Orbit(const Orbit & copy);
	
	/**
	 * @brief Constructor.
	 *
	 * Creates an Orbit containing the given Pixel.
	 * The Pixels are not copied !
	 * @param[in] pixels is a vector of Pixel* to be added to the Orbit.
	 */
	Orbit(const std::vector<Pixel*> & pixels);
	
	/**
	 * @brief Constructor.
	 *
	 * Creates an Orbit and creates Pixel.
	 * If time or sat_lon or sat_lat or mask are empty, default values will be used instead.
	 * If a given latitude is outside [-90°;90°] then the Pixel will be deactivated (whatever given mask).
	 * 
	 * @param[in] pixel_template template of Pixel to use.
	 * @param[in] time is a vector of Pixel times.
	 * @param[in] lon is a vector of Pixel longitudes.
	 * @param[in] lat is a vector of Pixel latitudes.
	 * @param[in] sat_lon is a vector of satellite longitudes.
	 * @param[in] sat_lat is a vector of satellite latitudes.
	 * @param[in] mask is a vector of boolean that will be used in Pixel::setProceed().
	 */
	Orbit(const Pixel& pixel_template, const std::vector<double>& time, const std::vector<double>& lon, const std::vector<double>& lat, const std::vector<double>& sat_lon, const std::vector<double>& sat_lat, const std::vector<bool>& mask);
	
	/**
	 * @brief Constructor.
	 *
	 * Creates an empty Orbit of size i
	 * @param[in] i is the size of the Orbit to be allocated.
	 */
	Orbit(const size_t i);
	
	/**
	 * @brief Constructor.
	 *
	 * Creates an Orbit by copying Pixels from a given Image.
	 * @param[in] image is the Image from where are copied the Pixels.
	 */
	explicit Orbit(const Image& image);
	
	// Destructeur
	~Orbit(); //!< Deallocate every Pixel in the Orbit.
	
	// Méthodes
	/**
	 * @brief Calls Pixel::setValue() of each non NULL Pixel in the Orbit.
	 * 
	 * @param array array of double to be set in each Pixel. The array size must be greater than the Orbit's.
	 * @param index index where to store the values in Pixel::m_pixel.
	 */
	void setValue(const double* array, const size_t index=0);
	
	/**
	 * @brief Calls Pixel::setValue() of each non NULL Pixel in the Orbit.
	 * 
	 * @param vector vector of double to be set in each Pixel.
	 * @param index index where to store the values in Pixel::m_pixel.
	 */
	void setValue(const std::vector<double>& vector, const size_t index=0);
	
	/**
	 * @brief Deallocate every Pixel in the Orbit.
	 *
	 * Deallocate every Pixel in the Orbit. Nevertheless the container vector is not cleared, all elements are set to NULL.
	 */
	void free();
	void clear(); //!< Deallocate every Pixel in the Orbit and clear the vector.
	
	/**
	 * @brief Resize the vector of Pixel.
	 *
	 * @param[in] n number of Pixel to store in memory.
	 */
	void resize(const size_t n);
	
	/**
	 * @brief Sets the Pixel at the given position.
	 * 
	 * The Pixel is copied. If a pixel is already set here, it will be deleted before.
	 * 
	 * @param[in] i index of the Pixel to be set.
	 * @param[in] pixel Pixel to store in the Image.
	 */
	void set(const size_t i, const Pixel& pixel);
	
	// Getteurs
	/**
	 * @brief Returns longitudes of all pixels.
	 *
	 * @return a vector that contains longitudes of all pixels
	 */
	std::vector<double> lon() const;

	/**
	 * @brief Returns latitudes of all pixels.
	 *
	 * @return a vector that contains latitudes of all pixels.
	 */
	std::vector<double> lat() const;
	
	/**
	 * @brief Returns satellite longitudes of all pixels.
	 *
	 * @return a vector that contains satellite longitudes of all pixels
	 */
	std::vector<double> satLon() const;

	/**
	 * @brief Returns satellite latitudes of all pixels.
	 *
	 * @return a vector that contains satellite latitudes of all pixels.
	 */
	std::vector<double> satLat() const;

	/**
	 * @brief Returns times of all pixels.
	 *
	 * @return a vector that contains times of all pixels.
	 */
	std::vector<double> time() const;
	
	/**
	 * @brief Returns the index-th value of all pixels.
	 *
	 * @return a vector that contains values of all pixels.
	 */
	std::vector<double> value(const size_t index=0) const;

	// Setteurs
	/**
	 * @brief Sort the Orbit.
	 *
	 * Sort the Orbit. The comparison is made thanks to the compareTime() method.
	 * @see compareTime()
	 */
	void sort();
	
	/**
	 * @brief Sort the Orbit.
	 *
	 * Sort the Orbit. The comparison is made thanks to the compareTime() method.
	 * @see compareTime()
	 */
	void stable_sort();
	
	/**
	 * @brief Calculate satellite azimuth for each Pixel.
	 *
	 * If the Scan_mode is XT, this method calculates the satellite azimuth with successive positions. The this value is given to each Pixel.
	 * 
	 * Warning: To work, this method needs to sort the Orbit according to time. So the stable_sort() method is called and initial order may be changed.
	 *
	 * @param[in] onlyZen0 If set, only Pixel's having a VZA = 0 will be updated.
	 * @param[in] nosort If set, Orbit won't be sorted.
	 */
	void updateAzimuthAngle(const bool onlyZen0 = false, const bool nosort = false);
	
	/**
	 * @brief Comparison function between two Pixel pointer.
	 * 
	 * This function tells which Pixel has the lower time.
	 * 
	 * It is useful to sort() or stable_sort() an Orbit.
	 * 
	 * @param[in] pixelA Pixel* to compare.
	 * @param[in] pixelB Pixel* to compare.
	 * @return true if the left hand Pixel is earlier, else false.
	 */
	static bool compareTime(const Pixel *const pixelA, const Pixel *const pixelB);
	
	/**
	 * @brief Comparison function between two Pixel pointer.
	 * 
	 * This function tells which Pixel has the lower latitude.
	 * 
	 * @param[in] pixelA Pixel* to compare.
	 * @param[in] pixelB Pixel* to compare.
	 * @return true if the left hand Pixel has lower latitude, else false.
	 */
	static bool compareLat(const Pixel *const pixelA, const Pixel *const pixelB);
	
	/**
	 * @brief Secant method.
	 *
	 * Find the index xn of Pixel the nearest of aim.
	 * @param[in] aim The time to reach.
	 * @param xn_1 Index of the lower boundary.
	 * @param xn Index of the upper boundary and result once converged.
	 * @param[in] max Maximum number of iterations to be done.
	 * @return Number of iterations to converge.
	 */
	unsigned int secant(const double aim, long& xn_1, long& xn, const unsigned int max);
	
	/**
	 * @brief Dichotomy method.
	 *
	 * Find the index of Pixel the nearest of aim by dichotomy (or bisection) method.
	 * @param[in] aim The time to reach.
	 * @param left Index of the lower boundary.
	 * @param right Index of the upper boundary and result once converged.
	 * @param[in] max Maximum number of iterations to be done.
	 * @return Number of iterations to converge.
	 */
	unsigned int dichotomy(const double aim, long& left, long& right, const unsigned int max);
	
	/**
	 * @brief False position method.
	 *
	 * Find the index of Pixel the nearest of aim by false position method (Illinois algoritm).
	 * @param[in] aim The time to reach.
	 * @param left Index of the lower boundary.
	 * @param right Index of the upper boundary and result once converged.
	 * @param[in] max Maximum number of iterations to be done.
	 * @return Number of iterations to converge.
	 */
	unsigned int falsiMethod(const double aim, long& left, long& right, const unsigned int max);
	
	/**
	 * @brief Assignment operator.
	 *
	 * Copies every Pixel in a this Orbit.
	 * @param[in] copy is the Orbit to be copied.
	 */
	Orbit& operator=(const Orbit & copy);
	
	/**
	 * @brief Concatenation operator.
	 * 
	 * Copy the Pixels from orbit and add those at the end of this Orbit.
	 * @param[in] orbit Orbit to concanate.
	 */
	Orbit& operator+=(const Orbit& orbit);
}; // Orbit

#endif
