#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <coloc/pixel.hpp>
#include <vector>
#include <geo/geo.hpp>
#include <navgeo/navgeo.hpp>
#include <caldat/caldat.hpp>

/**
 * @brief Pixel container. Useful for a great number Pixel stored in a fixed grid (typically from Geostationary).
 */
class Image
{
	public:
	// Constructeurs
	/**
	 * @brief Constructor.
	 *
	 * Creates an empty Image with the given dimensions
	 * 
	 * @param[in] ncols Number of columns.
	 * @param[in] nrows Number of rows.
	 */
	Image(const size_t ncols, const size_t nrows);
	
	/**
	 * @brief Copy constructor.
	 *
	 * Every Pixel and the Geostationary are copied in a new Image.
	 * 
	 * @param[in] copy Image to be copied.
	 */
	Image(const Image& copy);

	/**
	 * @brief Destructor.
	 *
	 * Deallocate every Pixel inside
	 */
	virtual ~Image();

	/**
	 * @brief Returns the read-only i-th Pixel*.
	 *
	 * @param[in] i index of the Pixel to be returned.
	 * @return the i-th Pixel*.
	 */
	Pixel const *const & operator[](const size_t i) const;

	/**
	 * @brief Returns the read-only Pixel* at the given position.
	 *
	 * @param[in] c column of the Pixel to be returned.
	 * @param[in] r row of the Pixel to be returned.
	 * @return the Pixel* at the given position.
	 */
	Pixel const *const & operator()(long c, long r) const;
	
	/**
	 * @brief Returns the i-th Pixel*.
	 *
	 * @param[in] i index of the Pixel to be returned.
	 * @return the i-th Pixel*.
	 */
	Pixel*& operator[](const size_t i);

	/**
	 * @brief Returns the Pixel* at the given position.
	 *
	 * @param[in] c column of the Pixel to be returned.
	 * @param[in] r row of the Pixel to be returned.
	 * @return the Pixel* at the given position.
	 */
	Pixel*& operator()(long c, long r);
	
	/**
	 * @brief Returns the number of Pixels of the Image.
	 *
	 * @return the number of Pixels of the Image.
	 */
	size_t size() const;
	
	/**
	 * @brief Returns the number of rows of the Image.
	 *
	 * @return the number of rows of the Image.
	 */
	const size_t& nrows() const;
	
	/**
	 * @brief Returns the number of columns of the Image.
	 *
	 * @return the number of columns of the Image.
	 */
	const size_t& ncols() const;

	/**
	 * @brief Sets the Pixel at the given position.
	 * 
	 * The Pixel is copied. If a pixel is already set here, it will be deleted before.
	 * 
	 * @param[in] c column of the Pixel to be set.
	 * @param[in] r row of the Pixel to be set.
	 * @param[in] pixel to store in the Image.
	 */
	void set(const size_t c, const size_t r, const Pixel& pixel);

	/**
	 * @brief Sets the Pixel at the given position.
	 * 
	 * The Pixel is copied. If a pixel is already set here, it will be deleted before.
	 * 
	 * @param[in] index of the Pixel to be set.
	 * @param[in] pixel to store in the Image.
	 */
	void set(const size_t index, const Pixel& pixel);
	
	/**
	 * @brief Calls Pixel::setValue() of each non NULL Pixel in the Image.
	 * 
	 * @param array array of double to be set in each Pixel. The array size must be greater than the Image's.
	 * @param index index where to store the values in Pixel::m_pixel.
	 */
	void setValue(const double* array, const size_t index=0);
	
	/**
	 * @brief Calls Pixel::setValue() of each non NULL Pixel in the Image.
	 * 
	 * @param vector vector of double to be set in each Pixel.
	 * @param index index where to store the values in Pixel::m_pixel.
	 */
	void setValue(const std::vector<double>& vector, const size_t index=0);
	
	/**
	 * @brief Gives the same time to all non NULL Pixel in the Image.
	 * 
	 * @param time Time to give to all Pixel in the Image.
	 */
	void setTime(const double time);
	
	/**
	 * @brief Gives the same Date to all non NULL Pixel in the Image.
	 * 
	 * @param date Date to give to all Pixel in the Image.
	 */
	void setTime(const Date& date);
	
	/**
	 * @brief Gives the column and row of a Pixel given its Coordinate.
	 * 
	 * @param[in] coord Coordinate of the Pixel to be found.
	 * @param[out] col column of the nearest Pixel.
	 * @param[out] row row of the nearest Pixel.
	 */
	virtual Pixel const *const & geo2pix(const Coordinate& coord, size_t& col, size_t& row) const;
	
	/**
	 * @brief Finds the column and row of the nearest Pixel*.
	 * 
	 * This method use the algorithm of Newton to find the nearest Pixel of the given
	 * Coordinate.
	 * It converges usually in less than 10 iterations. Nevertheless
	 * it can miss of one or two Pixel.
	 * 
	 * Pixels in space should be set to NULL. They will be ignored.
	 *
	 * @param[in] coord Coordinate of the Pixel to be found.
	 * @param[out] col column of the nearest Pixel.
	 * @param[out] row row of the nearest Pixel.
	 * @param[in] scol column where to start search.
	 * @param[in] srow row where to start search.
	 * @return number of iterations needed to converge.
	 */
	unsigned findpixcoord(const Coordinate& coord, size_t& col, size_t& row, size_t* scol=NULL, size_t* srow=NULL) const;
	
	/**
	 * @brief Deallocates every Pixel in the Image.
	 *
	 * Every Pixel of the image is deallocated. Nevertheless the container array is not deallocated, all elements are set to NULL.
	 */
	void free();
	
	/**
	 * @brief Assignment operator.
	 *
	 * Every Pixel are copied in this Image.
	 * 
	 * @param[in] copy Image to be copied.
	 */
	Image& operator=(const Image& copy);
	
	protected:
	// Attributs
	Pixel** m_pixels; //!< Array of Pixel*.
	
	size_t m_ncols; //!< Number of columns of the Image.
	size_t m_nrows; //!< Number of rows of the Image.
};

/**
 * @brief Defines an Image which have a constant resolution.
 * 
 * 
 */
class ImageRegular : public Image
{
	public:
	/**
	 * @brief Constructor
	 * 
	 * If pixel_template is given, the ImageRegular will be filled with this king of Pixel.
	 * 
	 * @param[in] ncols Number of columns.
	 * @param[in] nrows Number of rows.
	 * @param[in] top_left Coordinate of the top left Pixel in the ImageRegular.
	 * @param[in] bottom_right Coordinate of the bottom right Pixel in the ImageRegular.
	 * @param[in] pixel_template if given, the contructor will fill the ImageRegular with this kind of pixels.
	 */
	ImageRegular(const size_t ncols, const size_t nrows, const Coordinate& top_left, const Coordinate& bottom_right, const Pixel *const pixel_template = NULL);
	
	virtual Pixel const *const & geo2pix(const Coordinate& coord, size_t& col, size_t& row) const;
	
	/**
	 * @brief Calculates Coordinate of the wanted cell.
	 * 
	 * This method do not return the Coordinate of a Pixel. The Coordinate is calulated from the Image size and resolution.
	 * This method can be used if no Pixel is set in this cell.
	 * 
	 * @param[in] col column in the ImageRegular.
	 * @param[in] row column in the ImageRegular.
	 * @return Coordinate of the cell.
	 */
	Coordinate pix2geo(const size_t col, const size_t row) const;
	
	/**
	 * @brief Calculates Coordinate of the wanted cell.
	 * 
	 * This method do not return the Coordinate of a Pixel. The Coordinate is calulated from the Image size and resolution.
	 * This method can be used if no Pixel is set in this cell.
	 * 
	 * @param[in] index index of the cell.
	 * @return Coordinate of the cell.
	 */
	Coordinate pix2geo(const size_t index) const;
	
	/**
	 * @brief Fill the ImageRegular with this kind of Pixel.
	 * 
	 * The Coordinate is given by pix2geo() method. The Pixel's satellite Coordinate is inherited from pixel_template and kept unchanged.
	 * You can use the Image::setVelue() method afterwards to enter values in each Pixel.
	 * 
	 * @param[in] pixel_template Template Pixel to be filled in the ImageRegular.
	 */
	void fill(const Pixel& pixel_template);
	
	/**
	 * @brief Returns the horizontal resolution of the ImageRegular in degrees per Pixel.
	 * @return horizontal resolution of the ImageRegular in degrees per Pixel.
	 */
	const double& xResolution() const;
	
	/**
	 * @brief Returns the vertical resolution of the ImageRegular in degrees per Pixel.
	 * @return vertical resolution of the ImageRegular in degrees per Pixel.
	 */
	const double& yResolution() const;
	
	protected:
	Coordinate m_top_left; //!< Coordinate of the top-left Pixel in the ImageRegular.
	Coordinate m_bottom_right; //!< Coordinate of the bottom-right Pixel in the ImageRegular.
	double m_xresolution; //!< Horizontal resolution of the ImageRegular in degrees per Pixel.
	double m_yresolution; //!< Vertical resolution of the ImageRegular in degrees per Pixel.
};

/**
 * @brief Image class that will use navgeo library.
 */
class ImageWithNavigationGeo : public Image
{
	public:
	// Constructeurs
	/**
	 * @brief Constructor.
	 *
	 * Creates an empty Image with the given dimensions
	 * 
	 * @param[in] navigation NavigationGeo object.
	 * @param[in] ncols Number of columns.
	 * @param[in] nrows Number of rows.
	 * @param[in] startCol First column of the Image.
	 * @param[in] startRow First row of the Image.
	 */
	ImageWithNavigationGeo(const navgeo::NavigationGeo& navigation, const size_t ncols, const size_t nrows, const size_t startCol=0, const size_t startRow=0);
	
	/**
	 * @brief Destructor.
	 *
	 * Deallocate every Pixel inside
	 */
	virtual ~ImageWithNavigationGeo();
	
	virtual const Pixel *const & geo2pix(const Coordinate& coord, size_t& col, size_t& row) const;
	
	/**
	 * @brief Fill the Image with this kind of Pixel.
	 * 
	 * The Coordinate is given by navigation().pix2geo() method. The Pixel's satellite Coordinate is inherited from pixel_template and kept unchanged.
	 * You can use the Image::setValue() method afterwards to enter values in each Pixel.
	 * 
	 * @param[in] pixel_template Template Pixel to be filled in the ImageRegular.
	 */
	void fill(const Pixel& pixel_template);
	
	/**
	 * @brief Returns the NavigationGeo object of the Image.
	 * @return the NavigationGeo object of the Image.
	 */
	const navgeo::NavigationGeo& navigation() const;
	
	protected:
	size_t m_startCol; //!< First col of the Image
	size_t m_startRow; //!< First row of the Image
	navgeo::NavigationGeo* m_navigation; //!< NavigationGeo object
};

#endif
