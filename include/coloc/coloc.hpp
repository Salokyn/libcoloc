#ifndef COLOC_HPP
#define COLOC_HPP
#include <coloc/pixel.hpp>
#include <coloc/orbit.hpp>
#include <coloc/image.hpp>
#include <bitset>
#include <utility>
#include <fstream>
#include <ctime>
#include <string>

/**
 * @brief Colocation context.
 *
 * This class helps configuring colocation process.
 *
 * The colocation uses the 2-dimensional integration method to estimate the ponderations of each Pixel. For each pixelA, a rectangle around it is defined. \f$ n_B \f$ pixelB's are found in that rectangle.
 * 
 * \image html MC.png "pixelA (green) and 10 pixelB's (red) found in the rectangle."
 * 
 * In this rectangle are set \f$ n_{IP} \f$ integration points. For each one, the PSF of the pixelA and of \f$ n_B \f$ selected pixelB's are calculated.
 * In m_link, for each PixelA are stored every colocated PixelB and the sum \f$ \sum\limits_{n_{IP}}(PSF_A \cdot PSF_B) \f$. Note that these sums are not normalized and can not be compared to other PixelA's ones.
 * 
 * The mean PSF given by meanPSF() is computed the following way. It can be considered as the number of layers of pixelB's that cover the pixelA.
 * \f[ \overline{PSF} = \frac{\sum\limits_{n_{IP}} \left ( \sum\limits_{n_B}(PSF_B) \cdot PSF_A \right )}{\sum\limits_{n_{IP}}(PSF_A)} \f]
 * 
 * In the following example, one colocates a CERES orbit with itself. The difference between original and colocated values should be very small.
 * @include coloc_ceres_ceres.cpp
 */
class Colocation
{
	public:
	
	/**
	 * @brief Flag indexes for colocation function.
	 */
	enum ConfigurationFlagIndex
	{
		COLOC_ZEN,      //!< Viewing zenithal angle colocation flag.
		COLOC_AZI,      //!< Viewing azimuthal angle colocation flag.
		COLOC_CONE,     //!< Conical angle colocation flag.
		COLOC_NO_NIGHT, //!< Night time Pixels will be ignored.
		COLOC_FORCE      //!< Colocation even if one of these flags are raised.
	};

	/**
	 * @brief Flag indexes telling why the pixel as been rejected.
	 */
	enum ColocationFlagIndex
	{
		FLAG_LAT,          //!< Latitude of the pixel is to close of the poles or outside the orbitB/imageB latitude span.
		FLAG_NIGHT,        //!< The pixel is in night side.
		FLAG_SELECT_TIME,  //!< No time match.
		FLAG_SELECT_COORD, //!< No coordinate match.
		FLAG_SELECT_ANGLE, //!< No angle match.
		FLAG_ROOTFINDING,  //!< Secant method probably diverged.
		FLAG_FPC,          //!< Findpixcoord failed.
		FLAG_NO_COLOC,     //!< No pixelB match this pixelA.
		FLAG_NO_PROCEEDB,  //!< Pixel::m_proceed flag was set to false.
		FLAG_NO_PROCEEDA,  //!< Pixel::m_proceed flag was set to false.
		FLAG_NULL,         //!< Pixel is NULL.
		FLAG_AZIMUTH,      //!< An azimuth angle is undifined.
		FLAG_INT_OFF       //!< Integration is turned off.
	};
	
	/**
	 * @brief Histogram normalization type.
	 */
	enum HistogramNormalizationType
	{
		NORM_OFF,          //!< No normalization. For each bin, weights of values entering this bin are accumulated.
		NORM_SUM,          //!< Normalization by the sum : accumulated weights are devided by \f$ \sum\limits_{n_B}(PSF_A \cdot PSF_B) \f$
		NORM_SURFACE,      //!< Normalization by the PixelA's surface : accumulated weights are devided by \f$ \frac{\sum\limits_{n_B}(PSF_A \cdot PSF_B)}{meanPSF()} \f$
	};
	
	/**
	 * @brief Integration Rule.
	 */
	enum IntegrationRule
	{
		INT_OFF,         //!< Integration is deactivated.
		INT_NEAREST,     //!< Nearest-neighbor selection.
		INT_SLAVE,       //!< Integration points are situated at each slave pixel center.
		INT_MONTECARLO,  //!< Monte Carlo integration rule.
		INT_TRAPEZOIDAL, //!< Trapezoidal integration rule.
		INT_SIMPSON      //!< Simpson's integration rule.
	};
	
	/**
	 * @brief Flags to set up the colocalisation.
	 * @see ConfigurationFlagIndex
	 */
	typedef std::bitset<5> ConfigurationFlag; // les bitsets sont initialisés à 0 par défaut.

	/**
	 * @brief Output flags.
	 * @see ColocationFlagIndex
	 */
	typedef std::bitset<13> ColocationFlag;
	
	/**
	 * @brief Pair of double
	 */
	typedef std::pair<double, double> dpair;
	
	/**
	 * @brief Pair of Pixel and double
	 */
	typedef std::pair<double, const Pixel*> dPpair;
	
	/**
	 * @brief Constructor.
	 * 
	 * Create a Colocation context to colocate an Orbit from an Orbit.
	 * 
	 * The run() method will call colocOrbit().
	 * 
	 * @param[in] orbitA Orbit to be colocated.
	 * @param[in] orbitB source Orbit. It is copied and sorted.
	 * @param[in] verbose verbal mode
	 */
	Colocation(const Orbit& orbitA, const Orbit& orbitB, const bool verbose = false);
	
	/**
	 * @brief Constructor.
	 * 
	 * Create a Colocation context to colocates an Orbit from an Image.
	 * 
	 * The run() method will call colocImage().
	 * 
	 * @param[in] orbitA Orbit to be colocated.
	 * @param[in] imageB source Image.
	 * @param[in] verbose verbal mode
	 */
	Colocation(const Orbit& orbitA, const Image& imageB, const bool verbose = false);
	
	virtual ~Colocation(); //!< Destructor
	
	/**
	 * @brief Returns the size of output vectors.
	 * 
	 * @return the size of output vectors.
	 */
	size_t size() const;
	
	/**
	 * @brief Returns the vector that contains colocated PixelB's.
	 * 
	 * @return a vector that contains colocated PixelB's.
	 */
	const std::vector< std::vector<dPpair> >& link() const;
	
	/**
	 * @brief Returns the mean PSF of the i-th Pixel.
	 * 
	 * \f[ \overline{PSF} = \frac{\sum\limits_{n_{IP}} \left ( \sum\limits_{n_B}(PSF_B) \cdot PSF_A \right )}{\sum\limits_{n_{IP}}(PSF_A)} \f]
	 * 
	 * @param[in] i index of the Pixel which mean PSF must be returned.
	 * @return the the mean PSF of the i-th Pixel.
	 */
	const double& meanPSF(const size_t i) const;
	
	/**
	 * @brief Returns the vector of mean PSF.
	 * 
	 * @return the the vector of mean PSF.
	 */
	const std::vector<double>& meanPSF() const;
	
	/**
	 * @brief Returns the reject flag of the i-th Pixel.
	 * 
	 * @param[in] i index of the Pixel which reject flag must be returned.
	 * @return the reject flag of the i-th Pixel.
	 */
	const ColocationFlag& flag(const size_t i) const;
	
	/**
	 * @brief Returns the vector of colocation flags.
	 * 
	 * @return the vector of colocation flags.
	 */
	const std::vector<ColocationFlag>& flag() const;
	
	/**
	 * @brief Returns the vector of reject flag as long.
	 * 
	 * @return the vector of reject flag.
	 */
	std::vector<unsigned long> flagUlong() const;
	
	/**
	 * @brief Returns the reject flag of the i-th Pixel as long.
	 * 
	 * @param[in] i index of the Pixel which reject flag must be returned.
	 * @return the reject flag of the i-th Pixel.
	 */
	unsigned long flagUlong(const size_t i) const;
	
	/**
	 * @brief Returns the number of integration points used to colocate the i-th Pixel.
	 * 
	 * @param[in] i index of the Pixel which the number of integration points must be returned.
	 * @return the number of integration points of the i-th Pixel.
	 */
	const size_t& nIntegrationPoints(const size_t i) const;
	
	/**
	 * @brief Returns the vector of number of integration points used to colocate each Pixel.
	 * 
	 * @return the number vector of integration points of each Pixel.
	 */
	const std::vector<size_t>& nIntegrationPoints() const;
	
	/**
	 * @brief Returns the number of colocated Pixels during the last run().
	 * 
	 * @return the number of colocated Pixels during the last run().
	 */
	const size_t& nColocatedPixels() const;
	
	/**
	 * @brief Returns the time threshold.
	 * 
	 * @return the time threshold.
	 */
	const double& timeThreshold() const;
	
	/**
	 * @brief Launch colocation.
	 * 
	 * Resizes the output vectors according to orbitA's size and calls colocOrbit() or colocImage(). Note that the output vectors are not cleared and may contain old values if run() is used several times. Use clear() to clear those.
	 */
	void run();
	
	/**
	 * @brief Clear the m_link, m_rejetc_flag and m_meanPSF vectors. Their size become 0.
	 */
	void clear();

	/**
	 * @brief Angular colocation.
	 *
	 * Tells if two Pixels have their angles colocated.
	 * 
	 * Use configure() method with COLOC_ZEN, COLOC_AZI or COLOC_CONE index to activate or deactivate the colocation criteria.
	 * Thresholds can be set with the methods setZenithThreshold(), setAzimuthThreshold() and setConeThreshold().
	 * 
	 * According to the Colocation configuration, this method will compare differences of scattering angle or viewing zentih angle and/or viewing azimuth. Scattering angle comparison has priority on the other criteria.
	 * If all colocation criterion are deactivated, the method will always return true.
	 * 
	 * The scattering angle (\f$ \gamma \f$) is defined from viewing zenithal angle (\f$ \zeta \f$) and viewing azimutal angle (\f$ \beta \f$) this way :
	 * \f[ \cos(\gamma) =  \cos \zeta_A \cdot \cos \zeta_B + \sin \zeta_A \cdot \sin \zeta_B \cdot \cos(\beta_A - \beta_B) \f]
	 * @image html cone.png "The scattering angle (γ) must be smaller than the threshold (θ)."
	 * 
	 * @param[in] pixelA a Pixel.
	 * @param[in] pixelB another Pixel.
	 * @return true if the two Pixels are colocated
	 */
	bool coloc_ang(const Pixel *const pixelA, const Pixel *const pixelB) const;
	
	/**
	 * @brief Sets the Colocation flags.
	 * @param[in] index index of the flag to change.
	 * @param[in] state value to be given to the index-th flag.
	 */
	void configure(const ConfigurationFlagIndex index, const bool state);
	
	/**
	 * @brief Returns the Colocation flags
	 * @return Colocation flags
	 */
	const ConfigurationFlag& config() const;
	
	/**
	 * @brief Returns the Colocation flags as unsigned long
	 * @return Colocation flags as unsigned long
	 */
	unsigned long configUlong() const;
	
	/**
	 * @brief Returns the integration rule
	 * @return Integration rule
	 */
	const IntegrationRule& integrationRule() const;
	
	/**
	 * @brief Sets Colocation flags
	 * @param[in] config Set colocation flags
	 */
	void setConfig(const ConfigurationFlag& config);
	
	void setIntegrationPointsPerB(const size_t n); //!< Sets the number of integration points per selected pixelB.
	void setIntegrationPointsMin(const size_t n);//!< Sets the min number of integration points.
	void setIntegrationPointsMax(const size_t n);//!< Sets the max number of integration points.
	void setRootFindingMax(const unsigned int rootFindingMax); //!< Sets the max number of iteration to be done in the root finding method.
	
	/**
	 * @brief Sets the time threshold.
	 * @param[in] deltaTime time threshold in days.
	 */
	void setTimeThreshold(const double deltaTime);
	
	/**
	 * @brief Sets the viewing zenith angle threshold.
	 * @param[in] zen threshold angle.
	 * @param[in] u unit of threshold angle.
	 */
	void setZenithThreshold(double zen, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Sets the viewing azimuth angle threshold.
	 * @param[in] azi threshold angle.
	 * @param[in] u unit of threshold angle.
	 */
	void setAzimuthThreshold(double azi, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Sets the scattering angle threshold.
	 * @param[in] cone threshold angle.
	 * @param[in] u unit of threshold angle.
	 */
	void setConeThreshold(double cone, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Sets the solar zenith angle threshold.
	 * @param[in] sza threshold angle.
	 * @param[in] u unit of threshold angle.
	 */
	void setNightThreshold(double sza, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Sets the verbal mode.
	 * @param[in] b true to be verbal, else false.
	 */
	void setVerbose(const bool b);
	
	/**
	 * @brief Sets the progress mode.
	 * @param[in] p interval between prints in percents (0 to disable).
	 */
	void setProgressionInterval(const double p);
	
	/**
	 * @brief Sets the integration rule to use.
	 * @param[in] rule integration rule to use.
	 */
	void setIntegrationRule(const IntegrationRule rule);
	
	/**
	 * @brief Opens the file where to store points.
	 * @param[in] fileIPout file where to write.
	 * @return a reference of the ofstream.
	 */
	std::ofstream& openIPout(const std::string& fileIPout);
	
	/**
	 * @brief Closes the file where to store Monte-Carlo shots.
	 * @return a reference of the ofstream.
	 */
	std::ofstream& closeIPout();
	
	/**
	 * @brief Returns the duration of the last colocation in seconds.
	 *
	 * Time mesured between the start and the end of the colocOrbit or colocImage method.
	 *
	 * @return duration of the last colocation in seconds.
	 */
	double duration() const;
	
	/**
	 * @brief Calls mean().
	 * 
	 * This method is virtual, it can be overloaded with any method you like.
	 * 
	 * @param[in] iOrbitA index of PixelA to proceed.
	 * @param[in] index index of the value to colocate in PixelA
	 */
	virtual double postTreatment(const size_t iOrbitA, const size_t index=0) const;
	
	/**
	 * @brief Returns the mean of the values of colocated PixelB's in the iOrbitA-th Pixel.
	 * 
	 * The mean is calculated this way :
	 * \f[ \mu_A = \frac{\sum\limits_{n_B} \left( \sum\limits_{n_{IP}}(PSF_A \cdot PSF_B) \cdot value_B \right )}{\sum\limits_{n_B} \left ( \sum\limits_{n_{IP}}(PSF_A \cdot PSF_B) \right )} \f]
	 * 
	 * @param[in] iOrbitA index of PixelA to proceed.
	 * @param[in] index index of the value to colocate in PixelA.
	 * @param[in] missingValue values that are equal to this value will be ignored.
	 * @return the averaged colocated value.
	 */
	double mean(const size_t iOrbitA, const size_t index=0, const double missingValue = NAN) const;
	
	/**
	 * @brief Returns the standard deviation of the values of colocated PixelB's in the iOrbitA-th Pixel.
	 * 
	 * The standard deviation is calculated this way :
	 * \f[ \sigma_A = \sqrt{ \frac{\sum\limits_{n_B} \left( \sum\limits_{n_{IP}}(PSF_A \cdot PSF_B) \cdot \left (value_B - mean() \right )^2 \right )}{\sum\limits_{n_B} \left ( \sum\limits_{n_{IP}}(PSF_A \cdot PSF_B) \right )} } \f]
	 * 
	 * @param[in] iOrbitA index of PixelA to proceed.
	 * @param[in] index index of the value to colocate in PixelA.
	 * @param[in] missingValue values that are equal to this value will be ignored.
	 * @return the stardard deviation.
	 */
	double stdev(const size_t iOrbitA, const size_t index=0, const double missingValue = NAN) const;
	
	/**
	 * @brief Returns the minimum of the values of colocated PixelB's in the iOrbitA-th Pixel.
	 * @param[in] iOrbitA index of PixelA to proceed.
	 * @param[in] index index of the value to colocate in PixelA.
	 * @param[in] missingValue values that are equal to this value will be ignored.
	 * @return the minimum.
	 */
	double min(const size_t iOrbitA, const size_t index=0, const double missingValue = NAN) const;
	
	/**
	 * @brief Returns the maximum of the values of colocated PixelB's in the iOrbitA-th Pixel.
	 * @param[in] iOrbitA index of PixelA to proceed.
	 * @param[in] index index of the value to colocate in PixelA.
	 * @param[in] missingValue values that are equal to this value will be ignored.
	 * @return the minimum.
	 */
	double max(const size_t iOrbitA, const size_t index=0, const double missingValue = NAN) const;
	
	/**
	 * @brief Returns a sorted (from greatest to least) histogram of pairs (weight, value) in bins of 1.
	 * 
	 * For every unique rounded PixelB value, weights are accumulated. Then these pairs (weight, value) are sorted according to the weights. The first pair is the most revelant one in the PixelA.
	 * 
	 * @param[in] iOrbitA index of PixelA to proceed.
	 * @param[in] index index of the value to colocate in PixelA.
	 * @param[in] normalization if set, the weights will be normalized.
	 * @param[in] missingValue values that are equal to this value will be ignored.
	 * @return a vector of pairs (weight, value).
	 */
	std::vector<dpair> histogram(const size_t iOrbitA, const size_t index=0, const HistogramNormalizationType normalization=NORM_OFF, const double missingValue=NAN) const;
	
	/**
	 * @brief Returns the value of histogram where the weight is the greater.
	 * 
	 * @param[in] iOrbitA index of PixelA to proceed.
	 * @param[in] index index of the value to colocate in PixelA.
	 * @return the mode of the colocated values.
	 */
	double mode(const size_t iOrbitA, const size_t index=0) const;
	
	static std::string version(); //!< Returns the version of the code.
	
	protected:
	
	/**
	 * @brief Colocates Pixels of orbitB in each Pixels of orbitA..
	 * 
	 * Colocates m_orbitB in m_orbitA. Calculates proportion of PixelB's entering each PixelA. The colocated PixelB's and their weights are stored in m_link.
	 * 
	 * This function can colocate 2 Orbits whatever where the Pixels come from. So you can colocate pixel from an Orbit to an geostationary image or from an image to an Orbit. In the last case you shoud use the other coloc function that is optimezed for.
	 */
	void colocOrbit();
	
	/**
	 * @brief Colocates Pixel of imageB in orbitA.
	 *
	 * Colocates m_orbitB in m_orbitA. Calculates proportion of PixelB's entering each PixelA. The colocated PixelB's and their weights are stored in m_link.
	 *
	 * This function is optimized to colocate a 2D Geostationary Image in an Orbit. Anyway, the Orbit-to-Orbit coloc function will work too. You just need to store Image's pixels in an Orbit.
	 */
	void colocImage();
	
	/**
	 * @brief 2D integration method.
	 * 
	 * @param[in] iOrbitA Index of the PixelA.
	 * @param[in] width Width of the rectangle around the PixelA.
	 * @param[in] height Height of the rectangle around the PixelA.
	 * @param[in] selectB Vector of PixelB.
	 */
	virtual void integration(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB);
	
	/**
	 * @brief Nearest co-location method.
	 * 
	 * With this method one seek for each master pixel the nearest slave pixel. The the meanPSF is calculated as the mean between the PSF of each pixel at the center of the other.
	 * 
	 * @param[in] iOrbitA Index of the PixelA.
	 * @param[in] width Width of the rectangle around the PixelA (useless here).
	 * @param[in] height Height of the rectangle around the PixelA (useless here).
	 * @param[in] selectB Vector of PixelB.
	 */
	void nearest(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB);
	
	/**
	 * @brief Monte-Carlo integration method.
	 * 
	 * @param[in] iOrbitA Index of the PixelA.
	 * @param[in] width Width of the rectangle around the PixelA.
	 * @param[in] height Height of the rectangle around the PixelA.
	 * @param[in] selectB Vector of PixelB.
	 */
	void montecarlo(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB);
	
	/**
	 * @brief Trapezoidal rule integration.
	 * 
	 * @param[in] iOrbitA Index of the PixelA.
	 * @param[in] width Width of the rectangle around the PixelA.
	 * @param[in] height Height of the rectangle around the PixelA.
	 * @param[in] selectB Vector of PixelB.
	 */
	void trapezoidal(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB);
	
	/**
	 * @brief Simpson's integration rule.
	 * 
	 * @param[in] iOrbitA Index of the PixelA.
	 * @param[in] width Width of the rectangle around the PixelA.
	 * @param[in] height Height of the rectangle around the PixelA.
	 * @param[in] selectB Vector of PixelB.
	 */
	void simpson(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB);
	
	/**
	 * @brief Nagle-like integration method.
	 * 
	 * In the Nagle-like method. meanPSF is the average of the master pixel PSF at the center of every slave pixels around.
	 * 
	 * @param[in] iOrbitA Index of the PixelA.
	 * @param[in] width Width of the rectangle around the PixelA.
	 * @param[in] height Height of the rectangle around the PixelA.
	 * @param[in] selectB Vector of PixelB.
	 */
	void intSlave(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB);
	
	ConfigurationFlag m_config; //!< Colocation flags. Use configure() to change.
	
	const Orbit* m_orbitA; //!< Orbit to be colocated.
	Orbit* m_orbitB; //!< Source Orbit.
	const Image* m_imageB; //!< Source Image.
	bool m_verbose; //!< If true, the Calocation will be verbal.
	
	double m_progression_interval; //!< If non zero, the Calocation will print the progression in every m_progression percent.
	
	std::ofstream m_fileIPout; //!< File to eventually store points of integration.
	
	std::vector<float> m_randomPool; //!< Vector of random values to be picked up for MonteCarlo.
	
	std::vector<double> m_meanPSF; //!< Vector to store mean PSF of each Pixel.
	std::vector<ColocationFlag> m_colocationFlag; //!< Vector to store the ColocationFlag of each Pixel.
	std::vector<size_t> m_nIP; //!< Vector to store the number of points of integration for each Pixel.
	std::vector< std::vector<dPpair> > m_link; //!< Vector to store colocated PixelB's and their ponderation in each PixelA.
	
	size_t m_irand; //!< Index of the random current value.
	size_t m_IPpB; //!< Number of integration points per selected pixelB.
	size_t m_IPmin; //!< Min number of integration points.
	size_t m_IPmax; //!< Max number of integration points.
	unsigned int m_rootFindingMax; //!< Max number of iteration to be done in or Orbit::dichotomy() method.
	
	IntegrationRule m_integrationRule; //!< Integration rule.
	
	double m_timeThreshold; //!< Time threshold in days.
	double m_zenithThreshold; //!< Viewing zenith angle threshold in degrees.
	double m_azimuthThreshold; //!< Viewing azimuth angle threshold in degrees.
	double m_coneThreshold; //!< Scattering angle threshold in degrees (half-aperture of the cone).
	double m_nightThreshold; //!< Maximum solar zenith angle in degrees to consider Pixel as in daytime.
	
	time_t m_startTime; //!< Start time of colocation.
	time_t m_endTime; //!< End time of colocation.
	
	size_t m_nColocatedPixels; //!< Number of colocated Pixels during the last run().
	
	static constexpr size_t default_nbrandMax = 1000000; //!< Default maximum number of generated random numbers.
	static constexpr size_t default_IPpB  = 5; //!< Default IPpB.
	static constexpr size_t default_IPmin = 10; //!< Default IPmin.
	static constexpr size_t default_IPmax = 100; //!< Default IPmax.
	static constexpr size_t default_rootFindingMax = 50; //!< Default rootFindingMax.
	
	static constexpr double default_timeThreshold = 1e30; //!< Default time threshold in days.
	static constexpr double default_zenithThreshold = 10.; //!< Default viewing zenith angle threshold in degrees.
	static constexpr double default_azimuthThreshold = 10.; //!< Default viewing azimuth angle threshold in degrees.
	static constexpr double default_coneThreshold = 10.; //!< Default scattering angle threshold in degrees (aperture of the cone).
	static constexpr double default_nightThreshold = 80.; //!< Default maximum solar zenith angle in degrees to consider Pixel as in daytime.
	static constexpr IntegrationRule default_integrationRule = INT_MONTECARLO; //!< Default integration rule.
};

#endif
