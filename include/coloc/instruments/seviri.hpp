#ifndef SEVIRI_HPP
#define SEVIRI_HPP

#include <coloc/image.hpp>
#include <coloc/pixel.hpp>

/**
 * @brief Defines the SEVIRI Geostationary Pixel.
 */
class Pixel_SEVIRI : public PixelPinchCos
{
	public:
	// Constructeurs
	Pixel_SEVIRI(); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_SEVIRI(const Coordinate& coord, const Coordinate satCoord=Coordinate(navgeo::SEVIRI::default_ssp,0.)); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_SEVIRI(const double time, const Coordinate& coord, const Coordinate satCoord=Coordinate(navgeo::SEVIRI::default_ssp,0.)); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	virtual Pixel_SEVIRI* clone() const; // Constructeur vituel
	
	// Attribute
	static constexpr double default_relativeAltitude = 6.610739; //!< Default relative altitude.
	static constexpr ScanMode default_scanMode = GEO; //!< Default scan mode.
	static constexpr double default_radius = 6e3; //!< Pixel FWHM in m. http://articles.adsabs.harvard.edu//full/2000ESASP.452...29J/0000032.000.html
};

/**
 * @brief Defines the SEVIRI Geostationary Image.
 */
class Image_SEVIRI : public ImageWithNavigationGeo
{
	public:
	static constexpr size_t default_nrows = 3712; //!< Default number of rows in the Image
	static constexpr size_t default_ncols = 3712; //!< Default number of columns in the Image
	
	/**
	 * @brief constructor
	 * @param[in] rotated False if image is from top to bottom, True if bottom to top (scan direction).
	 * @param[in] ncols Number of columns.
	 * @param[in] nrows Number of rows.
	 * @param[in] startCol First column of the Image.
	 * @param[in] startRow First row of the Image.
	 */
	Image_SEVIRI(const bool rotated = false, const size_t ncols=default_ncols, const size_t nrows=default_nrows, const size_t startCol=0, const size_t startRow=0);
};

#endif
