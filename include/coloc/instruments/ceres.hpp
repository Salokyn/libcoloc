#ifndef CERES_HPP
#define CERES_HPP

#include <coloc/pixel.hpp>

/*
altitude Aqua = 699.553345km
altitude Terra =699.593811
*/

/**
 * @brief Defines the CERES Pixel.
 */
class Pixel_CERES : public Pixel
{
	public:
	// Constructeurs
	Pixel_CERES(); //!< Calls Pixel::Pixel() and sets default attributes.
	Pixel_CERES(const Coordinate& coord); //!< Calls Pixel::Pixel() and sets default attributes.
	Pixel_CERES(const Coordinate& coord, const Coordinate& satCoord); //!< Calls Pixel::Pixel() and sets default attributes.
	Pixel_CERES(const double time, const Coordinate& coord); //!< Calls Pixel::Pixel() and sets default attributes.
	Pixel_CERES(const double time, const Coordinate& coord, const Coordinate& satCoord); //!< Calls Pixel::Pixel() and sets default attributes.
	virtual Pixel_CERES* clone() const; // Constructeur vituel

	// Méthodes
	/**
	 * The CERES PSF is based on G. Louis Smith paper "Effect of time response on the point spread function of a scanning radiometer"
	 * 
	 * The the following expression, along-scan component (<var>a</var>) and cross-scan component (<var>c</var>) are in km (sub satellite).
	 * 
	 * \f[ psf(a, c) = \left\{ 
	 * \begin{array}{l l}
	 * \frac{1}{2} \left ( \cos \left (\frac{|a|}{18.49} \cdot \pi \right ) +1 \right ) * max \left ( min \left ( \frac{1-\frac{|c|}{16.13}}{0.2}, 1. \right ), 0. \right ) & 
	 * \quad \mbox{if } |a| \leq 18.49 \mbox{ and } |c| \leq 16.13 \\
	 * 0 & \quad \mbox{else} \\
	 * \end{array} \right. \f]
	 * 
	 * @image html ceres_psf.png "CERES' pixel point spread function."
	 */
	virtual double psf(const Coordinate& coord) const;
	virtual double radius() const; // Renvoie le rayon du disque dans lequel le pixel au Nadir est inscrit, c'est une constante propre au pixel
	virtual double surface() const;
	
	// Attribute
	static constexpr double default_relativeAltitude = 1.109687; //!< Default relative altitude taken from Ixion 33.26.
	static constexpr ScanMode default_scanMode = XT; //!< Default scan mode.
	static constexpr bool default_symmetric = false; //!< Default symmetry.
	
	protected:
	// Attributs

};

#endif
