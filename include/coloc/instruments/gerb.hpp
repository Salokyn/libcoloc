#ifndef GERB_HPP
#define GERB_HPP

#include <coloc/pixel.hpp>
#include <coloc/image.hpp>

/**
 * @brief Defines the GERB Geostationary Pixel.
 */
class Pixel_GERB : public PixelPinchCos
{
	public:
	// Constructeurs
	Pixel_GERB(); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_GERB(const Coordinate& coord, const Coordinate satCoord=Coordinate(default_ssp,0.)); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_GERB(const double time, const Coordinate& coord, const Coordinate satCoord=Coordinate(default_ssp,0.)); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	virtual Pixel_GERB* clone() const; // Constructeur vituel
	
	// Attribute
	static constexpr double default_relativeAltitude = 6.610739; //!< Default relative altitude.
	static constexpr ScanMode default_scanMode = GEO; //!< Default scan mode.
	static constexpr double default_radius = 44.5e3; //!< Pixel FWHM in m. Need to be confirmed.
	static constexpr double default_ssp = 0.; //!< Default Sub-satellite point.
};

/**
 * @brief Defines the GERB Geostationary Image.
 */
class Image_GERB : public Image
{
	public:
	// Const
	static constexpr size_t default_nrows = 256; //!< Default number of rows in the Image
	static constexpr size_t default_ncols = 256; //!< Default number of columns in the Image
	
	/**
	 * @brief constructor
	 * 
	 * @param[in] ncols Number of columns of the Image_GERB.
	 * @param[in] nrows Number of rows of the Image_GERB.
	 */
	Image_GERB(const size_t ncols = default_ncols, const size_t nrows = default_nrows);
	
	virtual const Pixel *const & geo2pix(const Coordinate& coord, size_t& col, size_t& row) const;
	
	/**
	 * @brief Calculates GERB column and row from lon/lat.
	 * 
	 * @param[in] rlat Latitude in degrees.
	 * @param[in] rlong Longitude in degrees.
	 * @param[out] line Line index of the image.
	 * @param[out] pixel Pixel index on the line.
	 * @param[in] nlines Number of lines of the image.
	 * @param[in] nsamps Number of columns of the image.
	 */
	static int georef(double rlat, double rlong, int &line, int &pixel, const int nlines = default_nrows, const int nsamps = default_ncols);
};

#endif
