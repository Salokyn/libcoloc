#ifndef SCARAB_HPP
#define SCARAB_HPP

#include <coloc/pixel.hpp>

/**
 * @brief Defines the ScaRaB Pixel.
 */
class Pixel_ScaRaB : public Pixel
{
	public:
	// Constructeurs
	Pixel_ScaRaB(); //!< Calls Pixel::Pixel() and sets default attributes.
	Pixel_ScaRaB(const Coordinate& coord); //!< Calls Pixel::Pixel() and sets default attributes.
	Pixel_ScaRaB(const Coordinate& coord, const Coordinate& satCoord); //!< Calls Pixel::Pixel() and sets default attributes.
	Pixel_ScaRaB(const double time, const Coordinate& coord); //!< Calls Pixel::Pixel() and sets default attributes.
	Pixel_ScaRaB(const double time, const Coordinate& coord, const Coordinate& satCoord); //!< Calls Pixel::Pixel() and sets default attributes.
	virtual Pixel_ScaRaB* clone() const; // Constructeur vituel

	// Méthodes
	/**
	 * The ScaRaB PSF is given in Steven Dewitte paper "Determination of the ScaRaB FM1 Broadband Channel Point Spread Functions"
	 * 
	 * In the the following expressions, along-scan component \f$ \delta_a \f$ and cross-scan component \f$ \delta_c \f$ are in degrees.
	 * \f[ g1(\delta_a, \delta_c) = max \left ( min \left ( \frac{\delta_{right}-\left | \delta_a \right |}{\delta_{right}-\delta_{left}}, 1. \right ), 0. \right ) \f]
	 * where \f[ \delta_{left} = \left | 1.91°-0.63°-\left | \delta_c \right | \right | \f] and \f[ \delta_{right} = 1.91°+0.63°-\left | \delta_c \right | \f]
	 * \f[ g2(\delta_c) = max \left ( min \left ( \frac{1.91°-\left | \delta_c \right |}{0.63°}, 1. \right ), 0. \right ) \f]
	 * 
	 * \f[ psf(\delta_a, \delta_c) = g1(\delta_a, \delta_c ) \cdot g2(\delta_c) \f]
	 * 
	 * I convert distances (<var>a</var>, <var>c</var>) to angles (\f$ \delta_a \f$,\f$ \delta_c \f$) the following way :
	 * \f[ \delta_a = a \cdot \arctan(altitude()) \f]
	 * \f[ \delta_c = c \cdot \arctan(altitude()) \f]
	 * 
	 * @image html scarab_psf.png "ScaRaB's pixel point spread function."
	 */
	virtual double psf(const Coordinate& coord) const;
	virtual double radius() const; // Renvoie le rayon du disque dans lequel le pixel au Nadir est inscrit, c'est une constante propre au pixel
	virtual double surface() const;

	static constexpr double default_relativeAltitude = 1.135705; //!< Default relative altitude taken from Ixion 33.26.
	static constexpr ScanMode default_scanMode = XT; //!< Default scan mode.
	static constexpr bool default_symmetric = false; //!< Default symmetry.
	
	protected:
	// Attributs
	// Méthodes
	static double g1(const double a, const double c); //!< Method used to calculate ScaRaB PSF.
	static double g2(const double c); //!< Method used to calculate ScaRaB PSF.
};

#endif
