#ifndef BRAIN_HPP
#define BRAIN_HPP

#include <coloc/pixel.hpp>

/**
 * @brief Definition of the Pixel of the BRAIN product
 * 
 * Its distortion factors are set to 1.
 */
class Pixel_BRAIN : public PixelCircle
{
	public:
	// Constructeurs
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and sets default attributes.
	 * 
	 * @param[in] radius radius of the PixelCircle.
	 */
	Pixel_BRAIN(const double radius);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and sets default attributes.
	 * 
	 * @param[in] radius radius of the PixelCircle.
	 * @param[in] coord Coordinate of the Pixel.
	 */
	Pixel_BRAIN(const double radius, const Coordinate& coord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and sets default attributes.
	 * 
	 * @param[in] radius radius of the PixelCircle.
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	Pixel_BRAIN(const double radius, const Coordinate& coord, const Coordinate& satCoord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and sets default attributes.
	 * 
	 * @param[in] radius radius of the PixelCircle.
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Coordinate of the Pixel.
	 */
	Pixel_BRAIN(const double radius, const double time, const Coordinate& coord);
	
	/**
	 * @brief Constructor. Calls Pixel::Pixel() and sets default attributes.
	 * 
	 * @param[in] radius radius of the PixelCircle.
	 * @param[in] time Julian day of the Pixel.
	 * @param[in] coord Coordinate of the Pixel.
	 * @param[in] satCoord Coordinate of the satellite.
	 */
	Pixel_BRAIN(const double radius, const double time, const Coordinate& coord, const Coordinate& satCoord);
	
	virtual Pixel_BRAIN* clone() const; // Constructeur vituel

	// Méthodes
	virtual double psf(const Coordinate& coord) const;
	
	/**
	 * @brief Gaussian function
	 * 
	 * \f[ f(x) = e^{- { \frac{x^2}{2 \sigma^2} } } \f]
	 * 
	 * @param[in] x variable.
	 * @param[in] sigma half-height width.
	 * @return Gaussian function.
	 */
	static double gaussian(const double x, const double sigma);
	
	// Attribute
	static constexpr ScanMode default_scanMode = CONICAL; //!< Default scan mode.
	static constexpr double default_relativeAltitude = 1e30; //!< Default relative altitude.
};

#endif
