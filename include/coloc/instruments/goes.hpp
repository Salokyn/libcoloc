#ifndef GOES_HPP
#define GOES_HPP

#include <coloc/image.hpp>
#include <coloc/pixel.hpp>

/**
 * @brief Defines the SEVIRI Geostationary Pixel.
 */
class Pixel_GOES : public PixelCircle
{
	public:
	// Constructeurs
	Pixel_GOES(); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_GOES(const Coordinate& coord); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_GOES(const Coordinate& coord, const Coordinate& satCoord); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_GOES(const double time, const Coordinate& coord); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_GOES(const double time, const Coordinate& coord, const Coordinate& satCoord); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	virtual Pixel_GOES* clone() const; // Constructeur vituel
	
	// Attribute
	static constexpr double default_relativeAltitude = 6.610739; //!< Default relative altitude.
	static constexpr ScanMode default_scanMode = GEO; //!< Default scan mode.
	static constexpr double default_radius = 2e3; //!< Pixel radious in m for channels 5.8µm to 11.20µm.
};

/**
 * @brief Defines the Generic GOES Geostationary Image.
 */
class Image_GOES : public ImageWithNavigationGeo
{
	public:
	static constexpr size_t default_nrows = 2806; //!< Default number of rows in the Image
	static constexpr size_t default_ncols = 2806; //!< Default number of columns in the Image
	
	/**
	 * @brief constructor
	 * @param[in] ssp Sub-satellite point longitude.
	 * @param[in] ncols Number of columns.
	 * @param[in] nrows Number of rows.
	 * @param[in] startCol First column of the Image.
	 * @param[in] startRow First row of the Image.
	 */
	Image_GOES(const double ssp, const size_t ncols=default_ncols, const size_t nrows=default_nrows, const size_t startCol=0, const size_t startRow=0);
};
#endif
