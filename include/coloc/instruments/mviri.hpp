#ifndef MVIRI_HPP
#define MVIRI_HPP

#include <coloc/pixel.hpp>
#include <coloc/image.hpp>
#include <navgeo/navgeo.hpp>

/**
 * @brief Defines the MVIRI Geostationary Pixel.
 */
class Pixel_MVIRI : public PixelCircle
{
	public:
	// Constructeurs
	Pixel_MVIRI(); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_MVIRI(const Coordinate& coord, const Coordinate satCoord=Coordinate(navgeo::MVIRI::default_ssp,0.)); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_MVIRI(const double time, const Coordinate& coord, const Coordinate satCoord=Coordinate(navgeo::MVIRI::default_ssp,0.)); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	virtual Pixel_MVIRI* clone() const; // Constructeur vituel
	
	// Attribute
	static constexpr double default_relativeAltitude = 6.610739; //!< Default relative altitude taken from Ixion 33.26.
	static constexpr ScanMode default_scanMode = GEO; //!< Default scan mode.
	static constexpr double default_radius = 5e3; //!< Pixel radius in m. Need to be confirmed.
	
	protected:
	// Attributs
};

/**
 * @brief Defines the MVIRI Geostationary Image.
 */
class Image_MVIRI : public Image
{
	public:
	// Const
	static constexpr size_t default_nrows = 2500; //!< Default number of rows in the Image
	static constexpr size_t default_ncols = 2500; //!< Default number of columns in the Image
	
	/**
	 * @brief constructor
	 */
	Image_MVIRI();
	
	/**
	 * @brief Gives the column and row of a Pixel given its Coordinate.
	 * 
	 * @param[in] coord Coordinate of the Pixel to be found.
	 * @param[out] col column of the nearest Pixel.
	 * @param[out] row Row of the nearest Pixel.
	 * @param[in] ssp Sub-satellite point's longitude
	 */
	virtual const Pixel *const & geo2pix(const Coordinate& coord, size_t& col, size_t& row, double ssp = navgeo::MVIRI::default_ssp) const;
	
	/**
	 * @brief Calculates MVIRI column and row from lon/lat.
	 *
	 * @param[in] rlat Latitude of the Pixel in degrees.
	 * @param[in] rlong Longitude of the Pixel in degrees.
	 * @param[out] line Line index of the image.
	 * @param[out] pixel Pixel index on the line.
	 * @param[in] ssp Sub-satellite longitude.
	 */
	static int georef(double rlat, double rlong, int &line, int &pixel, double ssp = 0.);
};

#endif
