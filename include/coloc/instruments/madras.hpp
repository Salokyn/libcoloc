#ifndef MADRAS_HPP
#define MADRAS_HPP

#include <coloc/pixel.hpp>

/**
 * @brief Defines the Madras Geostationary Pixel.
 */
class Pixel_Madras : public PixelCircle
{
	public:
	// Constructeurs
	Pixel_Madras(); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_Madras(const Coordinate& coord); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_Madras(const Coordinate& coord, const Coordinate& satCoord); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_Madras(const double time, const Coordinate& coord); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	Pixel_Madras(const double time, const Coordinate& coord, const Coordinate& satCoord); //!< Calls PixelCircle::PixelCircle() and sets default attributes.
	virtual Pixel_Madras* clone() const; // Constructeur vituel
	
	// Attribute
	static constexpr double default_relativeAltitude = 1.135705; //!< Default relative altitude taken from Ixion 33.30.
	static constexpr ScanMode default_scanMode = CONICAL; //!< Default scan mode.
	static constexpr double default_radius = 3267.; //!< Pixel radius in m for channel 89GHz. Synthesis of the MADRAS characteristics relevant for radiative transfert application, N. Viltard, 2007 (K=2.573, L=1.529)
};

#endif
