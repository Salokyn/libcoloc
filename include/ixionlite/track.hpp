#ifndef TRACK_HPP
#define TRACK_HPP

#include <vector>
#include <geo.hpp>
#include <coloc/pixel.hpp>

struct TrackElement
{
	TrackElement(); //!< Default constructor
	TrackElement(const double t, const Coordinate& c, const double i); //!< constructor
	TrackElement(const Pixel& p); //!< constructor
	static bool less(const TrackElement& a, const TrackElement& b);
	
	double time;
	Coordinate coord;
	double trackInclination;
};

typedef std::vector< TrackElement > Track;

#endif
