#ifndef SWATHGENERATOR_HPP
#define SWATHGENERATOR_HPP
#include <coloc/orbit.hpp>
#include <geo.hpp>

/**
 * @brief Swath generator class
 */
class SwathGenerator
{
	public:
	SwathGenerator(const Pixel& pixel); //!< Constructor
	
	virtual ~SwathGenerator(); //!< Destructor
	
	/**
	 * @brief Generates the Pixels of the Swath.
	 * 
	 * @return An Orbit containing m_size Pixel.
	 */
	virtual Orbit get() const = 0;
	
	/**
	 * @brief Sets the number of Pixel in a swath.
	 * 
	 * @param[in] i Number of Pixel in a swath.
	 */
	void setSize(const size_t i);
	
	/**
	 * @brief Sets the inclination of the swath.
	 * 
	 * @param[in] i Inclination of the swath.
	 * @param[in] u is the unit of the angle.
	 */
	void setInclination(double i, const geo::unit_angle u=geo::DEGREE);
	
	/**
	 * @brief Sets the maximum viewing zenith angle of the swath.
	 * 
	 * @param[in] z Maximum viewing zenith angle of the swath.
	 * @param[in] u is the unit of the angle.
	 */
	void setViewingZenithAngle(double z, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Sets the maximum swath angle.
	 * 
	 * @param[in] f Maximum swath angle.
	 * @param[in] u is the unit of the angle.
	 */
	void setSwathAngle(double f, const geo::unit_angle u = geo::DEGREE);
	
	/**
	 * @brief Cast operator.
	 * 
	 * Calls get().
	 */
	operator Orbit() const;
	
	protected:
	const Pixel* m_pixel; //!< Template Pixel
	size_t m_size; //!< Number of pixels in a swath.
	double m_inclination; //!< Inclination of the Swath.
	double m_swathAngle; //!< Width of the swath as seen from the satellite in radians.
};

/**
 * @brief Planar Swath generator class
 * 
 * @image html PlanarSwath.png "Planar Swath. f stands for swath angle."
 */
class PlanarSwathGenerator : public SwathGenerator
{
	public:
	/**
	 * @brief Default constructor
	 * 
	 * The Swath angle and the half-aperture angle are initialized from the given Pixel.
	 * 
	 * @param[in] pixel Template Pixel. By default, this Pixel is considered as the last Pixel of the planar Swath.
	 */
	PlanarSwathGenerator(const Pixel& pixel);
	virtual ~PlanarSwathGenerator();
	
	/**
	 * @brief Generates the Pixels of the Swath.
	 * @return An Orbit containing m_size Pixel.
	 *
	 * Generates m_size Pixels from -m_swathAngle to +m_swathAngle.
	 */
	virtual Orbit get() const;
};

/**
 * @brief Conical Swath generator class
 * 
 * @image html ConicalSwath.png "Conical Swath. f stands for swath angle. θ stands for half aperture angle."
 */
class ConicalSwathGenerator : public SwathGenerator
{
	public:
	/**
	 * @brief Default constructor
	 * 
	 * The Swath angle and half-aperture angle are initialized from the given Pixel.
	 * 
	 * @param[in] pixel Template Pixel. By default, this Pixel is considered as the Pixel at the center of the conical Swath.
	 */
	ConicalSwathGenerator(const Pixel& pixel);
	virtual ~ConicalSwathGenerator();
	
	/**
	 * @brief Sets the half-aperture of the conical Swath.
	 * 
	 * @param[in] a half-aperture of the conical Swath.
	 * @param[in] u is the unit of the angle.
	 */
	void setHalfApertureAngle(double a, const geo::unit_angle u=geo::DEGREE);
	
	/**
	 * @brief Generates the Pixels of the Swath.
	 * @return An Orbit containing m_size Pixel.
	 *
	 * Generates m_size Pixels from -m_halfApertureAngle to +m_halfApertureAngle. Swath angle remains constant.
	 */
	virtual Orbit get() const;
	
	protected:
	double m_halfApertureAngle; //!< Half-aperture of the Swath.
};
#endif
