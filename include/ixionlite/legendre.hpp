#ifndef LEGENDRE_HPP
#define LEGENDRE_HPP

#include <stdexcept>

namespace Legendre
{
	double P(int n, double x);
	double P0(double x);
	double P1(double x);
	double P2(double x);
	double P3(double x);
	double P4(double x);
	double P5(double x);
}

#endif
