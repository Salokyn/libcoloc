#ifndef PLANET_HPP
#define PLANET_HPP

class Planet
{
	public:
	Planet(); //!< Default constructor
	
	/**
	 * @brief Returns the equatorial radius in meters.
	 * @return Equatorial radius.
	 */
	const double& equatorialRadius() const;
	
	/**
	 * @brief Returns the flattening.
	 * @return Flattening.
	 */
	const double& flattening() const;
	
	/**
	 * @brief Returns the sideral year duration in days.
	 * @return Sideral year duration in days.
	 */
	const double& meanDay() const;
	
	/**
	 * @brief Returns the mean day duration in seconds.
	 * @return Mean day duration in seconds.
	 */
	const double& sideralYear() const;
	
	/**
	 * @brief Returns the mass in kg.
	 * @return Mass in kg.
	 */
	const double& mass() const;
	
	/**
	 * @brief Calculates and returns the geocentric gravitational constant in \f$ m^3 \cdot s^{-2} \f$.
	 * @return Geocentric gravitational constant in \f$ m^3 \cdot s^{-2} \f$.
	 * 
	 * \f[ \mu = G*M \f]
	 */
	double geocentricGravitationalConstant() const;
	
	/**
	 * @brief Calculates and returns the polar radius in meters.
	 * @return Mean day duration in seconds.
	 * 
	 * \f[ r_{pol} = (1-f) \cdot r_{eq} \f]
	 */
	double polarRadius() const;
	
	/**
	 * @brief Calculates and returns the revolution speed \f$ \dot{\Omega_S} \f$ in \f$ rad \cdot s^{-1} \f$.
	 * @return Revolution speed in \f$ rad \cdot s^{-1} \f$.
	 */
	double revolutionSpeed() const;
	
	/**
	 * @brief Calculates and returns the rotation speed \f$ \varpi = \dot{\Omega_T} \f$ in \f$ rad \cdot s^{-1} \f$.
	 * @return Rotation speed in \f$ rad \cdot s^{-1} \f$.
	 */
	double rotationSpeed() const;
	
	/**
	 * @brief Calculates and returns the eccentricity.
	 * @return Excentricity.
	 */
	double eccentricity() const;
	
	const double& J2() const;
	const double& J3() const;
	const double& J4() const;
	
	protected:
	double m_equatorialRadius; //!< Equatorial radius in meters.
	double m_flattening; //!< Flattening. \f[ f_{earth} = 1-\frac{rpol_{earth}}{req_{earth}} \f]
	double m_meanDay; //!< Mean day duration in seconds.
	double m_sideralYear; //!< Sideral year duration in days.
	double m_mass; //!< Mass in kg.
	
	double m_J2;
	double m_J3;
	double m_J4;
};

class Earth : public Planet
{
	public:
	Earth(); //!< Default constructor
	
	static const double default_equatorialRadius = 6378136.3; //!< Default equatorial radius of the Earth.
	static const double default_flattening = 1./298.25765; //!< Default flattening of the Earth.
	static const double default_meanDay = 86400.; //!< Default mean day duration of the Earth.
	static const double default_sideralYear = 365.256360;  //!< Default sideral year duration duration of the Earth.
	static const double default_mass = 5.973698991e24;  //!< Default mass of the Earth.
	
	static const double default_J2 = 1.0826362e-3;
	static const double default_J3 = -2.53615069e-6;
	static const double default_J4 = -1.61936555e-6;
};

class Mars : public Planet
{
	public:
	Mars(); //!< Default constructor
	
	static const double default_equatorialRadius = 3396200; //!< Default equatorial radius of Mars.
	static const double default_flattening = 1./169.8; //!< Default flattening of Mars.
	static const double default_meanDay = 88775.245; //!< Default mean day duration of Mars.
	static const double default_sideralYear = 686.98;  //!< Default sideral year duration duration of Mars.
	static const double default_mass = 6.4185e23;  //!< Default mass of Mars.
	
	static const double default_J2 = 1.960e-3;
	static const double default_J3 = 36.0e-6;
	static const double default_J4 = 32.0e-6;
};


#endif
