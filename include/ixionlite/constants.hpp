#ifndef ASTRO_CONSTANTS_HPP
#define ASTRO_CONSTANTS_HPP

namespace AstronomicalConstants
{
	static const double G = 6.67259e-11; //!< Universal constant of gravitation in \f$ m^{-3} \cdot s^{-2} \cdot kg^{-1} \f$.
	static const double k = .01720209895; //!< Gauss gravitational constant.
	static const double c = 299792458.; //!< Speed of light in \f$ m \cdot s^{-1} \f$.
	static const double au = 1.4959787061e11; //!< Astronominacal unit in meters.
	static const double solarMass = 1.98892e30; //!< Sun mass in kg.
}

#endif
