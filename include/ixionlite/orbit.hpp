#ifndef ORBITGENERATOR_HPP
#define ORBITGENERATOR_HPP

#include "planet.hpp"
#include "cartesian.hpp"
#include "track.hpp"
#include <geo.hpp>


class OrbitGenerator
{
	public:
	/**
	 * @brief Constructor
	 * @param[in] planet Planet around wich is the Orbit.
	 */
	OrbitGenerator(const Planet& planet=Earth());
	
	/**
	 * @brief Constructor
	 * @param[in] radius Orbit radius in meters.
	 * @param[in] inclination Orbit inclination in radians.
	 * @param[in] eccentricity Eccentricity of the Orbit.
	 * @param[in] planet Planet around wich is the Orbit.
	 */
	OrbitGenerator(const double radius, const double inclination=0., const double eccentricity=0., const Planet& planet=Earth());
	
	/**
	 * @brief Constructor
	 * 
	 * Experimental: Compute orbital parameters from the given track. The track must cross the equator at least once.
	 * 
	 * @param[in] track Track from wich orbital parameters must be guessed.
	 * @param[in] inclination Orbit inclination in radians.
	 * @param[in] planet Planet around wich is the Orbit.
	 */
	OrbitGenerator(const Track& track, const double inclination=NAN, const Planet& planet=Earth());
	
	/**
	 * @brief Returns the radius \f$ a \f$ of the Orbit in meters.
	 * @return Radius of the Orbit.
	 */
	const double& radius() const;
	
	/**
	 * @brief Returns the inclination \f$ i \f$ of the Orbit in radians.
	 * @return Inclination of the Orbit.
	 */
	const double& inclination() const;
	
	/**
	 * @brief Returns the eccentricity \f$ z \f$ of the Orbit.
	 * @return Eccentricity of the Orbit.
	 */
	const double& eccentricity() const;
	
	/**
	 * @brief Returns the Planet of the Orbit.
	 * @return Planet of the Orbit.
	 */
	const Planet& planet() const;
	
	/**
	 * @brief Calculates and returns the relative radius \f$ \eta = a/R \f$.
	 * @return Relative radius.
	 */
	double relativeRadius() const;
	
	/**
	 * @brief Calculates and returns the Keplerian mean motion \f$ n_0 \f$ in \f$ rad \cdot s^{-1} \f$.
	 * @return Keplerian mean motion in \f$ rad \cdot s^{-1} \f$.
	 */
	double keplerianMeanMotion() const;
	
	/**
	 * @brief Calculates and returns the Keplerian mean motion variation \f$ \frac{\Delta n}{n} \f$.
	 * @return Keplerian mean motion variation in \f$ rad \cdot s^{-1} \f$.
	 */
	double meanMotionVariation() const;
	
	/**
	 * @brief Calculates and returns the true motion \f$ n \f$ in \f$ rad \cdot s^{-1} \f$.
	 * @return True motion in \f$ rad \cdot s^{-1} \f$.
	 */
	double trueMeanMotion() const;
	
	double K0() const;
	
	/**
	 * @brief Calculates and returns the apsidal precession rate \f$ \dot{\omega} \f$ in \f$ rad \cdot s^{-1} \f$
	 */
	double apsidalPrecessionRate() const;
	
	/**
	 * @brief Calculates and returns the nodal precession rate \f$ \dot{\Omega} \f$ in \f$ rad \cdot s^{-1} \f$
	 */
	double nodalPrecessionRate() const;
	
	/**
	 * @brief Calculates and returns the anomalistic period \f$ T_a \f$ in seconds.
	 * @return Anomalistic period in seconds.
	 */
	double anomalisticPeriod() const;
	
	/**
	 * @brief Calculates and returns the Keplerian period \f$ T_0 \f$ in seconds.
	 * @return Keplerian period in seconds.
	 */
	double keplerianPeriod() const;
	
	/**
	 * @brief Calculates and returns the nodal (or draconitic) period \f$ T_d \f$ in seconds.
	 * @return Nodal period in seconds.
	 */
	double nodalPeriod() const;
	
	/**
	 * @brief Calculates and returns the daily recurrence frequency \f$ \kappa \f$.
	 * 
	 * @return Daily recurrence frequency.
	 */
	double dailyReccurenceFrequency() const;
	
	/**
	 * @brief Calculates and returns the daily orbital frequency \f$ \nu \f$.
	 * 
	 * The daily frequency of a satellite measures the number of revolutions the satellite covers in one day (24 hours).
	 * 
	 * @return Daily orbital frequency.
	 */
	double dailyOrbitalFrequency() const;
	
	/**
	 * @brief Calculates and returns the geopotential at given latitude \f$ U \f$ in \f$ m^2 \cdot s^{-2} \f$.
	 * @param[in] latitude Latitude.
	 * @param[in] u Angle unit.
	 * @return Daily orbital frequency.
	 */
	double potential(double latitude, const geo::unit_angle u=geo::DEGREE) const;
	
	/**
	 * @brief Calculates and returns the satellite distance \f$ r \f$ from Planet center at given anomaly in meters.
	 * @param[in] anomaly Anomaly in radians.
	 * @return Satellite distance from Planet center.
	 */
	double satelliteDistance(const double anomaly) const;
	
	/**
	 * @brief Calculates and returns the apparent inclination of the track.
	 * @return Apparent inclination.
	 */
	double apparentInclination() const;
	
	/**
	 * @brief Calculates and returns the inclination of the track at the given latitude.
	 * @return Angle between the North direction and the track.
	 */
	double trackInclination(double time, double timeAN=0.) const;
	
	/**
	 * @brief Return the first derivative of Z in \f$ m \cdot s^{-1} \f$.
	 * @param[in] time Time to generate.
	 * @param[in] timeAN Date at the ascending node in julian days.
	 * @return First derivative of Z.
	 */
	double dZ(double time, double timeAN=0.) const;
	
	/**
	 * @brief Tells if orbit is ascending at this time.
	 * @param[in] time Time to generate.
	 * @param[in] timeAN Date at the ascending node in julian days.
	 * @return True if the orbit is ascending, else false.
	 */
	bool ascending(double time, double timeAN=0.) const;
	
	/**
	 * @brief Sets the radius of the Orbit.
	 * @param[in] radius Radius of the Orbit in meters.
	 */
	void setRadius(const double radius);
	
	/**
	 * @brief Sets the altitude of the Orbit.
	 * @param[in] altitude altitude of the Orbit in meters.
	 */
	void setAltitude(const double altitude);
	
	/**
	 * @brief Sets the inclination of the Orbit.
	 * @param[in] inclination Inclination of the Orbit.
	 * @param[in] u Angle unit.
	 */
	void setInclination(double inclination, const geo::unit_angle u=geo::RADIAN);
	
	/**
	 * @brief Sets the eccentricity of the Orbit.
	 * @param[in] eccentricity Eccentricity of the Orbit.
	 */
	void setEccentricity(const double eccentricity);
	
	/**
	 * @brief Calculates and the ground track coordinates.
	 * 
	 * @param[in] startTime Begining of generation in days.
	 * @param[in] endTime End of generation in days.
	 * @param[in] resolution Interval between two track points in seconds.
	 * @param[in] lonAN Longitude at the ascending node in degrees.
	 * @param[in] timeAN Date at the ascending node in julian days.
	 * @return A vector of pairs of time (in days) and Coordinate.
	 */
	Track getTrack(double startTime, double endTime, double resolution, double lonAN=0., double timeAN=0.) const;
	
	/**
	 * @brief Calculates and the ground track coordinates.
	 * 
	 * @param[in] time Time to generate.
	 * @param[in] lonAN Longitude at the ascending node in degrees.
	 * @param[in] timeAN Date at the ascending node in julian days.
	 * @return The CartesianCoordinate of the Satellite.
	 */
	CartesianCoordinate getCartesianCoordinate(double time, double lonAN=0., double timeAN=0.) const;
	
	/**
	 * @brief Calculates and the ground track coordinates.
	 * 
	 * @param[in] time Time to generate.
	 * @param[in] lonAN Longitude at the ascending node in degrees.
	 * @param[in] timeAN Date at the ascending node in julian days.
	 * @return The Coordinate of the Satellite.
	 */
	Coordinate getCoordinate(double time, double lonAN=0., double timeAN=0.) const;
	
	protected:
	Planet m_planet; //!< Orbit aound this Planet.
	
	// Metric orbital elements
	double m_radius; //!< Radius of the Orbit in meters.
	double m_inclination; //!< Orbit's inclination in radians.
	double m_eccentricity; //!< Orbit's eccentricity.
};

#endif
