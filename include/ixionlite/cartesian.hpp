#ifndef CARTESIAN_HPP
#define CARTESIAN_HPP

#include <geo.hpp>

struct CartesianCoordinate
{
	CartesianCoordinate(double xin=0., double yin=0., double zin=0.);
	CartesianCoordinate(const Coordinate& coord, double rin=1.);
	CartesianCoordinate& fromEuler(double E1, double E2, double E3, double rin=1.);
	double x;
	double y;
	double z;
	double r() const;
	operator Coordinate() const;
	CartesianCoordinate operator*(double a) const;
	CartesianCoordinate& operator*=(double a);
	CartesianCoordinate operator/(double a) const;
	CartesianCoordinate& operator/=(double a);
	CartesianCoordinate operator+(const CartesianCoordinate& c) const;
	CartesianCoordinate& operator+=(const CartesianCoordinate& c);
	CartesianCoordinate operator-(const CartesianCoordinate& c) const;
	CartesianCoordinate& operator-=(const CartesianCoordinate& c);
};

#endif
