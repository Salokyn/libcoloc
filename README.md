The project is build around several libraries

## caldat

Provides the Date object that help play with dates and time.

### Documentation

https://salokyn.gitlab.io/libcoloc/caldat

## geo

Provides the Coordinate object and spherical geometry functions

### Documentation

https://salokyn.gitlab.io/libcoloc/geo

## navgeo

Provides tools to navigate in geostationary satellites images (geo2pix and pix2geo)

### Documentation

https://salokyn.gitlab.io/libcoloc/navgeo

## coloc

Colocation algoritms based on pixel's point spread functions.

### Documentation

https://salokyn.gitlab.io/libcoloc/coloc

[Megha-Tropiques Technical Memorandum n°2: Co-location algorithms, geophysical data projection using pixel point spread function](http://meghatropiques.ipsl.polytechnique.fr/megha-tropiques-technical-memorandum/)