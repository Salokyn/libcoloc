#include <geo/geo.hpp>
#include <cmath>
#include <cstdlib>
#include <numeric>
#include <stdexcept>

namespace geo
{
	double cosalpha(const Coordinate& coord0, const Coordinate& coord1)
	{
		if (coord0 == coord1) return 1.;
		
		const double lon0 = coord0.lon(RADIAN);
		const double lat0 = coord0.lat(RADIAN);
		const double lon1 = coord1.lon(RADIAN);
		const double lat1 = coord1.lat(RADIAN);
		
		const double coslt1 = cos(lat1);
		const double sinlt1 = sin(lat1);
		const double coslt0 = cos(lat0);
		const double sinlt0 = sin(lat0);
	
		const double l0l1 = lon0-lon1;
		const double cosl0l1 = cos(l0l1);
	
		double cosa = sinlt0 * sinlt1 + coslt0 * coslt1 * cosl0l1;
		
		if (std::abs(cosa) > 1.) cosa = copysign(1., cosa); // Sometimes cos > 1
		
		return cosa;
	}
	
	void distortion(const double swath, const double eta, double& K, double& L)
	{
		if (swath == 0.)
		{
			K = L = 1.;
			return;
		}
		const double s = eta*sin(swath);
		
		if (std::abs(s) > 1.)
		{
			throw std::runtime_error("void distortion(const double swath, const double eta, double& K, double& L) : Swath looking in space !");
		}
		
		const double alpha = -swath + asin(s); // angle de vue du pixel en radians par rapport au centre de la terre
	
		K = distortionK(cos(alpha), eta);
		L = distortionL(cos(alpha), eta);
		
	} //distortion
	
	double distortionK(const double cosalpha, const double eta)
	{
		if (cosalpha == 1.) return 1.;
		if (eta == INFINITY) return 1./cosalpha; // limit
		if (cosalpha <= 1./eta) throw std::runtime_error("double distortionK(const double cosalpha, const double eta) : Pixel out of sight !");
		return 1./(eta-1.) * (eta*eta - 2*eta*cosalpha + 1.) / (eta*cosalpha - 1.);
	}
	
	double distortionL(const double cosalpha, const double eta)
	{
		if (cosalpha == 1.) return 1.;
		if (eta == INFINITY) return 1.; // limit
		if (cosalpha <= 1./eta) throw std::runtime_error("double distortionL(const double cosalpha, const double eta) : Pixel out of sight !");
		return 1./(eta-1.) * sqrt(eta*eta - 2*eta*cosalpha + 1.);
	}
	
	/*=================================================================================\
	! Calcul de la distance entre deux points sur la terre                             !
	! Inputs :                                                                         !
	!   lon0 : longitude du point d'observation en DEGRE                               !
	!   lat0 : latitude du point d'observation en DEGRE                                !
	!   lon1 : longitude du point observé DEGRE                                        !
	!   lat1 : latitude du point observé DEGRE                                         !
	! Outputs :                                                                        !
	!   map_2points : distance entre deux points sur la terre en metres                !
	!   azi (optionel) : azimut entre les deux points en RADIAN, dans le sens trigo    !
	!==================================================================================!
	! VALIDEE le 3/3/2010                                                              !
	\=================================================================================*/
	double map_2points(const Coordinate& coord0, const Coordinate& coord1, double* azi, const unit_angle u, const double radius)
	{
		// Dans IDL on utilise 'l0l1 = lon1-lon0' ce qui donne l'angle dans le sens horaire
		
		const double lon0 = coord0.lon(RADIAN);
		const double lat0 = coord0.lat(RADIAN);
		const double lon1 = coord1.lon(RADIAN);
		const double lat1 = coord1.lat(RADIAN);
	
		const double coslt1 = cos(lat1);
		const double sinlt1 = sin(lat1);
		const double coslt0 = cos(lat0);
		const double sinlt0 = sin(lat0);
	
		const double l0l1 = lon0-lon1;
		const double cosl0l1 = cos(l0l1);
		const double sinl0l1 = sin(l0l1);
	
		double cosa = sinlt0 * sinlt1 + coslt0 * coslt1 * cosl0l1; //Cos of angle between pnts
		
		if (std::abs(cosa) > 1.) cosa = copysign(1., cosa); // Sometimes cos > 1
		
		const double alpha = acos(cosa);
	
		// calcul de l'azimut
		if (azi != NULL)
		{
			const double sina = sqrt(1. - cosa*cosa);
			double beta;

			if ( sina != 0. ) // Small angle?
			{
				const double cosaz = (coslt0*sinlt1 - sinlt0*coslt1*cosl0l1) / sina; // Azimuth
				const double sinaz = sinl0l1*coslt1 / sina;
				
				beta = atan2(sinaz, cosaz);
				if (u == DEGREE) beta *= M_RADEG;
				
			} else { // It's antipodal
				beta = 0.;
			}
			
			*azi = beta;
		}
	
		return alpha * radius; // acos(cosc) = alpha
		
	} // map_2points
	
	double map_2points(const double lon0, const double lat0, const double lon1, const double lat1, double* azi, const unit_angle u, const double radius)
	{
		return map_2points(Coordinate(lon0, lat0), Coordinate(lon1, lat1), azi, u, radius);
	}
	
	void rotate(const double xin, const double yin, const double angle, double& xout, double& yout)
	{
		xout = xin * cos(angle) - yin * sin(angle);
		yout = xin * sin(angle) + yin * cos(angle);
	} // rotate
	
	double zenangle(const Coordinate& coord0, const Coordinate& coord1, const double eta, const unit_angle u)
	{
		if (coord0 == coord1) return 0.;
		
		const double cosa = cosalpha(coord0, coord1); //Cos of angle between pnts
		
		return zenangle(cosa, eta, u);
	} // zenangle
	
	double zenangle(const double lon0, const double lat0, const double lon1, const double lat1, const double eta, const unit_angle u)
	{
		return zenangle(Coordinate(lon0, lat0), Coordinate(lon1, lat1), eta, u);
	} // zenangle
	
	double zenangle(const double cosalpha, const double eta, const unit_angle u)
	{
		const double sinalpha = sqrt(1.0 - cosalpha*cosalpha);
		
		double zen = atan2(sinalpha, cosalpha - 1./eta);
		if (u == DEGREE) zen *= M_RADEG;
		
		return zen;
	} // zenangle
	
	double swath(const Coordinate& coord0, const Coordinate& coord1, const double eta, const unit_angle u)
	{
		if (coord0 == coord1) return 0.;
		
		const double cosa = cosalpha(coord0, coord1); //Cos of angle between pnts
		
		return swath(cosa, eta, u);
	} // swath
	
	double swath(const double lon0, const double lat0, const double lon1, const double lat1, const double eta, const unit_angle u)
	{
		return swath(Coordinate(lon0, lat0), Coordinate(lon1, lat1), eta, u);
	} // swath
	
	double swath(const double cosalpha, const double eta, const unit_angle u)
	{
		const double sinalpha = sqrt(1.0 - cosalpha*cosalpha);
		
		double f = atan2(sinalpha, eta - cosalpha);
		if (u == DEGREE) f *= M_RADEG;
		
		return f;
	} // swath
	
	/*=================================================================================\
	! Calcule la déclinaison du soleil                                                 !
	! Inputs :                                                                         !
	!   jour : jour de l'année                                                         !
	! Outputs :                                                                        !
	!   declinaison : declinaison en RADIAN                                            !
	!==================================================================================!
	! VALIDEE le 3/3/2010                                                              !
	\=================================================================================*/
	double declination(const int day)
	{
		const double sine = sin(23.4392/M_RADEG); // earth obliquity
		return asin( sine*sin( (2.*M_PI/365.*(day-82.)+0.033161*sin(2.*M_PI/365.*(day-3.))) ) );
	
	} // declination
	
	double sol_zenangle(const Date& date, const Coordinate& coord, double* azi, const unit_angle u)
	{
		const double heureloc = date.dec_hour()+coord.lon()/15.;
		const double dec = declination((int) date.yday());
	
		return sol_zenangle(coord.lat(), dec, heureloc, azi, u);
	}
	
	double sol_zenangle(const double lat, const double dec, const double heureloc, double* azi, const unit_angle u)
	{
		const double hsol = (heureloc-12.)/24.*2.*M_PI;
	
		const double coslt = cos(lat/M_RADEG);
		const double sinlt = sin(lat/M_RADEG);
		const double cosd  = cos(dec);
		const double sind  = sin(dec);
		
		double cosz = sinlt*sind+coslt*cosd*cos(hsol);
		if (std::abs(cosz) > 1.) cosz = copysign(1., cosz); // Sometimes cos > 1
		
		double zen = acos(cosz);
		if (u == DEGREE) zen *= M_RADEG;
		
		if (azi != NULL)
		{
			const double sinz = sqrt(1.0 - cosz*cosz);
		
			if (sinz == 0.)
			{
				*azi = 0.;
				return zen;
			}
		
			double cosaz = (-coslt*sind+sinlt*cosd*cos(hsol))/sinz;
			if (std::abs(cosaz) > 1.) cosaz = copysign(1., cosaz);
			
			*azi = acos(cosaz); // cet azimut est donné par rapport au SUD dans [0;pi] !!
	
			// l'azimut est compris dans [-pi;0] le matin et dans [0;pi] l'après-midi
			*azi = -copysign(*azi, hsol);
	
			// maintenant c'est par rapport au nord dans [-pi;pi]
			*azi -= copysign(M_PI, *azi);
			if (u == DEGREE) *azi *= M_RADEG;
		}
		
		return zen;
	}
	
	/*=================================================================================\
	! Calcule un rayon d'inclinaison t, d'une ellipse de demi grand a et de demi grand !
	! axe a et de demi petit axe b                                                     !
	! Inputs :                                                                         !
	!   a : demi grand axe                                                             !
	!   b : demi petit axe                                                             !
	!   t : inclinaison du rayon                                                       !
	! Outputs :                                                                        !
	!   radius_ellipse : longueur du rayon                                             !
	!==================================================================================!
	! VALIDEE le 3/3/2010                                                              !
	\=================================================================================*/
	double radius_ellipse(const double a, const double b, double t, const geo::unit_angle u)
	{
		// si les deux demi-axes ont la même longeur
		if (std::abs(a) == std::abs(b)) return std::abs(a);
		
		if (u == geo::DEGREE) t /= M_RADEG;
		
		return std::abs(a*b)/sqrt(a*a-(a*a-b*b)*cos(t)*cos(t));
	}
	
	double to_180_180(double lon)
	{
		if (std::abs(lon) > 180.)
		{
			lon -= 360.*round(lon/360.);
		}
		return lon;
	}
	
	double to_0_360(double lon)
	{
		if (std::abs(lon-180.) > 180.)
		{
			lon -= 360.*floor(lon/360.);
		}
		return lon;
	}
	
	double to_geodetic(double gc, unit_angle u, double f)
	{
		if (u==DEGREE) gc /= M_RADEG;
		
		double gd = atan(tan(gc)/(f*f-2*f+1));
		
		if (u==DEGREE) gd *= M_RADEG;
		
		return gd;
	}
	
	double to_geocentric(double gd, unit_angle u, double f)
	{
		if (u==DEGREE) gd /= M_RADEG;
		
		double gc =atan(tan(gd)*(f*f-2*f+1));
		
		if (u==DEGREE) gc *= M_RADEG;
		
		return gc;
	}
	
	Coordinate equidistantCircle(const Coordinate& center, double alpha, double azimuth, unit_angle u)
	{
		if (alpha == 0.) return center;
		
		if (u == DEGREE)
		{
			alpha /= M_RADEG;
			azimuth /= M_RADEG;
		}
		
		const double coslt0 = cos(center.lat(RADIAN));
		const double sinlt0 = sin(center.lat(RADIAN));
		const double cosalpha = cos(alpha);
		const double sinalpha = sin(alpha);
		
		double sinlt1 = cosalpha*sinlt0+sinalpha*coslt0*cos(azimuth);
		if (std::abs(sinlt1) > 1.) sinlt1 = copysign(1., sinlt1);
		
		const double lat = asin(sinlt1)*M_RADEG;
		
		const double coslt1 = sqrt(1.-sinlt1*sinlt1);
		
		const double cosln1 = (cosalpha-sinlt0*sinlt1) / (coslt0*coslt1);
		double sinln1=sinalpha*sin(azimuth)/coslt1;
		if (std::abs(sinln1) > 1.) sinln1 = copysign(1., sinln1);
		
		double dlon = asin(sinln1);
		
		// Si on dépasse le pôle
		if (cosln1 < 0.) dlon = M_PI-dlon;
		
		const double lon = center.lon()-dlon*M_RADEG;
		
		return Coordinate(lon, lat);
	}

} // namespace




/******************************************************************************\
* NAME: Coordinate                                                             *
* AUTH: Nicolas Gif <nicolas.gif@lmd.polytechnique.fr>                         *
* DATE: mar. 08 juin 2010 11:00:07 CEST                                        *
* TYPE: Classe                                                                 *
* DESC: Gère les lon/lat (limites et conversions)                              *
\******************************************************************************/

// Constructeurs
Coordinate::Coordinate():m_lon(0.),m_lat(0.)
{}
Coordinate::Coordinate(const double lon, const double lat, const geo::unit_angle u)
{
	set(lon, lat, u);
}
Coordinate::~Coordinate()
{}

// Getteurs
double Coordinate::lon(const geo::unit_angle u) const
{
	if (u == geo::RADIAN) return m_lon/M_RADEG;
	return m_lon;
}

double Coordinate::relativeLon(const geo::unit_angle u) const
{
	return lon(u);
}

double Coordinate::absoluteLon(const geo::unit_angle u) const
{
	double alon = geo::to_0_360(lon(geo::DEGREE));
	if (u == geo::RADIAN) return alon/M_RADEG;
	return alon;
}

double Coordinate::lat(const geo::unit_angle u) const
{
	if (u == geo::RADIAN) return m_lat/M_RADEG;
	return m_lat;
}

double Coordinate::colat(const geo::unit_angle u) const
{
	if (u == geo::RADIAN) return (90.-m_lat)/M_RADEG;
	return 90.-m_lat;
}

// Setteurs
void Coordinate::set_lon(double lon, const geo::unit_angle u)
{
	if (u == geo::RADIAN) lon *= M_RADEG; // conversion en DEGREE
	m_lon = geo::to_180_180(lon);
}

void Coordinate::set_lat(double lat, const geo::unit_angle u)
{
	if (u == geo::RADIAN) lat *= M_RADEG; // conversion en DEGREE
	if (lat >  90.) throw std::domain_error("void Coordinate::set_lat(double lat, const geo::unit_angle u) : Latitude >  90°");
	if (lat < -90.) throw std::domain_error("void Coordinate::set_lat(double lat, const geo::unit_angle u) : Latitude < -90°");
	m_lat = lat;
}

void Coordinate::set_colat(const double colat, const geo::unit_angle u)
{
	set_lat(90.-colat, u);
}

void Coordinate::set(const double lon, const double lat, const geo::unit_angle u)
{
	set_lon(lon, u);
	set_lat(lat, u);
}

// Opérateurs
Coordinate Coordinate::operator+(const Coordinate& coord) const
{
	return Coordinate(m_lon+coord.lon(), m_lat+coord.lat());
}
Coordinate Coordinate::operator-(const Coordinate& coord) const
{
	return Coordinate(m_lon-coord.lon(), m_lat-coord.lat());
}
Coordinate& Coordinate::operator+=(const Coordinate& coord)
{
	*this = *this + coord;
	return *this;
}
Coordinate& Coordinate::operator-=(const Coordinate& coord)
{
	*this = *this - coord;
	return *this;
}
bool Coordinate::operator==(const Coordinate& coord) const
{
	return m_lon == coord.lon() and m_lat == coord.lat();
}
bool Coordinate::operator!=(const Coordinate& coord) const
{
	return !(*this == coord);
}

std::ostream& std::operator<<(std::ostream& out, const Coordinate& coord)
{
	out << "(" << coord.lon() << ", " << coord.lat() << ")";
	return out;
}
