#include <coloc/orbit.hpp>
#include <stdexcept>
#include <geo/geo.hpp>
#include <cmath>
#include <algorithm>
#include <sstream>

// Constructeurs
Orbit::Orbit() {}
Orbit::Orbit(const size_t i) : std::vector<Pixel*>(i) {}
Orbit::Orbit(const std::vector<Pixel*> & pixels) : std::vector<Pixel*>(pixels) {}
Orbit::Orbit(const Orbit& copy) // Constructeur de copie
{
	*this = copy;
}

Orbit::Orbit(const Image& image)
{
	size_t size = image.size();
	resize(size);
	for (size_t i=0; i < size; i++)
	{
		if (image[i] != NULL) (*this)[i] = image[i]->clone();
		else (*this)[i] = NULL;
	}
}

Orbit::Orbit(const Pixel& pixel_template, const std::vector<double>& time, const std::vector<double>& lon, const std::vector<double>& lat, const std::vector<double>& satLon, const std::vector<double>& satLat, const std::vector<bool>& mask)
{
	const bool use_time = time.size() > 0;
	const bool use_satCoord = satLon.size() > 0 and satLat.size() > 0;
	const bool use_mask = mask.size() > 0;
	
	resize(lon.size());
	
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < size(); i++)
	{
		Pixel* pixel = pixel_template.clone();
		
		if (use_time) pixel->setTime(time[i]);
		
		if (use_mask) pixel->setProceed(mask[i]);
		
		if (use_satCoord)
		{
			Coordinate coord, satCoord;
			
			try
			{
				coord = Coordinate(lon[i], lat[i]);
				satCoord = Coordinate(satLon[i], satLat[i]);
			}
			catch(const std::exception& e)
			{
				delete pixel;
				pixel = NULL;
			}
			
			try
			{
				pixel->setCoord(coord, satCoord);
			}
			catch(const std::exception& e)
			{
				delete pixel;
				pixel = NULL;
				
				if (use_mask and mask[i])
				{
					std::stringstream ss;
					ss << "Orbit::Orbit(...) : Exception caught at index '" << i << "' : " << e.what();
					throw std::runtime_error(ss.str());
				}
			}
		} else {
			Coordinate coord;
			
			try
			{
				coord = Coordinate(lon[i], lat[i]);
			}
			catch(const std::exception& e)
			{
				pixel->setProceed(false);
			}
			
			try
			{
				pixel->setCoord(coord);
			}
			catch(const std::exception& e)
			{
				delete pixel;
				pixel = NULL;
				
				if (use_mask and mask[i])
				{
					std::stringstream ss;
					ss << "Orbit::Orbit(...) : Exception caught at index '" << i << "' : " << e.what();
					throw std::runtime_error(ss.str());
				}
			}
		}
		
		(*this)[i] = pixel;
	}
}

// Destructeur
Orbit::~Orbit() { free(); }

// Méthodes
void Orbit::setValue(const double* array, const size_t index)
{
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < size(); i++)
	{
		Pixel* pixel = (*this)[i];
		if (pixel == NULL) continue;
		pixel->setValue(array[i], index);
	}
}

void Orbit::setValue(const std::vector<double>& vector, const size_t index)
{
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < std::min(size(), vector.size()); i++)
	{
		Pixel* pixel = (*this)[i];
		if (pixel == NULL) continue;
		pixel->setValue(vector[i], index);
	}
}

void Orbit::free()
{
	for (size_t i=0; i < size(); i++)
	{
		delete (*this)[i];
		(*this)[i] = NULL;
	}
}

void Orbit::clear()
{
	free();
	std::vector<Pixel*>::clear();
}

void Orbit::resize(const size_t n)
{
	if (n < size())
	{
		for (size_t i=n; i<size(); i++)
		{
			delete (*this)[i];
		}
	}
	std::vector<Pixel*>::resize(n, NULL);
}

void Orbit::set(const size_t i, const Pixel& pixel)
{
	if (i >= size()) throw std::out_of_range("void Image::set(size_t i, Pixel* pixel) : Given index is greater than Image size !");
	delete (*this)[i];
	(*this)[i] = pixel.clone();
}

// Getteurs
std::vector<double> Orbit::lon() const
{
	std::vector<double> out(size(), NAN);
	for (size_t i=0; i<size(); i++)
		if ((*this)[i] != NULL) out[i] = (*this)[i]->lon();
	return out;
}

std::vector<double> Orbit::lat() const
{
	std::vector<double> out(size(), NAN);
	for (size_t i=0; i<size(); i++)
		if ((*this)[i] != NULL) out[i] = (*this)[i]->lat();
	return out;
}

std::vector<double> Orbit::time() const
{
	std::vector<double> out(size(), NAN);
	for (size_t i=0; i<size(); i++)
		if ((*this)[i] != NULL) out[i] = (*this)[i]->time();
	return out;
}

std::vector<double> Orbit::satLon() const
{
	std::vector<double> out(size(), NAN);
	for (size_t i=0; i<size(); i++)
		if ((*this)[i] != NULL) out[i] = (*this)[i]->satLon();
	return out;
}

std::vector<double> Orbit::satLat() const
{
	std::vector<double> out(size(), NAN);
	for (size_t i=0; i<size(); i++)
		if ((*this)[i] != NULL) out[i] = (*this)[i]->satLat();
	return out;
}

std::vector<double> Orbit::value(const size_t index) const
{
	std::vector<double> out(size(), NAN);
	for (size_t i=0; i<size(); i++)
		if ((*this)[i] != NULL) out[i] = (*this)[i]->value(index);
	return out;
}

// Setteurs
void Orbit::sort()
{
	std::sort(begin(), end(), compareTime);
}

void Orbit::stable_sort()
{
	std::stable_sort(begin(), end(), compareTime);
}

bool Orbit::compareTime(const Pixel *const pixelA, const Pixel *const pixelB)
{
	if (pixelA == NULL) return false;
	if (pixelB == NULL) return true;
	return pixelA->time() < pixelB->time();
}

bool Orbit::compareLat(const Pixel *const pixelA, const Pixel *const pixelB)
{
	if (pixelA == NULL) return false;
	if (pixelB == NULL) return true;
	return pixelA->lat() < pixelB->lat();
}

void Orbit::updateAzimuthAngle(const bool onlyZen0, const bool nosort)
{
	if (size() < 2) return;
	
	// Initialisations
	double last_lon = (*this)[0]->satLon();
	double last_lat = (*this)[0]->satLat();
	size_t firstpix = 0; // indice du 1er pixel updaté
	
	double azimuth;
	
	if (!nosort) stable_sort();
	
	for (size_t i = 1; i < size(); i++)
	{
		Pixel* pixel = (*this)[i];
		
		if (pixel->scanMode() != Pixel::XT) continue;
		if (onlyZen0 and pixel->cosalpha() != 1.) continue;
		
		double diffLat = pixel->satLat()-last_lat;
		double diffLon = geo::to_180_180(pixel->satLon()-last_lon);
		
		if (std::abs(diffLat) > .1 or std::abs(diffLon) > .1)
		{
			geo::map_2points(Coordinate(last_lon, last_lat), pixel->satCoord(), &azimuth, geo::DEGREE);
			
			if (firstpix == 0) firstpix = i;
			
			last_lon = pixel->satLon();
			last_lat = pixel->satLat();
		}
		
		pixel->setAzimuthAngle(azimuth);
	}
	
	// Recopie le 1er azimuth calculé dans les 1ers pixels.
	double firstazi = (*this)[firstpix]->azimuthAngle();
	for (size_t i = 0; i < firstpix; i++)
	{
		Pixel* pixel = (*this)[i];
		
		if (pixel->scanMode() != Pixel::XT) continue;
		if (onlyZen0 and pixel->cosalpha() != 1.) continue;
		
		pixel->setAzimuthAngle(firstazi);
	}
}

unsigned int Orbit::secant(const double aim, long& xn_1, long& xn, const unsigned int max)
{
	double last_dt = 0.;
	unsigned int n;
	double best_delta_time = 1e30;
	long best_xn = xn;
	long last_best_xn = -1;
	
	for (n = 0; n <= max; n++)
	{
		double dt = ((*this)[xn]->time() - (*this)[xn_1]->time()); //gradient du temps
		if (dt == 0.) break;
		
		if (dt == last_dt) break; // teste la convergence
		last_dt = dt;
		
		double dx = double(xn - xn_1);
		
		double delta_time = ((*this)[xn]->time()-aim);
		double delta_x = dx/dt * delta_time;
		
		if (std::abs(delta_time) < best_delta_time)
		{
			best_delta_time = std::abs(delta_time);
			best_xn = xn;
		}
		
		if (best_xn == last_best_xn) break; // teste la convergence
		last_best_xn = best_xn;
		
		xn_1 = xn;
		xn = std::max(std::min(xn - (long) round(delta_x), (long) size()-1), 0l);
	}
	
	xn = best_xn;
	
	return n;
}

unsigned int Orbit::falsiMethod(const double aim, long& left, long& right, const unsigned int max)
{
	char side = 0;
	unsigned int n=0;
	double tLeft = (*this)[left]->time()-aim;
	double tRight = (*this)[right]->time()-aim;
	
	if (tLeft - tRight == 0) return 0; // Already converged ?
	if (tLeft * tRight > 0.) // Does a root exists ?
	{
		if (std::abs(tLeft) < std::abs(tRight))
		{
			right = left;
		}
		else if (std::abs(tLeft) > std::abs(tRight))
		{
			left = right;
		}
		return 0;
	}
	
	for (n = 0; n <= max; n++)
	{
		long hint = (long) round((right*tLeft-left*tRight)/(tLeft-tRight));
		
		double tHint = (*this)[hint]->time()-aim;
		
		if (tRight * tHint > 0. and right != hint)
		{
			right = hint;
			tRight = tHint;
			if (side == -1) tLeft /= 2.;
			side = -1;
		}
		else if (tLeft * tHint > 0. and left != hint)
		{
			left = hint;
			tLeft = tHint;
			if (side == +1) tRight /= 2.;
			side = +1;
		}
		else
		{
			break;
		}
	}
	
	return n;
}

unsigned int Orbit::dichotomy(const double aim, long& left, long& right, const unsigned int max)
{
	unsigned int n=0;
	double tLeft = (*this)[left]->time()-aim;
	double tRight = (*this)[right]->time()-aim;
	
	if (tLeft - tRight == 0) return 0; // Already converged ?
	if (tLeft * tRight > 0.) // Does a root exists ?
	{
		if (std::abs(tLeft) < std::abs(tRight))
		{
			right = left;
		}
		else if (std::abs(tLeft) > std::abs(tRight))
		{
			left = right;
		}
		return 0;
	}
	
	for (n = 0; n <= max; n++)
	{
		long middle = (left + right)/2;
		
		if (aim < (*this)[middle]->time() and right != middle)
			right = middle;
		else if (aim > (*this)[middle]->time() and left != middle)
			left = middle;
		else
			break;
	}
	
	return n;
}

Orbit& Orbit::operator=(const Orbit & copy)
{
	if (this == &copy) return *this;
	
	free();
	
	size_t size = copy.size();
	resize(size);
	for (size_t i=0; i < size; i++)
	{
		if (copy[i] != NULL) (*this)[i] = copy[i]->clone();
		else (*this)[i] = NULL;
	}
	
	return *this;
}

Orbit& Orbit::operator+=(const Orbit& orbit)
{
	size_t oldSize = size();
	size_t newSize = size()+orbit.size();
	resize(newSize);
	
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < orbit.size(); i++)
	{
		if (orbit[i] != NULL)
			(*this)[i+oldSize] = orbit[i]->clone();
	}
	
	return *this;
}
