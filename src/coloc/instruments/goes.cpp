#include <coloc/instruments/goes.hpp>
#include <geo/geo.hpp>
#include <cmath>
#include <stdexcept>

// Constructeurs
Pixel_GOES::Pixel_GOES():PixelCircle(default_radius, default_relativeAltitude)
{
	m_scanMode = default_scanMode;
}

Pixel_GOES::Pixel_GOES(const Coordinate& coord):PixelCircle(default_radius, default_relativeAltitude, coord)
{
	m_scanMode = default_scanMode;
}

Pixel_GOES::Pixel_GOES(const Coordinate& coord, const Coordinate& satCoord):PixelCircle(default_radius, default_relativeAltitude, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_GOES::Pixel_GOES(const double time, const Coordinate& coord):PixelCircle(default_radius, default_relativeAltitude, time, coord)
{
	m_scanMode = default_scanMode;
}

Pixel_GOES::Pixel_GOES(const double time, const Coordinate& coord, const Coordinate& satCoord):PixelCircle(default_radius, default_relativeAltitude, time, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_GOES* Pixel_GOES::clone() const
{
	return new Pixel_GOES(*this);
}

Image_GOES::Image_GOES(const double ssp, const size_t ncols, const size_t nrows, const size_t startCol, const size_t startRow):
ImageWithNavigationGeo(navgeo::GOES(ssp), ncols, nrows, startCol, startRow)
{
}
