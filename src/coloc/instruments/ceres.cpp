#include <coloc/instruments/ceres.hpp>
#include <geo/geo.hpp>
#include <cmath>

// Constructeurs
Pixel_CERES::Pixel_CERES():Pixel()
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
}

Pixel_CERES::Pixel_CERES(const Coordinate& coord):Pixel(coord)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
}

Pixel_CERES::Pixel_CERES(const Coordinate& coord, const Coordinate& satCoord):Pixel(coord, satCoord)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
	updateViewingAzimuthAngle();
	updateAzimuthAngle();
	updateDistortion();
}

Pixel_CERES::Pixel_CERES(double time, const Coordinate& coord):Pixel(time, coord)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
}

Pixel_CERES::Pixel_CERES(double time, const Coordinate& coord, const Coordinate& satCoord):Pixel(time, coord, satCoord)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
	updateViewingAzimuthAngle();
	updateAzimuthAngle();
	updateDistortion();
}

Pixel_CERES* Pixel_CERES::clone() const
{
	return new Pixel_CERES(*this);
}

// Méthodes
double Pixel_CERES::psf(const Coordinate& coord) const
{
	if (this->coord() == coord) return 1.;
	
	const double amax = 18.49e3; // en m
	const double cmax = 16.13e3; // en m
	
	double dist, azi;
	dist = geo::map_2points(this->coord(), coord, &azi, geo::RADIAN);
	
	// si le point est en dehors du rayon
	if (dist > radius()*K())
	{
		return 0.;
	}
	
	std::complex<double> z = std::polar(dist, azi);
	rotate_and_distort(z);
	
	const double adf = std::abs(std::real(z))/amax; // Normalisation entre 0 et 1;
	const double cdf = std::abs(std::imag(z))/cmax; // Normalisation entre 0 et 1;
	
	// si le point est en dehors du pixel
	if (adf > 1. or cdf > 1.)
	{
		return 0.;
	}
	
	return 1./2.*(cos(adf*M_PI)+1.) * std::max(std::min((1-cdf)/.2,1.),0.);
}

double Pixel_CERES::radius() const
{
	return altitude()*sqrt(pow(tan(1.55/M_RADEG),2.) + pow(tan(1.32/M_RADEG),2.));
}

double Pixel_CERES::surface() const
{
	return 18.49e3*16.13e3*K()*L()/2.;
}
