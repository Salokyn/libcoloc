#include <coloc/instruments/madras.hpp>

// Constructeurs
Pixel_Madras::Pixel_Madras():PixelCircle(default_radius, default_relativeAltitude)
{
	m_scanMode = default_scanMode;
}

Pixel_Madras::Pixel_Madras(const Coordinate& coord):PixelCircle(default_radius, default_relativeAltitude, coord)
{
	m_scanMode = default_scanMode;
}

Pixel_Madras::Pixel_Madras(const Coordinate& coord, const Coordinate& satCoord):PixelCircle(default_radius, default_relativeAltitude, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_Madras::Pixel_Madras(const double time, const Coordinate& coord):PixelCircle(default_radius, default_relativeAltitude, time, coord)
{
	m_scanMode = default_scanMode;
}

Pixel_Madras::Pixel_Madras(const double time, const Coordinate& coord, const Coordinate& satCoord):PixelCircle(default_radius, default_relativeAltitude, time, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_Madras* Pixel_Madras::clone() const
{
	return new Pixel_Madras(*this);
}
