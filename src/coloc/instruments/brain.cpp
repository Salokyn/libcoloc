#include <coloc/instruments/brain.hpp>
#include <algorithm>
#include <cmath>

// Constructeurs
Pixel_BRAIN::Pixel_BRAIN(const double radius):PixelCircle(radius,default_relativeAltitude)
{
	m_scanMode = default_scanMode;
}

Pixel_BRAIN::Pixel_BRAIN(const double radius, const Coordinate& coord):PixelCircle(radius,default_relativeAltitude,coord)
{
	m_scanMode = default_scanMode;
}

Pixel_BRAIN::Pixel_BRAIN(const double radius, const Coordinate& coord, const Coordinate& satCoord):PixelCircle(radius,default_relativeAltitude,coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_BRAIN::Pixel_BRAIN(const double radius, const double time, const Coordinate& coord):PixelCircle(radius,default_relativeAltitude,time, coord)
{
	m_scanMode = default_scanMode;
	
}

Pixel_BRAIN::Pixel_BRAIN(const double radius, const double time, const Coordinate& coord, const Coordinate& satCoord):PixelCircle(radius,default_relativeAltitude,time, coord, satCoord)
{
	m_scanMode = default_scanMode;
	
}

Pixel_BRAIN* Pixel_BRAIN::clone() const
{
	return new Pixel_BRAIN(*this);
}

// Méthodes
double Pixel_BRAIN::psf(const Coordinate& coord) const
{
	double dist;
	dist = geo::map_2points(this->coord(), coord);
	
	// si le point est en dehors du rayon
	if (dist > radius())
	{
		return 0.;
	}
	
	return gaussian(dist, radius()/2);
}

double Pixel_BRAIN::gaussian(const double x, const double sigma)
{
	return exp(-x*x/(2*sigma*sigma));
}
