#include <coloc/instruments/seviri.hpp>
#include <geo/geo.hpp>
#include <cmath>
#include <stdexcept>

// Constructeurs
Pixel_SEVIRI::Pixel_SEVIRI():PixelPinchCos(default_radius, default_relativeAltitude)
{
	m_satCoord = Coordinate(navgeo::SEVIRI::default_ssp,0.);
	m_scanMode = default_scanMode;
}

Pixel_SEVIRI::Pixel_SEVIRI(const Coordinate& coord, const Coordinate satCoord):PixelPinchCos(default_radius, default_relativeAltitude, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_SEVIRI::Pixel_SEVIRI(const double time, const Coordinate& coord, const Coordinate satCoord):PixelPinchCos(default_radius, default_relativeAltitude, time, coord, satCoord)
{
	m_scanMode = default_scanMode;
}


Pixel_SEVIRI* Pixel_SEVIRI::clone() const
{
	return new Pixel_SEVIRI(*this);
}

Image_SEVIRI::Image_SEVIRI(const bool rotated, const size_t ncols, const size_t nrows, const size_t startCol, const size_t startRow):
ImageWithNavigationGeo(navgeo::SEVIRI(rotated), ncols, nrows, startCol, startRow)
{
}
