#include <coloc/instruments/scarab.hpp>
#include <geo/geo.hpp>
#include <algorithm>

//constructeurs
Pixel_ScaRaB::Pixel_ScaRaB():Pixel()
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
}

Pixel_ScaRaB::Pixel_ScaRaB(const Coordinate& coord):Pixel(coord)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
}

Pixel_ScaRaB::Pixel_ScaRaB(const Coordinate& coord, const Coordinate& satCoord):Pixel(coord, satCoord)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
	updateViewingAzimuthAngle();
	updateAzimuthAngle();
	updateDistortion();
}

Pixel_ScaRaB::Pixel_ScaRaB(const double time, const Coordinate& coord):Pixel(time, coord)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
}

Pixel_ScaRaB::Pixel_ScaRaB(const double time, const Coordinate& coord, const Coordinate& satCoord):Pixel(time, coord, satCoord)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = default_relativeAltitude;
	m_symmetric = default_symmetric;
	updateViewingAzimuthAngle();
	updateAzimuthAngle();
	updateDistortion();
}

Pixel_ScaRaB* Pixel_ScaRaB::clone() const
{
	return new Pixel_ScaRaB(*this);
}

double Pixel_ScaRaB::g1(const double a, const double c)
{
	const double left  = std::abs(1.91-.63-c);
	const double right = 1.91+.63-c;
	
	if (a > right)
		return 0.;
	else if (a < left)
		return 1.;
	else
		return (right - a)/(right-left);
} //g1

double Pixel_ScaRaB::g2(const double c)
{
	if (c > 1.91)
		return 0.;
	else if (c < 1.91-.63)
		return 1.;
	else
		return (1.91-c)/.63;
} // g2

double Pixel_ScaRaB::psf(const Coordinate& coord) const
{
	if (this->coord() == coord) return 1.;
	
	const double arctan = atan(1./altitude()); // on pourrait meme dire "arctan = 1./865.5e3"
	
	double dist, azi;
	dist = geo::map_2points(this->coord(), coord, &azi, geo::RADIAN);
	
	// si le point est en dehors du rayon
	if (dist > radius()*K())
	{
		return 0.;
	}
	
	std::complex<double> z = std::polar(dist, azi);
	rotate_and_distort(z);
	
	// convertion en angle de vue, en degrés
	z *= arctan*M_RADEG;
	
	return g1(std::abs(std::real(z)),std::abs(std::imag(z)))*g2(std::abs(std::imag(z)));
}

double Pixel_ScaRaB::radius() const
{
	return tan((1.91+.63)/M_RADEG)*altitude();
}

double Pixel_ScaRaB::surface() const
{
	double size = 2.*tan(1.35/M_RADEG)*altitude();
	return size*K()*size*L();
}
