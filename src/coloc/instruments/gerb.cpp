#include <coloc/instruments/gerb.hpp>
#include <geo/geo.hpp>
#include <cmath>
#include <stdexcept>

// Constructeurs
Pixel_GERB::Pixel_GERB():PixelPinchCos(default_radius, default_relativeAltitude)
{
	m_satCoord = Coordinate(default_ssp,0.);
	m_scanMode = default_scanMode;
}


Pixel_GERB::Pixel_GERB(const Coordinate& coord, const Coordinate satCoord):PixelPinchCos(default_radius, default_relativeAltitude, coord, satCoord)
{
	m_scanMode = default_scanMode;
}


Pixel_GERB::Pixel_GERB(const double time, const Coordinate& coord, const Coordinate satCoord):PixelPinchCos(default_radius, default_relativeAltitude, time, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_GERB* Pixel_GERB::clone() const
{
	return new Pixel_GERB(*this);
}

Image_GERB::Image_GERB(const size_t ncols, const size_t nrows):Image(ncols, nrows)
{
}

const Pixel *const & Image_GERB::geo2pix(const Coordinate& coord, size_t& col, size_t& row) const
{
	int icol, irow;
	
	const int res = georef(coord.lat(), coord.lon(), irow, icol, nrows(), ncols());
	
	if (res != 0)  throw std::domain_error("void Image_GERB::geo2pix(const Coordinate& coord, size_t& col, size_t& row) const : Coordinate out of sight.");
	
	col = ncols()-icol;
	row = nrows()-irow;
	
	if (col >= m_ncols or row >= m_nrows) throw std::out_of_range("void Image_GERB::geo2pix(const Coordinate& coord, size_t& col, size_t& row) const : Index out of image.");
	
	return (*this)(col, row);
}

/* -------------------------------------------------------------------------------
// Input Parameters
// rlat  :	latitude of the pixel, in degrees North from Equator
// rlong :	longitude, in degrees East from Greenwich
// Note that these are standard geographic coordonnates as would be
//               found in an atlas

// Output Parameters
// line :	line number, measured from southern end of frame
// pixel :	pixel number, measured from eastern end of frame
// visible : flat set to TRUE if pixel is on visible disc
//   flag set to FALSE if pixel is in space (mis a 999)

// (c) EUMETSAT (1997)

// Set up constants
// altitude : distance from earth center to satellite
// req : equatorial earth radius
// rpol : polar earth radius
// oblate : earth oblateness
// deg_to_rad and rad_to_deg are conversions factors
; -------------------------------------------------------------------------------*/
int Image_GERB::georef(double rlat, double rlong, int &line, int &pixel, const int nlines, const int nsamps)
{
	const double altitude = 42164.0;
	const double req = 6378.140;
	const double rpol = 6356.755;
	const double oblate = 1./298.257;
	
	// Convert inputs to radians
	// ----------------------------------------------------------------
	const double geolat = rlat/M_RADEG;
	const double lon = rlong/M_RADEG;

	// Convert geodetic latitudes (as input) to geocentric latitudes
	// for use within the algorithm
	// ----------------------------------------------------------------
	const double lat=atan(pow(1. - oblate,2.) * tan(geolat));
	
	const double coslt = cos(lat);
	const double sinlt = sin(lat);
	const double cosln = cos(lon);
	const double sinln = sin(lon);

	// Calculate rheta. This is distance from the earth centre to a point 
	// on the surface at latitude 'lat'
	// ----------------------------------------------------------------
	const double rheta=(req*rpol)/sqrt(rpol*rpol * coslt*coslt + req*req * sinlt*sinlt);

	// Calculate Cartesian coordinates of target point. This is basic 
	// geometry. The coordinate system is geocentric with the x-axis 
	// towards the spacecraft, the y-axis to the east and the x-axis 
	// towards the N pole
	const double x = rheta * coslt* cosln;
	const double y = rheta * coslt* sinln;
	const double z = rheta * sinlt;

	// Check for the invisibility. This is done using the basic geometric
	// theoreme that the dot product of two vectors A and B is equal to
	// |A||B|cos(theta)
	// where theta is the angle between them. In this case, the test is simple.
	//  The horizon is defined as the locus of points where the local normal 
	// is perpendicular to the spacecraft sighline vector. All visible points 
	// have (theta) less than 90 degrees and all invisible points have (theta) 
	// greater than 90 degrees.
	// The vector from the point to the spacecraft has the components Rs -x, -y,
	//  -z where Rs is the distance from the origin to the satellite. The vector
	//  for the normal has components x y z (Re/Rp)^2

	const double dotprod = (altitude-x)*x - y*y - z*z * pow(req/rpol, 2.);
	if (dotprod <= 0.)
	{
		line=-999;
		pixel=-999;
		return 1;
	}

	// If this coordinate system the spacecraft (S) is at position 
	// (altitude,0,0) the earth centre (0) at (0,0,0) and the point 
	// (P) at (x,y,z). Two additional points need to be defined, so that
	//  the angles from the reference planes to the target point (ie 
	// the position of the point in the sensor FOV) can be extracted. 
	// These points are defined by dropping lines perpendicularly from 
	// P onto the equatorial plane and the Greenwich meridian plane. 
	// Their coordonates are defined as :
	// 	O'=(x,y,0) and O''=(x,0,z)
	// With these points, right-angled triangles can be defined SO'P 
	// and SO''P wich can be used directly to determine the angular 
	// coordinates (aline,asamp) of P in the FOV
	double asamp = atan(y / (altitude-x));
	double aline = atan(z / sqrt(y*y + (altitude - x)*(altitude - x)));

	// Convert back to degrees
	asamp *= M_RADEG;
	aline *= M_RADEG;

	// Calculate line, pixel. Note that since samples are measured 
	// from the right of the image, and the angular conversion was 
	// measured in the x(east) direction, asign correction hes to be 
	// included for samples. The image represents an 18 x 18 degrees 
	// field of view divided up on an equi-angular basis
	
	asamp /= 18. / nsamps;
	aline /= 18. / nlines;

	if (asamp >= 0.)
	{
		pixel = nsamps / 2 - int(asamp);
	} else {
		pixel = nsamps / 2 + 1 - int(asamp);
	}
	
	if (aline >= 0.)
	{
		line = nlines / 2 + 1 + int(aline);
	} else {
		line = nlines / 2 + int(aline);
	}
	
	return 0;
}
