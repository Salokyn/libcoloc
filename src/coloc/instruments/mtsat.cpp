#include <coloc/instruments/mtsat.hpp>
#include <geo/geo.hpp>
#include <cmath>
#include <stdexcept>

// Constructeurs
Pixel_MTSAT::Pixel_MTSAT():PixelCircle(default_radius, default_relativeAltitude)
{
	m_satCoord = Coordinate(navgeo::MTSAT::default_ssp,0.);
	m_scanMode = default_scanMode;
}

Pixel_MTSAT::Pixel_MTSAT(const Coordinate& coord, const Coordinate satCoord):PixelCircle(default_radius, default_relativeAltitude, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_MTSAT::Pixel_MTSAT(const double time, const Coordinate& coord, const Coordinate satCoord):PixelCircle(default_radius, default_relativeAltitude, time, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

Pixel_MTSAT* Pixel_MTSAT::clone() const
{
	return new Pixel_MTSAT(*this);
}

Image_MTSAT::Image_MTSAT(const size_t ncols, const size_t nrows, const size_t startCol, const size_t startRow):
ImageWithNavigationGeo(navgeo::MTSAT(), ncols, nrows, startCol, startRow)
{
}
