#include <coloc/image.hpp>
#include <stdexcept>
#include <cmath>
#include <algorithm>


Image::Image(const size_t ncols, const size_t nrows):m_ncols(ncols),m_nrows(nrows)
{
	m_pixels = new Pixel*[size()];
	
	for (size_t i=0; i < size(); i++)
	{
		(*this)[i] = NULL;
	}
}

Image::Image(const Image& copy)
{
	*this = copy;
}

Image::~Image()
{
	free();
	delete[] m_pixels;
}

void Image::free()
{
	for (size_t i=0; i < size(); i++)
	{
		if ((*this)[i] != NULL)
		{
			delete (*this)[i];
			(*this)[i] = NULL;
		}
	}
}

const Pixel *const & Image::operator[](const size_t i) const
{
	if (i >= size()) throw std::out_of_range("Pixel* Image::operator[](size_t i) const : Given index is greater than Image size !");
	return (const Pixel *const &) m_pixels[i]; // J'ai un warning si je ne caste pas.
}

const Pixel *const & Image::operator()(long c, long r) const
{
	while (c < 0) c += ncols();
	c %= ncols();
	r = std::max(std::min(r, (long) nrows()-1), (long) 0);
	return (*this)[(size_t) r*ncols()+(size_t) c];
}

Pixel*& Image::operator[](const size_t i)
{
	if (i >= size()) throw std::out_of_range("Pixel* Image::operator[](size_t i) const : Given index is greater than Image size !");
	return m_pixels[i];
}

Pixel*& Image::operator()(long c, long r)
{
	while (c < 0) c += ncols();
	c %= ncols();
	r = std::max(std::min(r, (long) nrows()-1), (long) 0);
	return (*this)[(size_t) r*ncols()+(size_t) c];
}

void Image::set(const size_t c, const size_t r, const Pixel& pixel)
{
	set(r*ncols()+c, pixel);
}

void Image::set(const size_t i, const Pixel& pixel)
{
	if (i >= size()) throw std::out_of_range("void Image::set(size_t i, Pixel* pixel) : Given index is greater than Image size !");
	delete (*this)[i];
	(*this)[i] = pixel.clone();
}

void Image::setValue(const double* array, const size_t index)
{
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < size(); i++)
	{
		Pixel* pixel = (*this)[i];
		if (pixel == NULL) continue;
		pixel->setValue(array[i], index);
	}
}

void Image::setTime(const double time)
{
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < size(); i++)
	{
		Pixel* pixel = (*this)[i];
		if (pixel == NULL) continue;
		pixel->setTime(time);
	}
}

void Image::setTime(const Date& date)
{
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < size(); i++)
	{
		Pixel* pixel = (*this)[i];
		if (pixel == NULL) continue;
		pixel->setTime(date);
	}
}

void Image::setValue(const std::vector<double>& vector, const size_t index)
{
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < std::min(size(), vector.size()); i++)
	{
		Pixel* pixel = (*this)[i];
		if (pixel == NULL) continue;
		pixel->setValue(vector[i], index);
	}
}

const size_t& Image::ncols() const { return m_ncols; }

const size_t& Image::nrows() const { return m_nrows; }

size_t Image::size() const { return m_ncols*m_nrows; }

const Pixel *const & Image::geo2pix(const Coordinate& coord, size_t& col, size_t& row) const
{
	static size_t scol = ncols()/2;
	static size_t srow = nrows()/2;
	findpixcoord(coord, col, row, &scol, &srow);
	
	return (*this)(col, row);
}

Image& Image::operator=(const Image& copy)
{
	if (this == &copy) return *this;
	
	free();
	delete[] m_pixels;
	
	m_pixels = new Pixel*[copy.size()];
	
	for (size_t i=0; i < size(); i++)
	{
		if (copy[i] != NULL) (*this)[i] = copy[i]->clone();
		else (*this)[i] = NULL;
	}
	
	return *this;
}

unsigned Image::findpixcoord(const Coordinate& coord, size_t& col, size_t& row, size_t* scol, size_t* srow) const
{
	const unsigned imax = 50;
	const double gain = .85;
	const double limit = .33;
	unsigned i;

	double last_elon=0., last_elat=0.;

	//Valeurs par défaut
	col=ncols()/2;
	row=nrows()/2;

	if (scol != NULL) col=std::min(std::max( *scol, (size_t) 1), ncols()-2);
	if (srow != NULL) row=std::min(std::max( *srow, (size_t) 1), nrows()-2);

	// Il faut s'assurer que ce pixel initial existe
	i = 0;
	while (i < 100 and (*this)(col, row) == NULL)
	{
		col = rand() % ncols();
		row = rand() % nrows();
		i++;
	}
	if ((*this)(col, row) == NULL) throw std::runtime_error("Can not find a non NULL initial pixel.");

	i = 0;
	while ( i < imax )
	{
		const Pixel* pixelA = (*this)(col,row);
		const Pixel *const pixelB = (*this)(col-1,row);
		const Pixel *const pixelC = (*this)(col,row-1);
		const Pixel *const pixelD = (*this)(col+1,row);
		const Pixel *const pixelE = (*this)(col,row+1);
	
		if (pixelA == NULL or pixelB == NULL or pixelC == NULL or pixelD == NULL or  pixelE == NULL) throw std::runtime_error("NULL Pixel");
	
		// écart mesure-consigne
		double elon = geo::to_180_180(coord.lon() - pixelA->lon());
		const double elat = coord.lat() - pixelA->lat();
	
		// teste la convergence
		if ( std::abs(last_elon) == std::abs(elon) and std::abs(last_elat) == std::abs(elat) ) break;
	
		last_elon = elon;
		last_elat = elat;
	
		// calcul de la dérivée
		double dlon = geo::to_180_180((pixelD->lon() - pixelB->lon()) / 2.);
		const double dlat = (pixelE->lat() - pixelC->lat()) / 2.;
	
		// calcul de la correction (limitée pour éviter de sortir dans l'espace)
		const int dcol = (int) round(std::min(std::max(elon/dlon*gain,-limit*(double) ncols()),limit*(double) ncols()));
		const int drow = (int) round(std::min(std::max(elat/dlat*gain,-limit*(double) nrows()),limit*(double) nrows()));
	
		// application de la correction
		col = std::min(std::max( col+dcol, (size_t) 1), ncols()-2);
		row = std::min(std::max( row+drow, (size_t) 1), nrows()-2);
	
		pixelA = (*this)(col,row);
	
		// garde fou
		if (pixelA == NULL)
		{
			col -= dcol/2;
			row -= drow/2;
		}
	
		// test si le pixel est toujours NULL
		if (pixelA == NULL) throw std::runtime_error("NULL Pixel");
	
		i++;
	}

	if (scol != NULL) *scol = col;
	if (srow != NULL) *srow = row;

	return i;
}


ImageRegular::ImageRegular(const size_t ncols, const size_t nrows, const Coordinate& top_left, const Coordinate& bottom_right, const Pixel *const pixel_template):
Image(ncols,nrows),
m_top_left(top_left),
m_bottom_right(bottom_right)
{
	if (ncols <= 1 or nrows <= 1) throw std::runtime_error("ImageRegular::ImageRegular(const size_t ncols, const size_t nrows, const Coordinate& top_left, const Coordinate& bottom_right): ncols <= 1 or nrows <= 1. ncols and nrows must be greater than 1.");
	
	double deltalon = geo::to_0_360(bottom_right.lon()-top_left.lon());
	m_xresolution = deltalon/(ncols-1);
	
	m_yresolution = (top_left.lat()-bottom_right.lat())/(nrows-1);
	if (m_yresolution < 0.) throw std::runtime_error("ImageRegular::ImageRegular(const size_t ncols, const size_t nrows, const Coordinate& top_left, const Coordinate& bottom_right): Y resolution is negative. top_left should have a higher latitude than bottom_left.");
	
	// Initialize the Pixels
	if (pixel_template != NULL)
	{
		fill(*pixel_template);
	}
}

Coordinate ImageRegular::pix2geo(const size_t col, const size_t row) const
{
	return Coordinate(m_top_left.lon()+col*m_xresolution, m_top_left.lat()-row*m_yresolution);
}

Coordinate ImageRegular::pix2geo(const size_t index) const
{
	const size_t col = index%ncols();
	const size_t row = index/ncols();
	return pix2geo(col, row);
}

const Pixel *const & ImageRegular::geo2pix(const Coordinate& coord, size_t& col, size_t& row) const
{
	double deltalon = geo::to_180_180(coord.lon()-m_top_left.lon());
	if (deltalon < -m_xresolution/2.) deltalon += 360.;
	col = (size_t) round(deltalon/m_xresolution);
	
	row = (size_t) round((m_top_left.lat()-coord.lat())/m_yresolution);
	
	if (col >= ncols() or row >= nrows()) throw std::out_of_range("Coordinate out of image.");
	
	return (*this)(col, row);
}

void ImageRegular::fill(const Pixel& pixel_template)
{
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < size(); i++)
	{
		delete (*this)[i];
		
		Pixel* pixel = pixel_template.clone();
		
		pixel->setCoord(Coordinate(pix2geo(i)));
		
		(*this)[i] = pixel;
	}
}

const double& ImageRegular::xResolution() const
{
	return m_xresolution;
}

const double& ImageRegular::yResolution() const
{
	return m_yresolution;
}

ImageWithNavigationGeo::ImageWithNavigationGeo(const navgeo::NavigationGeo& navigation, const size_t ncols, const size_t nrows, const size_t startCol, const size_t startRow):
Image(ncols, nrows),
m_startCol(startCol),
m_startRow(startRow),
m_navigation(new navgeo::NavigationGeo(navigation))
{}

ImageWithNavigationGeo::~ImageWithNavigationGeo()
{
	delete m_navigation;
}

const Pixel *const & ImageWithNavigationGeo::geo2pix(const Coordinate& coord, size_t& col, size_t& row) const
{
	m_navigation->geo2pix(coord, col, row);
	
	col -= m_startCol;
	row -= m_startRow;
	
	if (col >= m_ncols or row >= m_nrows) throw std::out_of_range("void ImageWithNavigationGeo::geo2pix(const Coordinate& coord, size_t& col, size_t& row) const : Index out of image.");
	
	return (*this)(col, row);
}

void ImageWithNavigationGeo::fill(const Pixel& pixel_template)
{
	#pragma omp parallel for schedule(dynamic)
	for (size_t i=0; i < size(); i++)
	{
		Coordinate coord;
		try
		{
			size_t col = i%ncols() + m_startCol;
			size_t row = i/ncols() + m_startRow;
			coord = navigation().pix2geo(col, row);
		}
		catch(const std::exception& e)
		{
			continue;
		}
		
		Pixel* pixel;
		
		try
		{
			pixel = pixel_template.clone();
			pixel->setCoord(coord);
			delete (*this)[i];
			(*this)[i] = pixel;
		}
		catch(const std::exception& e) // Can happen when VZA > 90.
		{
			continue;
		}
	}
}

const navgeo::NavigationGeo& ImageWithNavigationGeo::navigation() const
{
	return *m_navigation;
}
