#include <coloc/coloc.hpp>
#include <cmath>
#include <geo/geo.hpp>
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <functional>
#include <string>
#include <stdexcept>
#include <omp.h>

static inline double drand()
{
	// Génération d'un nombre aléatoire entre 0 et 1
	return (double) rand() / ((double)(RAND_MAX) +1.);
}

Colocation::Colocation(const Orbit& orbitA, const Orbit& orbitB, const bool verbose):
m_orbitA(&orbitA),
m_orbitB(new Orbit(orbitB)),
m_imageB(NULL),
m_verbose(verbose),
m_progression_interval(0.),
m_IPpB(default_IPpB),
m_IPmin(default_IPmin),
m_IPmax(default_IPmax),
m_rootFindingMax(default_rootFindingMax),
m_integrationRule(default_integrationRule),
m_timeThreshold(default_timeThreshold),
m_zenithThreshold(default_zenithThreshold),
m_azimuthThreshold(default_azimuthThreshold),
m_coneThreshold(default_coneThreshold),
m_nightThreshold(default_nightThreshold),
m_nColocatedPixels(0)
{
	// Tri de l'orbite B
	if (m_verbose) std::cout << "Tri de l'orbitB ... " << std::flush;
	m_orbitB->sort();
	if (m_verbose) std::cout << "fait" << std::endl;
}

Colocation::Colocation(const Orbit& orbitA, const Image& imageB, const bool verbose):
m_orbitA(&orbitA),
m_orbitB(NULL),
m_imageB(&imageB),
m_verbose(verbose),
m_progression_interval(0.),
m_IPpB(default_IPpB),
m_IPmin(default_IPmin),
m_IPmax(default_IPmax),
m_rootFindingMax(default_rootFindingMax),
m_integrationRule(default_integrationRule),
m_timeThreshold(default_timeThreshold),
m_zenithThreshold(default_zenithThreshold),
m_azimuthThreshold(default_azimuthThreshold),
m_coneThreshold(default_coneThreshold),
m_nightThreshold(default_nightThreshold),
m_nColocatedPixels(0)
{
}

Colocation::~Colocation()
{
	if (m_orbitB != NULL) delete m_orbitB;
}

size_t Colocation::size() const
{
	return m_link.size();
}

const std::vector< std::vector<Colocation::dPpair> >& Colocation::link() const
{
	return m_link;
}

const double& Colocation::meanPSF(const size_t i) const
{
	if (i >= m_meanPSF.size()) throw std::out_of_range("double Colocation::meanPSF(size_t i) const : Given index is greater than Orbit size !");
	return m_meanPSF[i];
}

const std::vector<double>& Colocation::meanPSF() const
{
	return m_meanPSF;
}

const Colocation::ColocationFlag& Colocation::flag(const size_t i) const
{
	if (i >= m_colocationFlag.size()) throw std::out_of_range("Colocation::ColocationFlag Colocation::flag(size_t i) const : Given index is greater than Orbit size !");
	return m_colocationFlag[i];
}

const std::vector<Colocation::ColocationFlag>& Colocation::flag() const
{
	return m_colocationFlag;
}

std::vector<unsigned long> Colocation::flagUlong() const
{
	size_t size = m_colocationFlag.size();
	std::vector<unsigned long> out(size);
	for (size_t i=0; i < size; i++)
	{
		out[i] = m_colocationFlag[i].to_ulong();
	}
	return out;
}

unsigned long Colocation::flagUlong(const size_t i) const
{
	return flag(i).to_ulong();
}

const size_t& Colocation::nIntegrationPoints(const size_t i) const
{
	if (i >= m_nIP.size()) throw std::out_of_range("size_t Colocation::nIntegrationPoints(size_t i) const : Given index is greater than Orbit size !");
	return m_nIP[i];
}

const std::vector<size_t>& Colocation::nIntegrationPoints() const
{
	return m_nIP;
}

const size_t& Colocation::nColocatedPixels() const
{
	return m_nColocatedPixels;
}

const double& Colocation::timeThreshold() const
{
	return m_timeThreshold;
}

void Colocation::run()
{
	const size_t sizeA = m_orbitA->size();
	m_meanPSF.resize(sizeA, 0.);
	m_nIP.resize(sizeA, 0);
	m_colocationFlag.resize(sizeA);
	m_link.resize(sizeA);
	m_nColocatedPixels = 0;
	
	if (m_integrationRule == INT_MONTECARLO)
	{
		srand(time(NULL)); // génération d'une random seed
		m_irand = 0;
		
		const size_t nbrandMax = default_nbrandMax; // Maximum number of generated random numbers.
		const size_t nrand = std::min(2*m_IPmax*m_orbitA->size(), nbrandMax);
		
		if (m_verbose) std::cout << "Génération des nombres aléatoires ... " << std::flush;
		m_randomPool.resize(nrand);
		std::generate(m_randomPool.begin(), m_randomPool.end(), drand);
		if (m_verbose) std::cout << "fait" << std::endl;
	}
	
	//================//
	// Colocalisation //
	//================//
	if (m_verbose) std::cout << "Colocalisation de " << m_orbitA->size() << " pixels ... " << std::flush;
	time(&m_startTime);
	if (m_orbitB != NULL) colocOrbit();
	else
	if (m_imageB != NULL) colocImage();
	time(&m_endTime);
	if (m_verbose) std::cout << "fait" << std::endl;
	
	if (m_integrationRule == INT_MONTECARLO) m_randomPool.clear(); // Random pool is not usefull anymore
}

void Colocation::clear()
{
	m_meanPSF.clear();
	m_colocationFlag.clear();
	m_nIP.clear();
	m_link.clear();
}

double Colocation::mean(const size_t iOrbitA, const size_t index, const double missingValue) const
{
	const size_t size = link()[iOrbitA].size();
	
	if (size == 0) throw std::runtime_error("double Colocation::mean(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	double numer = 0.;
	double denom = 0.;
	for (size_t i = 0; i < size; i++)
	{
		if (link()[iOrbitA][i].second == NULL) continue; // This should not happen anyway
		
		const double value = link()[iOrbitA][i].second->value(index);
		if (value == missingValue or std::isnan(missingValue) and std::isnan(value)) continue;
		
		numer += link()[iOrbitA][i].first * value;
		denom += link()[iOrbitA][i].first;
	}
		
	if (denom == 0.) throw std::runtime_error("double Colocation::mean(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	return numer/denom;
}

double Colocation::stdev(const size_t iOrbitA, const size_t index, const double missingValue) const
{
	const size_t size = link()[iOrbitA].size();
	
	if (size == 0) throw std::runtime_error("double Colocation::stdev(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	double m = mean(iOrbitA, index);
	
	double numer = 0.;
	double denom = 0.;
	for (size_t i = 0; i < size; i++)
	{
		if (link()[iOrbitA][i].second == NULL) continue; // This should not happen anyway
		
		const double value = link()[iOrbitA][i].second->value(index);
		if (value == missingValue or std::isnan(missingValue) and std::isnan(value)) continue;
		
		numer += link()[iOrbitA][i].first * pow(value - m, 2.);
		denom += link()[iOrbitA][i].first;
	}
	
	if (denom == 0.) throw std::runtime_error("double Colocation::stdev(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	return sqrt(numer/denom);
}

double Colocation::min(const size_t iOrbitA, const size_t index, const double missingValue) const
{
	const size_t size = link()[iOrbitA].size();
	
	if (size == 0) throw std::runtime_error("double Colocation::min(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	size_t start = 0;
	
	// Cherche le premier pixel non NULL
	while (start < size and link()[iOrbitA][start].second == NULL) start++;
	if (start >= size) throw std::runtime_error("double Colocation::min(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	// Cherche le premier pixel dont la valeur n'est pas une missingValue
	while (start < size and link()[iOrbitA][start].second->value(index) == missingValue or std::isnan(missingValue) and std::isnan(link()[iOrbitA][start].second->value(index))) start++;
	if (start >= size) throw std::runtime_error("double Colocation::min(const size_t iOrbitA, const size_t index) const : No valid value found.");
	
	double min = link()[iOrbitA][start].second->value(index);
	
	for (size_t i = start+1; i < size; i++)
	{
		if (link()[iOrbitA][i].second == NULL) continue;
		
		const double value = link()[iOrbitA][i].second->value(index);
		if (value == missingValue or std::isnan(missingValue) and std::isnan(value)) continue;
		
		if (value < min) min = value;
	}
	
	return min;
}

double Colocation::max(const size_t iOrbitA, const size_t index, const double missingValue) const
{
	const size_t size = link()[iOrbitA].size();
	
	if (size == 0) throw std::runtime_error("double Colocation::max(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	size_t start = 0;
	
	// Cherche le premier pixel non NULL
	while (start < size and link()[iOrbitA][start].second == NULL) start++;
	if (start >= size) throw std::runtime_error("double Colocation::max(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	// Cherche le premier pixel dont la valeur n'est pas une missingValue
	while (start < size and link()[iOrbitA][start].second->value(index) == missingValue or std::isnan(missingValue) and std::isnan(link()[iOrbitA][start].second->value(index))) start++;
	if (start >= size) throw std::runtime_error("double Colocation::max(const size_t iOrbitA, const size_t index) const : No valid value found.");
	
	double max = link()[iOrbitA][start].second->value(index);
	
	for (size_t i = start+1; i < size; i++)
	{
		if (link()[iOrbitA][i].second == NULL) continue;
		
		const double value = link()[iOrbitA][i].second->value(index);
		if (value == missingValue or std::isnan(missingValue) and std::isnan(value)) continue;
		
		if (value > max) max = value;
	}
	
	return max;
}

std::vector<Colocation::dpair> Colocation::histogram(const size_t iOrbitA, const size_t index, const HistogramNormalizationType normalization, const double missingValue) const
{
	size_t size = link()[iOrbitA].size();
	
	if (size == 0) throw std::runtime_error("std::vector<Colocation::dpair> Colocation::histogram(const size_t iOrbitA, const size_t index, const HistogramNormalizationType normalization, const double missingValue) const : No pixel have been colocated.");
	
	std::vector<dpair> values;
	values.reserve(size);
	double sum_psf = 0.;
	for (size_t i = 0; i < size; i++)
	{
		if (link()[iOrbitA][i].second == NULL) continue; // This should not happen anyway
		
		const double value = link()[iOrbitA][i].second->value(index);
		if (value == missingValue or std::isnan(missingValue) and std::isnan(value)) continue;
		
		values.push_back( dpair(value, link()[iOrbitA][i].first) );
		sum_psf += link()[iOrbitA][i].first;
	}
	
	if (sum_psf == 0.) throw std::runtime_error("std::vector<Colocation::dpair> Colocation::histogram(const size_t iOrbitA, const size_t index, const HistogramNormalizationType normalization, const double missingValue) const : No pixel have been colocated or no valid value found.");
	
	// tri selon les valeurs.
	std::sort(values.begin(), values.end());
	
	size = values.size();
	
	std::vector<dpair> histogram; // PSF en 1er, valeur en 2e
	histogram.reserve(size);
	
	// initialise last_val tel qu'il soit différent de values[0].first
	double last_val = round(values[0].first)-1.;
	for (size_t i = 0; i < size; i++)
	{
		double value = round(values[i].first);
		double psf = values[i].second;
		
		if (value == last_val)
		{
			histogram[histogram.size()-1].first += psf;
		} else {
			histogram.push_back( dpair(psf, value) );
			last_val = value;
		}
	}
	
	// normalisation des poids
	if (normalization)
	{
		double quantization = sum_psf;
		
		if (normalization == NORM_SURFACE) quantization /= meanPSF()[iOrbitA];
		
		for (size_t i = 0; i < histogram.size(); i++)
		{
			histogram[i].first /= quantization;
		}
	}
	
	// tri selon la PSF
	std::sort(histogram.begin(),histogram.end(), std::greater<dpair>());
	
	return histogram;
}

double Colocation::mode(const size_t iOrbitA, const size_t index) const
{
	const size_t size = link()[iOrbitA].size();
	
	if (size == 0) throw std::runtime_error("double Colocation::mode(const size_t iOrbitA, const size_t index) const : No pixel have been colocated.");
	
	return histogram(iOrbitA, index)[0].second;
}

double Colocation::postTreatment(const size_t iOrbitA, const size_t index) const
{
	return mean(iOrbitA, index);
}

void Colocation::colocOrbit()
/******************************************************************************\
* AUTH: Nicolas Gif <nicolas.gif@lmd.polytechnique.fr>                         *
* DATE: mar. 08 juin 2010 11:00:07 CEST                                        *
* TYPE: Fonction                                                               *
* DESC: Code de colocalisation Défilant VS Défilant                            *
* INPT: Orbit&, Orbit&                                                         *
* OUPT: Orbit&                                                                 *
* RTRN: void                                                                   *
\******************************************************************************/
{
	//=================//
	// Initialisations //
	//=================//
	
	const size_t nOrbitA = m_orbitA->size();
	const size_t nOrbitB = m_orbitB->size();
	
	if (nOrbitA == 0)
	{
		throw std::length_error("void Colocation::colocOrbit() : OrbitA is empty !");
	}
	
	if (nOrbitB == 0)
	{
		throw std::length_error( "void Colocation::colocOrbit() : OrbitB is empty !");
	}
	
	// Recherche des min et max des latitudes et des temps de l'orbitB
	size_t start=0;
	while(start < nOrbitB and ((*m_orbitB)[start] == NULL or !(*m_orbitB)[start]->proceed())) start++;
	if (start >= nOrbitB) throw std::runtime_error("All PixelB's are NULL or deactivated.");
	
	double minTimeB = (*m_orbitB)[start]->time();
	double maxTimeB = (*m_orbitB)[start]->time();
	size_t iMinTimeB = start;
	size_t iMaxTimeB = start;
	double minLatB = (*m_orbitB)[start]->lat()-(*m_orbitB)[start]->radius()*(*m_orbitB)[start]->K()/geo::r_earth*M_RADEG;
	double maxLatB = (*m_orbitB)[start]->lat()+(*m_orbitB)[start]->radius()*(*m_orbitB)[start]->K()/geo::r_earth*M_RADEG;
	for (size_t i=start; i < nOrbitB; i++)
	{
		if ((*m_orbitB)[i] == NULL or !(*m_orbitB)[i]->proceed()) continue;
		
		double time = (*m_orbitB)[i]->time();
		if (time < minTimeB)
		{
			minTimeB = time;
			iMinTimeB = i;
		}
		if (time >= maxTimeB)
		{
			maxTimeB = time;
			iMaxTimeB = i;
		}
		
		double lat = (*m_orbitB)[i]->lat();
		double latSpan = (*m_orbitB)[i]->radius()*(*m_orbitB)[i]->K()/geo::r_earth*M_RADEG;
		if (lat-latSpan < minLatB) minLatB = lat-latSpan;
		if (lat+latSpan > maxLatB) maxLatB = lat+latSpan;
	}
	
	// Recherche des min et max des temps de l'orbiteA
	start=0;
	while(start < nOrbitA and ((*m_orbitA)[start] == NULL or !(*m_orbitA)[start]->proceed())) start++;
	if (start >= nOrbitA) throw std::runtime_error("All PixelA's are NULL or deactivated.");
	
	double minTimeA = (*m_orbitA)[start]->time();
	double maxTimeA = (*m_orbitA)[start]->time();
	for (size_t i=start; i < nOrbitA; i++)
	{
		if ((*m_orbitA)[i] == NULL or !(*m_orbitA)[i]->proceed()) continue;
		
		double time = (*m_orbitA)[i]->time();
		if (time < minTimeA)
		{
			minTimeA = time;
		}
		if (time > maxTimeA)
		{
			maxTimeA = time;
		}
	}
	
	//==============================================//
	// Recherche des limites de recherches en temps //
	// dans orbitB par la méthode de la sécante     //
	//==============================================//
	size_t iMinTimeA = iMinTimeB;
	size_t iMaxTimeA = iMaxTimeB;
	
	if (maxTimeB > minTimeB and minTimeA-timeThreshold() > minTimeB and maxTimeA+timeThreshold() < maxTimeB)
	{
		long left;
		long right;
		
		left  = iMinTimeB;
		right = iMaxTimeB;
		
		// Recherche de (minTimeA-timeThreshold())
		if (m_orbitB->falsiMethod(minTimeA-timeThreshold(), left, right, nOrbitB) < nOrbitB)
		{
			iMinTimeA = left;
		}
		
		right = iMaxTimeB;
		
		// Recherche de (maxTimeA+timeThreshold())
		if (m_orbitB->falsiMethod(maxTimeA+timeThreshold(), left, right, nOrbitB) < nOrbitB)
		{
			iMaxTimeA = right;
		}
	}
	
	// Recherche le rayon de pixelB maximum
	double maxRadiusB = 0.;
	for (size_t i=iMinTimeA; i <= iMaxTimeA; i++)
	{
		double radius = (*m_orbitB)[i]->radius()*(*m_orbitB)[i]->K();
		if (radius > maxRadiusB) maxRadiusB = radius;
	}
	
	//==============================//
	// boucle sur les pixels orbitA //
	//==============================//
	if (m_progression_interval > 0. and m_verbose) std::cout << std::endl;
	double last_progression = -100.;

	#pragma omp parallel for schedule(dynamic)
	for (long int iOrbitA=0; iOrbitA < (long int) nOrbitA; iOrbitA++)
	{
		const Pixel *const pixelA = (*m_orbitA)[iOrbitA];
		
		m_colocationFlag[iOrbitA].reset();
		m_link[iOrbitA].clear();
		
		if (pixelA == NULL)
		{
			m_colocationFlag[iOrbitA].set(FLAG_NULL);
			continue;
		}
		
		// regarde si on doit traiter ce pixel
		if (!pixelA->proceed())
		{
			m_colocationFlag[iOrbitA].set(FLAG_NO_PROCEEDA);
			continue;
		}
		
		m_link[iOrbitA].clear();
		
		// Prints progression
		if (omp_get_thread_num() == 0 and m_progression_interval > 0.)
		{
			const double progression = 100.* (double)iOrbitA / (double)nOrbitA;
			if (progression - last_progression >= m_progression_interval)
			{
				std::cout << progression << std::endl;
				last_progression = progression;
			}
		}
		
		// test sur la latitude (cos(lat) doit être non nul)
		if (std::abs(pixelA->lat()) > 89.)
		{
			m_colocationFlag[iOrbitA].set(FLAG_LAT);
			continue;
		}
	
		// teste si on peut trouver des pixels
		if (pixelA->time()-timeThreshold() > maxTimeB or pixelA->time()+timeThreshold() < minTimeB)
		{
			m_colocationFlag[iOrbitA].set(FLAG_SELECT_TIME);
			continue;
		}
		
		double radiusA;
		if (!pixelA->distortable() or pixelA->cosalpha() == 1.)
		{
			radiusA = pixelA->radius()*pixelA->K();
		} else {
			radiusA = pixelA->radius();
		}
		double latSpanA = radiusA/geo::r_earth*M_RADEG;
		
		if (pixelA->lat()-latSpanA > maxLatB or pixelA->lat()+latSpanA < minLatB)
		{
			m_colocationFlag[iOrbitA].set(FLAG_LAT);
			continue;
		}
	
		// teste la nuit
		if (m_config.test(COLOC_NO_NIGHT) and pixelA->night(m_nightThreshold, geo::DEGREE))
		{
			m_colocationFlag[iOrbitA].set(FLAG_NIGHT);
			if (!m_config.test(COLOC_FORCE)) continue;
		}
		
		//==============================================//
		// Recherche des limites de recherches en temps //
		// dans orbitB par la méthode de la sécante     //
		//==============================================//
		
		size_t istart = iMinTimeA;
		size_t iend = iMaxTimeA;
		
		if (maxTimeB > minTimeB)
		{
			long left;
			long right;
			
			left  = iMinTimeA;
			right = iMaxTimeA;
			
			// Recherche de (pixelA->time()-timeThreshold())
			if (m_orbitB->falsiMethod(pixelA->time()-timeThreshold(), left, right, m_rootFindingMax) >= m_rootFindingMax)
			{
				m_colocationFlag[iOrbitA].set(FLAG_ROOTFINDING);
			}
			istart = left;
			
			right = iMaxTimeA;
			
			// Recherche de (pixelA->time()+timeThreshold())
			if (m_orbitB->falsiMethod(pixelA->time()+timeThreshold(), left, right, m_rootFindingMax) >= m_rootFindingMax)
			{
				m_colocationFlag[iOrbitA].set(FLAG_ROOTFINDING);
			}
			iend = right;
		}
		
		if (iend < istart)
		{
			m_colocationFlag[iOrbitA].set(FLAG_ROOTFINDING);
			continue;
		}
		
		const double latThreshold = (radiusA + maxRadiusB)/geo::r_earth*M_RADEG;
		const double lonThreshold = latThreshold / cos(pixelA->lat(geo::RADIAN));
		
		//=======================================================//
		// sélection des pixelB susceptibles d'êtres colocalisés //
		//=======================================================//
	
		// Définition du vecteur qui contiendra les indices des pixelB sélectionnés
		std::vector<const Pixel*> selectB;
		
		size_t nb_spatial=0, nb_temp=0, nb_angular=0, nb_proceed=0;
		
		//=======================//
		// boucle sur les pixelB //
		//=======================//
		for (size_t i=istart; i<=iend; i++)
		{
			const Pixel *const pixelB = (*m_orbitB)[i];
			
			// regarde si on doit traiter ce pixel
			if (!pixelB->proceed()) continue;
			
			nb_proceed++;
			
			//===========================//
			// colocaliastion temporelle //
			//===========================//
			if (std::abs(pixelB->time()-pixelA->time()) > timeThreshold()) continue;
			
			nb_temp++;
			
			//=========================//
			// colocaliastion spatiale //
			//=========================//
			if (pixelA->coord() != pixelB->coord())
			{
				// Test sur la latitude
				if ( std::abs(pixelB->lat()-pixelA->lat()) > latThreshold ) continue;
				
				// Test sur la longitude
				if ( std::abs(geo::to_180_180(pixelB->lon()-pixelA->lon())) > lonThreshold ) continue;
				
				// Test sur la distance
				double dist, azi;
				
				try
				{
					dist = geo::map_2points(pixelA->coord(), pixelB->coord(), &azi); // en m
				}
				catch (const std::exception& e) // Ne doit arriver que si les 2 pixels sont aux antipodes.
				{
					continue;
				}
				
				double rA;
				if (!pixelA->distortable() or pixelA->cosalpha() == 1.)
				{
					rA = pixelA->radius();
				}
				else
				{
					try
					{
						rA = geo::radius_ellipse(pixelA->radius()*pixelA->K(), pixelA->radius()*pixelA->L(), pixelA->viewingAzimuthAngle()-azi);
					}
					catch (const std::exception& e)
					{
						rA = pixelA->radius()*pixelA->K();
					}
				}
				
				double rB;
				if (!pixelB->distortable() or pixelB->cosalpha() == 1.)
				{
					rB = pixelB->radius();
				}
				else
				{
					try
					{
						rB = geo::radius_ellipse(pixelB->radius()*pixelB->K(), pixelB->radius()*pixelB->L(), pixelB->viewingAzimuthAngle()-azi);
					}
					catch (const std::exception& e)
					{
						rB = pixelB->radius()*pixelB->K();
					}
				}
				
				if (dist >= rA + rB) continue;
				
			}
			
			nb_spatial++;
			
			//==========================//
			// colocalisation angulaire //
			//==========================//
			try
			{
				if (!coloc_ang(pixelA, pixelB))
				{
					if (!m_config.test(COLOC_FORCE)) continue;
				} else {
					nb_angular++;
				}
			}
			catch (const std::exception& e)
			{
				continue;
			}
			
			selectB.push_back(pixelB);
		}
		
		if (nb_proceed==0)
		{
			m_colocationFlag[iOrbitA].set(FLAG_NO_PROCEEDB);
			continue;
		}
		if (nb_temp==0)
		{
			m_colocationFlag[iOrbitA].set(FLAG_SELECT_TIME);
			continue;
		}
		if (nb_spatial==0)
		{
			m_colocationFlag[iOrbitA].set(FLAG_SELECT_COORD);
			continue;
		}
		if (nb_angular==0)
		{
			m_colocationFlag[iOrbitA].set(FLAG_SELECT_ANGLE);
		}
	
		const size_t nSelectB = selectB.size();
		
		if (nSelectB == 0) continue;
		
		if (m_integrationRule == INT_OFF)
		{
			m_colocationFlag[iOrbitA].set(FLAG_INT_OFF);
			continue;
		}
		
		double height, width;
		
		if (!pixelA->distortable() or pixelA->cosalpha() == 1.)
		{
			height = atan(2.*pixelA->radius()/geo::r_earth) * M_RADEG;
			width  = atan(2.*pixelA->radius()/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN)); // La division par le cos(lat) vient du fait que les méridiens se rapprochent aux hautes latitudes
		}
		else
		{
			try
			{
				// Définition du rectangle de recherche
				height = atan(2.*geo::radius_ellipse(pixelA->radius()*pixelA->K(), pixelA->radius()*pixelA->L(), pixelA->viewingAzimuthAngle())/geo::r_earth) * M_RADEG;
				width  = atan(2.*geo::radius_ellipse(pixelA->radius()*pixelA->K(), pixelA->radius()*pixelA->L(), 90.+pixelA->viewingAzimuthAngle())/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN)); // La division par le cos(lat) vient du fait que les méridiens se rapprochent aux hautes latitudes
			}
			catch (const std::exception& e)
			{
				height = atan(2.*pixelA->radius()*pixelA->K()/geo::r_earth) * M_RADEG;
				width  = atan(2.*pixelA->radius()*pixelA->K()/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN)); // La division par le cos(lat) vient du fait que les méridiens se rapprochent aux hautes latitudes
			}
		}
		
		//=============//
		// Intégration //
		//=============//
		try
		{
			integration(iOrbitA, width, height, selectB);
		}
		catch (const std::exception& e)
		{
			m_colocationFlag[iOrbitA].set(FLAG_NO_COLOC);
			continue;
		}
	} // iOrbitA // fin de la boucle parallèle
} // coloc


void Colocation::colocImage()
/******************************************************************************\
* AUTH: Nicolas Gif <nicolas.gif@lmd.polytechnique.fr>                         *
* DATE: mar. 08 juin 2010 11:00:07 CEST                                        *
* TYPE: Fonction                                                               *
* DESC: Code de colocalisation Défilant VS Défilant                            *
* INPT: Orbit&, Orbit&                                                         *
* OUPT: Orbit&                                                                 *
* RTRN: void                                                                   *
\******************************************************************************/
{
	//=================//
	// Initialisations //
	//=================//
	
	const size_t nOrbitA = m_orbitA->size();
	const size_t nImageB = m_imageB->size();
	
	if (nOrbitA == 0)
	{
		throw std::length_error("void Colocation::colocImage() : OrbitA is empty !");
	}
	
	if (nImageB == 0)
	{
		throw std::length_error("void Colocation::colocImage() : imageB is empty !");
	}
	
	// Recherche des min et max des latitudes et des temps de l'image
	size_t start=0;
	while(start < nImageB and ((*m_imageB)[start] == NULL or !(*m_imageB)[start]->proceed())) start++;
	if (start >= nImageB) throw std::runtime_error("All PixelB's are NULL or deactivated.");
	
	double minTimeB = (*m_imageB)[start]->time();
	double maxTimeB = (*m_imageB)[start]->time();
	double minLatB = (*m_imageB)[start]->lat()-(*m_imageB)[start]->radius()*(*m_imageB)[start]->K();
	double maxLatB = (*m_imageB)[start]->lat()+(*m_imageB)[start]->radius()*(*m_imageB)[start]->K();
	for (size_t i=start; i < nImageB; i++)
	{
		if ((*m_imageB)[i] == NULL or !(*m_imageB)[i]->proceed()) continue;
		
		double time = (*m_imageB)[i]->time();
		if (time < minTimeB) minTimeB = time;
		if (time > maxTimeB) maxTimeB = time;
		
		double lat = (*m_imageB)[i]->lat();
		double radius = (*m_imageB)[i]->radius()*(*m_imageB)[i]->K();
		if (lat-radius < minLatB) minLatB = lat-radius;
		if (lat+radius > maxLatB) maxLatB = lat+radius;
	}
	
	//==============================//
	// boucle sur les pixels orbitA //
	//==============================//
	if (m_progression_interval > 0. and m_verbose) std::cout << std::endl;
	double last_progression = -100.;

	#pragma omp parallel for schedule(dynamic)
	for (long int iOrbitA=0; iOrbitA < (long int) nOrbitA; iOrbitA++)
	{
		const Pixel *const pixelA = (*m_orbitA)[iOrbitA];
		
		m_colocationFlag[iOrbitA].reset();
		m_link[iOrbitA].clear();
		
		if (pixelA == NULL)
		{
			m_colocationFlag[iOrbitA].set(FLAG_NULL);
			continue;
		}
		
		// regarde si on doit traiter ce pixel
		if (!pixelA->proceed())
		{
			m_colocationFlag[iOrbitA].set(FLAG_NO_PROCEEDA);
			continue;
		}
		
		m_link[iOrbitA].clear();
		
		// Prints progression
		if (omp_get_thread_num() == 0 and m_progression_interval > 0.)
		{
			const double progression = 100.* (double)iOrbitA / (double)nOrbitA;
			if (progression - last_progression >= m_progression_interval)
			{
				std::cout << progression << std::endl;
				last_progression = progression;
			}
		}
		
		// test sur la latitude
		if (std::abs(pixelA->lat()) > 89.)
		{
			m_colocationFlag[iOrbitA].set(FLAG_LAT);
			continue;
		}
		
		// teste si on peut trouver des pixels
		if (pixelA->time()-timeThreshold() > maxTimeB or pixelA->time()+timeThreshold() < minTimeB)
		{
			m_colocationFlag[iOrbitA].set(FLAG_SELECT_TIME);
			continue;
		}
		
		if (pixelA->lat()-pixelA->radius()*pixelA->K() > maxLatB or pixelA->lat()+pixelA->radius()*pixelA->K() < minLatB)
		{
			m_colocationFlag[iOrbitA].set(FLAG_LAT);
			continue;
		}
	
		// teste la nuit
		if (m_config.test(COLOC_NO_NIGHT) and pixelA->night(m_nightThreshold, geo::DEGREE))
		{
			m_colocationFlag[iOrbitA].set(FLAG_NIGHT);
			if (!m_config.test(COLOC_FORCE)) continue;
		}
		
		//=======================================================//
		// sélection des pixelB susceptibles d'êtres colocalisés //
		//=======================================================//
		
		size_t col, row;
		
		try
		{
			m_imageB->geo2pix(pixelA->coord(), col, row);
		}
		catch(const std::exception& e)
		{
			m_colocationFlag[iOrbitA].set(FLAG_FPC);
			continue;
		}
		
		// Calule le nombre de pixelB qui entrent dans le pixelA
		const Pixel* pixelBcenter = NULL;
		const Pixel* pixelBright = NULL;
		const Pixel* pixelBleft = NULL;
		const Pixel* pixelBup = NULL;
		const Pixel* pixelBdown = NULL;
		
		try
		{
			pixelBcenter = (*m_imageB)(col, row);
			pixelBright = (*m_imageB)(col+1, row);
			pixelBleft = (*m_imageB)(col-1, row);
			pixelBup = (*m_imageB)(col, row-1);
			pixelBdown = (*m_imageB)(col, row+1);
		}
		catch (const std::exception& e)
		{
			m_colocationFlag[iOrbitA].set(FLAG_FPC);
			continue;
		}
		
		if (pixelBcenter == NULL or pixelBright == NULL or pixelBleft == NULL or pixelBup == NULL or pixelBdown == NULL)
		{
			m_colocationFlag[iOrbitA].set(FLAG_FPC);
			continue;
		}
		
		double hA, wA;
		
		if (!pixelA->distortable() or pixelA->cosalpha() == 1.)
		{
			hA = atan(2.*pixelA->radius()/geo::r_earth) * M_RADEG;
			wA = atan(2.*pixelA->radius()/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN));
		}
		else
		{
			try
			{
				// Définition du rectangle de recherche, en degrés
				hA = atan(2.*geo::radius_ellipse(pixelA->radius()*pixelA->K(), pixelA->radius()*pixelA->L(), pixelA->viewingAzimuthAngle())/geo::r_earth) * M_RADEG;
				wA  = atan(2.*geo::radius_ellipse(pixelA->radius()*pixelA->K(), pixelA->radius()*pixelA->L(), 90.+pixelA->viewingAzimuthAngle())/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN)); // La division par le cos(lat) vient du fait que les méridiens se rapprochent aux hautes latitudes
			}
			catch (const std::exception& e)
			{
				hA = atan(2.*pixelA->radius()*pixelA->K()/geo::r_earth) * M_RADEG;
				wA = atan(2.*pixelA->radius()*pixelA->K()/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN));
			}
		}
		
		double hB, wB;
		if (!pixelBcenter->distortable() or pixelBcenter->cosalpha() == 1.)
		{
			hB = atan(2.*pixelBcenter->radius()/geo::r_earth) * M_RADEG;
			wB = atan(2.*pixelBcenter->radius()/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN));
		}
		else
		{
			try
			{
				hB = atan(2.*geo::radius_ellipse(pixelBcenter->radius()*pixelBcenter->K(), pixelBcenter->radius()*pixelBcenter->L(), pixelBcenter->viewingAzimuthAngle())/geo::r_earth) * M_RADEG;
				wB = atan(2.*geo::radius_ellipse(pixelBcenter->radius()*pixelBcenter->K(), pixelBcenter->radius()*pixelBcenter->L(), 90.+pixelBcenter->viewingAzimuthAngle())/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN));
			}
			catch (const std::exception& e)
			{
				hB = atan(2.*pixelBcenter->radius()*pixelBcenter->K()/geo::r_earth) * M_RADEG;
				wB = atan(2.*pixelBcenter->radius()*pixelBcenter->K()/geo::r_earth) * M_RADEG / cos(pixelA->lat(geo::RADIAN));
			}
		}
		
		double dlon = std::abs(pixelBleft->lon() - pixelBright->lon());
		// Si les pixels sont de part et d'autre de la longitude 180.
		while(dlon > 360.) dlon -= 360.;
		const double dlat = std::abs(pixelBup->lat() - pixelBdown->lat());
		
		// On estime ici que tous les pixels dans le rectangle ont le même rayon
		const int nX = (int) ceil(std::abs( (wA+wB) / dlon )); // nX est 1/2 du nombre de pixelB qui entrent dans pixelA en largeur (car dlon couvre 2 pixels)
		const int nY = (int) ceil(std::abs( (hA+hB) / dlat )); // nY est 1/2 du nombre de pixelB qui entrent dans pixelA en hauteur (car dlat couvre 2 pixels)
		
		size_t nb_spatial=0, nb_temp=0, nb_angular=0, nb_proceed=0;
		
		// Définition du vecteur qui contiendra les indices des pixelB sélectionnés
		std::vector<const Pixel*> selectB;
		
		// Reserve some memory for the vector to optimize push_back
		selectB.reserve(4*nX*nY);
		
		//=======================//
		// boucle sur les pixelB //
		//=======================//
		for (long iY=(long)row-nY; iY <= (long)row+nY; iY++)
		{
			for (long iX=(long)col-nX; iX <= (long)col+nX; iX++)
			{
				if (iY < 0 or iY >= (long) m_imageB->nrows()) continue;
				
				const Pixel *const pixelB = (*m_imageB)(iX, iY);
				
				// regarde si on doit traiter ce pixel
				if (pixelB == NULL) continue;
				nb_spatial++;
				
				if (!pixelB->proceed()) continue;
				nb_proceed++;
				
				//===========================//
				// colocaliastion temporelle //
				//===========================//
				if (std::abs(pixelB->time()-pixelA->time()) > timeThreshold()) continue;
				nb_temp++;
				
				//==========================//
				// colocalisation angulaire //
				//==========================//
				try
				{
					if (!coloc_ang(pixelA, pixelB))
					{
						if (!m_config.test(COLOC_FORCE)) continue;
					} else {
						nb_angular++;
					}
				}
				catch (const std::exception& e)
				{
					continue;
				}
				
				selectB.push_back(pixelB);
			} // iX
		} // iY
		
		if (nb_spatial==0)
		{
			m_colocationFlag[iOrbitA].set(FLAG_SELECT_COORD);
			continue;
		}
		if (nb_proceed==0)
		{
			m_colocationFlag[iOrbitA].set(FLAG_NO_PROCEEDB);
			continue;
		}
		if (nb_temp==0)
		{
			m_colocationFlag[iOrbitA].set(FLAG_SELECT_TIME);
			continue;
		}
		if (nb_angular==0)
		{
			m_colocationFlag[iOrbitA].set(FLAG_SELECT_ANGLE);
		}
	
		const size_t nSelectB = selectB.size();
		
		if (nSelectB == 0) continue;
		
		if (m_integrationRule == INT_OFF)
		{
			m_colocationFlag[iOrbitA].set(FLAG_INT_OFF);
			continue;
		}
		
		//=============//
		// Intégration //
		//=============//
		try
		{
			integration(iOrbitA, wA, hA, selectB);
		}
		catch (const std::exception& e)
		{
			m_colocationFlag[iOrbitA].set(FLAG_NO_COLOC);
			continue;
		}
	} // iOrbitA // fin de la boucle parallèle
} // coloc

void Colocation::integration(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB)
{
	switch(m_integrationRule)
	{
		case INT_OFF:
		throw std::runtime_error("Integration is turned off.");
		break;
		
		case INT_NEAREST:
		nearest(iOrbitA, width, height, selectB);
		break;
		
		case INT_SLAVE:
		intSlave(iOrbitA, width, height, selectB);
		break;
		
		case INT_MONTECARLO:
		montecarlo(iOrbitA, width, height, selectB);
		break;
		
		case INT_TRAPEZOIDAL:
		trapezoidal(iOrbitA, width, height, selectB);
		break;
		
		case INT_SIMPSON:
		simpson(iOrbitA, width, height, selectB);
		break;
		
		default:
		montecarlo(iOrbitA, width, height, selectB);
	}
	
	// La suite n'est exécutée que si aucune excpetion n'a été jetée lors de l'intégration
	
	// Tri de la proportion la plus grande à la plus petite.
	std::sort(m_link[iOrbitA].begin(), m_link[iOrbitA].end(), std::greater<dPpair>());

	// Supprime les PixelB non colocalisés
	size_t newSize=0;
	for (size_t i=0; i < m_link[iOrbitA].size(); i++)
	{
		if (m_link[iOrbitA][i].first > 0.) newSize++;
	}
	m_link[iOrbitA].resize(newSize);
	
	if (newSize == 0) throw std::runtime_error("void Colocation::integration() : No pixel colocated");
	
	#pragma omp critical
	m_nColocatedPixels++;
}

void Colocation::nearest(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB)
{
	const Pixel *const pixelA = (*m_orbitA)[iOrbitA];
	const size_t nSelectB = selectB.size();
	
	double nearestDist = INFINITY;
	const Pixel* nearestPixel = NULL;
	for (size_t iSelectB=0; iSelectB < nSelectB; iSelectB++)
	{
		const Pixel *const pixelB = selectB[iSelectB];
		
		double dist = geo::map_2points(pixelA->coord(), pixelB->coord());
		
		if (dist < nearestDist)
		{
			nearestDist = dist;
			nearestPixel = pixelB;
		}
	}
	
	if (nearestPixel == NULL) throw std::runtime_error("void Colocation::nearest() : No pixel colocated");
	
	const double psfA = pixelA->psf(nearestPixel->coord()) * pixelA->weight();
	const double psfB = nearestPixel->psf(pixelA->coord()) * nearestPixel->weight();
	const double psfAB = (psfA+psfB)/2.;
	
	// Ecrit dans le fichier
	if (m_fileIPout.is_open())
	{
		#pragma omp critical
		m_fileIPout << iOrbitA << '\t' << m_link[iOrbitA][0].second->lon() << '\t' << m_link[iOrbitA][0].second->lat() << '\t' << m_link[iOrbitA][0].first << std::endl;
	}
	
	if (psfAB == 0.)
	{
		m_link[iOrbitA].clear();
		throw std::runtime_error("void Colocation::nearest() : No pixel colocated");
	} else {
		m_link[iOrbitA].resize(1);
		m_link[iOrbitA][0] = dPpair(psfAB, nearestPixel);
		m_meanPSF[iOrbitA] = psfAB;
	}
}

void Colocation::montecarlo(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB)
{
	const Pixel *const pixelA = (*m_orbitA)[iOrbitA];
	const size_t nrand = m_randomPool.size();
	const size_t nSelectB = selectB.size();
	
	// Calcul du nombre de points d'intégration
	const size_t nIP = std::max(std::min(nSelectB * m_IPpB, m_IPmax), m_IPmin);
	m_nIP[iOrbitA] = nIP;
	
	// Reserve de la mémoire
	m_link[iOrbitA].resize(nSelectB, dPpair(0., NULL));
	
	// Initialisation des sommes
	double sum_psfA = 0.;
	double sum_psfAB = 0.;
	
	for (size_t iIP=0; iIP<nIP; iIP++)
	{
		// Génération d'un couple lon/lat aléatoire dans un rectangle autour de pixelA
		const double randLon = m_randomPool[m_irand++ % nrand] - .5;
		const double randLat = m_randomPool[m_irand++ % nrand] - .5;
		const Coordinate randCoord = pixelA->coord() + Coordinate(width * randLon, height * randLat);
		
		// Calcul de la PSF du pixelA aux coordonnées lon/lat
		double psfA = 0.;
		
		try
		{
			psfA = pixelA->psf(randCoord) * pixelA->weight();
		}
		catch (const std::exception& e)
		{
			m_colocationFlag[iOrbitA].set(FLAG_AZIMUTH);
			continue;
		}
		
		// Si on est en dehors du pixelA, on choisit un autre couple de lon/lat
		if (psfA == 0.) continue;
		
		double shot_sum_psfAB = 0.; // somme intermédiaire
		
		//=====================================//
		// Boucle sur les pixelsB sélectionnés //
		//=====================================//
		for (size_t iSelectB=0; iSelectB < nSelectB; iSelectB++)
		{
			const Pixel *const pixelB = selectB[iSelectB];
			
			if (m_link[iOrbitA][iSelectB].second == NULL) m_link[iOrbitA][iSelectB].second = pixelB;
			
			// Calcul de la PSF du pixelB aux coordonnées lon/lat
			double psfB = 0.;
			
			try
			{
				psfB = pixelB->psf(randCoord) * pixelB->weight();
			}
			catch (const std::exception& e)
			{
				m_colocationFlag[iOrbitA].set(FLAG_AZIMUTH);
				continue;
			}
			
			if (psfB == 0.) continue;
			
			// Calcul du produit de la PSF du pixelA et de la PSF du pixelB aux coordonnées lon/lat
			const double psfAB = psfA*psfB;
			
			if (psfAB == 0.) continue;
			
			m_link[iOrbitA][iSelectB].first += psfAB;
			
			shot_sum_psfAB += psfAB;
		} // iSelectB
		
		// Ecrit dans le fichier
		if (m_fileIPout.is_open())
		{
			#pragma omp critical
			m_fileIPout << iOrbitA << '\t' << randCoord.lon() << '\t' << randCoord.lat() << '\t' << shot_sum_psfAB << std::endl;
		}
		
		sum_psfAB += shot_sum_psfAB;
		sum_psfA += psfA;
	} // iIP
	
	// Y a t'il des pixels colocalisés ?
	if (sum_psfAB == 0.)
	{
		m_link[iOrbitA].clear();
		throw std::runtime_error("void Colocation::montecarlo() : No pixel colocated");
	} else {
		m_meanPSF[iOrbitA] = sum_psfAB/sum_psfA;
	}
} // montecarlo

void Colocation::trapezoidal(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB)
{
	const Pixel *const pixelA = (*m_orbitA)[iOrbitA];
	const size_t nSelectB = selectB.size();
	
	// Calcul du nombre de points d'intégration
	const int resolution = (int) ceil(sqrt(std::max(std::min(nSelectB * m_IPpB, m_IPmax), m_IPmin)));
	m_nIP[iOrbitA] = resolution * resolution;
	
	// Reserve de la mémoire
	m_link[iOrbitA].resize(nSelectB, dPpair(0., NULL));
	
	// Initialisation des sommes
	double sum_psfA = 0.;
	double sum_psfAB = 0.;
	
	// Calcul des PSF
	for (int iY=0; iY < resolution; iY++)
	{
		double normY = 0.;
		if (resolution > 1)
		{
			normY = (double) iY / ((double) resolution - 1.) - .5;
		}
		
		for (int iX=0; iX < resolution; iX++)
		{
			double normX = 0.;
			if (resolution > 1)
			{
				normX = (double) iX / ((double) resolution - 1.) - .5;
			}
			
			const Coordinate coord = pixelA->coord() + Coordinate(width * normX, height * normY);
			
			// Calcul de la PSF du pixelA aux coordonnées lon/lat
			double psfA = 0.;
			
			try
			{
				psfA = pixelA->psf(coord) * pixelA->weight();
			}
			catch (const std::exception& e)
			{
				m_colocationFlag[iOrbitA].set(FLAG_AZIMUTH);
				continue;
			}
			
			// Si on est en dehors du pixelA, on choisit un autre couple de lon/lat
			if (psfA == 0.) continue;
			
			double shot_sum_psfAB = 0.; // somme intermédiaire
			
			// Poids du point d'intégration
			double intWeight;
			if (resolution < 1)
			{
				intWeight = 1.;
			}
			// dans les 4 coins
			else if ((iX == 0 or iX == resolution-1) and (iY == 0 or iY == resolution-1))
			{
				intWeight = 1./4.;
			}
			// sur les 4 côtés
			else if (iX == 0 or iX == resolution-1 or iY == 0 or iY == resolution-1)
			{
				intWeight = 2./4.;
			}
			// à l'Intérieur
			else
			{
				intWeight = 4./4.;
			}
			
			psfA *= intWeight;
			
			//=====================================//
			// Boucle sur les pixelsB sélectionnés //
			//=====================================//
			for (size_t iSelectB=0; iSelectB < nSelectB; iSelectB++)
			{
				const Pixel *const pixelB = selectB[iSelectB];
				
				if (m_link[iOrbitA][iSelectB].second == NULL) m_link[iOrbitA][iSelectB].second = pixelB;
				
				// Calcul de la PSF du pixelB aux coordonnées lon/lat
				double psfB = 0.;
				try
				{
					psfB = pixelB->psf(coord) * pixelB->weight();
				}
				catch (const std::exception& e)
				{
					m_colocationFlag[iOrbitA].set(FLAG_AZIMUTH);
					continue;
				}
				
				if (psfB == 0.) continue;
				
				// Calcul du produit de la PSF du pixelA et de la PSF du pixelB aux coordonnées lon/lat
				const double psfAB = psfA*psfB;
				
				if (psfAB == 0.) continue;
				
				m_link[iOrbitA][iSelectB].first += psfAB;
				
				shot_sum_psfAB += psfAB;
			} // iSelectB
			
			// Ecrit dans le fichier
			if (m_fileIPout.is_open())
			{
				#pragma omp critical
				m_fileIPout << iOrbitA << '\t' << coord.lon() << '\t' << coord.lat() << '\t' << shot_sum_psfAB << std::endl;
			}
			
			sum_psfAB += shot_sum_psfAB;
			sum_psfA += psfA;
		} // ix
	} // iy
	
	// Y a t'il des pixels colocalisés ?
	if (sum_psfAB == 0.)
	{
		m_link[iOrbitA].clear();
		throw std::runtime_error("void Colocation::trapezoidal() : No pixel colocated");
	} else {
		m_meanPSF[iOrbitA] = sum_psfAB/sum_psfA;
	}
} // trapezoid

void Colocation::simpson(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB)
{
	const Pixel *const pixelA = (*m_orbitA)[iOrbitA];
	const size_t nSelectB = selectB.size();
	
	// Calcul du nombre de points d'intégration
	const int resolution = (int) ceil(sqrt(std::max(std::min(nSelectB * m_IPpB, m_IPmax), m_IPmin))/2.)*2-1;
	m_nIP[iOrbitA] = resolution * resolution;
	
	// Reserve de la mémoire
	m_link[iOrbitA].resize(nSelectB, dPpair(0., NULL));
	
	// Initialisation des sommes
	double sum_psfA = 0.;
	double sum_psfAB = 0.;
	
	// Calcul des PSF
	for (int iY=0; iY < resolution; iY++)
	{
		double normY = 0.;
		if (resolution > 1)
		{
			normY = (double) iY / ((double) resolution - 1.) - .5;
		}
		
		for (int iX=0; iX < resolution; iX++)
		{
			double normX = 0.;
			if (resolution > 1)
			{
				normX = (double) iX / ((double) resolution - 1.) - .5;
			}
			
			const Coordinate coord = pixelA->coord() + Coordinate(width * normX, height * normY);
			
			// Calcul de la PSF du pixelA aux coordonnées lon/lat
			double psfA = 0.;
			
			try
			{
				psfA = pixelA->psf(coord) * pixelA->weight();
			}
			catch (const std::exception& e)
			{
				m_colocationFlag[iOrbitA].set(FLAG_AZIMUTH);
				continue;
			}
			
			// Si on est en dehors du pixelA, on choisit un autre couple de lon/lat
			if (psfA == 0.) continue;
			
			double shot_sum_psfAB = 0.; // somme intermédiaire
			
			// Poids du point d'intégration
			double intWeight;
			if (resolution < 1)
			{
				intWeight = 1.;
			}
			// dans les 4 coins
			else if ((iX == 0 or iX == resolution-1) and (iY == 0 or iY == resolution-1))
			{
				intWeight = 1./9.;
			}
			// sur les 4 côtés
			else if (iX == 0 or iX == resolution-1 or iY == 0 or iY == resolution-1)
			{
				intWeight = 2./9.;
				
				if (iX % 2 == 1 or iY % 2 == 1) intWeight *= 2;
			}
			// à l'Intérieur
			else
			{
				intWeight = 4./9.;
				
				if (iX % 2 == 1 or iY % 2 == 1) intWeight *= 2;
				
				if (iX % 2 == 1 and iY % 2 == 1) intWeight *= 2;
			}
			
			psfA *= intWeight;
			
			//=====================================//
			// Boucle sur les pixelsB sélectionnés //
			//=====================================//
			for (size_t iSelectB=0; iSelectB < nSelectB; iSelectB++)
			{
				const Pixel *const pixelB = selectB[iSelectB];
				
				if (m_link[iOrbitA][iSelectB].second == NULL) m_link[iOrbitA][iSelectB].second = pixelB;
				
				// Calcul de la PSF du pixelB aux coordonnées lon/lat
				double psfB = 0.;
				try
				{
					psfB = pixelB->psf(coord) * pixelB->weight();
				}
				catch (const std::exception& e)
				{
					m_colocationFlag[iOrbitA].set(FLAG_AZIMUTH);
					continue;
				}
				
				if (psfB == 0.) continue;
				
				// Calcul du produit de la PSF du pixelA et de la PSF du pixelB aux coordonnées lon/lat
				const double psfAB = psfA*psfB;
				
				if (psfAB == 0.) continue;
				
				m_link[iOrbitA][iSelectB].first += psfAB;
				
				shot_sum_psfAB += psfAB;
			} // iSelectB
			
			// Ecrit dans le fichier
			if (m_fileIPout.is_open())
			{
				#pragma omp critical
				m_fileIPout << iOrbitA << '\t' << coord.lon() << '\t' << coord.lat() << '\t' << shot_sum_psfAB << std::endl;
			}
			
			sum_psfAB += shot_sum_psfAB;
			sum_psfA += psfA;
		} // ix
	} // iy
	
	// Y a t'il des pixels colocalisés ?
	if (sum_psfAB == 0.)
	{
		m_link[iOrbitA].clear();
		throw std::runtime_error("void Colocation::simpson() : No pixel colocated");
	} else {
		m_meanPSF[iOrbitA] = sum_psfAB/sum_psfA;
	}
} // simpson

void Colocation::intSlave(const size_t iOrbitA, const double width, const double height, const std::vector<const Pixel*>& selectB)
{
	const Pixel *const pixelA = (*m_orbitA)[iOrbitA];
	const size_t nSelectB = selectB.size();
	
	const double surfA = pixelA->surface();
	
	// Calcul du nombre de points d'intégration
	m_nIP[iOrbitA] = nSelectB;
	
	// Reserve de la mémoire
	m_link[iOrbitA].resize(nSelectB, dPpair(0., NULL));
	
	// Initialisation des sommes
	double sum_psfA = 0.;
	double sum_psfAB = 0.;
	double sum_psfB = 0.;
	
	//=====================================//
	// Boucle sur les pixelsB sélectionnés //
	//=====================================//
	for (size_t iSelectB=0; iSelectB < nSelectB; iSelectB++)
	{
		const Pixel *const pixelB = selectB[iSelectB];
		const Coordinate& coord = pixelB->coord();
		
		// Calcul de la PSF du pixelA aux coordonnées lon/lat
		double psfA = 0.;
		try
		{
			psfA = pixelA->psf(coord) * pixelA->weight();
		}
		catch (const std::exception& e)
		{
			m_colocationFlag[iOrbitA].set(FLAG_AZIMUTH);
			continue;
		}
		
		// Si on est en dehors du pixelA, on choisit un autre couple de lon/lat
		if (psfA == 0.) continue;
		
		if (m_link[iOrbitA][iSelectB].second == NULL) m_link[iOrbitA][iSelectB].second = pixelB;
		
		// Calcul de la PSF du pixelB aux coordonnées lon/lat
		double psfB = 0.;
		try
		{
			psfB = pixelB->psf(coord) * pixelB->weight();
		}
		catch (const std::exception& e)
		{
			m_colocationFlag[iOrbitA].set(FLAG_AZIMUTH);
			continue;
		}
		
		if (psfB == 0.) continue;
		
		sum_psfB += psfB;
		
		// Pondération par la surface du pixel.
		const double surfRatio = pixelB->surface()/surfA;
		
		// Calcul du produit de la PSF du pixelA et de la PSF du pixelB aux coordonnées lon/lat
		const double psfAB = psfA*psfB*surfRatio;
		
		if (psfAB == 0.) continue;
		
		m_link[iOrbitA][iSelectB].first = psfAB;
		
		// Ecrit dans le fichier
		if (m_fileIPout.is_open())
		{
			#pragma omp critical
			m_fileIPout << iOrbitA << '\t' << coord.lon() << '\t' << coord.lat() << '\t' << psfAB << std::endl;
		}
		
		sum_psfAB += psfAB;
		sum_psfA += psfA;
	} // iSelectB
	
	// Y a t'il des pixels colocalisés ?
	if (sum_psfAB == 0.)
	{
		m_link[iOrbitA].clear();
		throw std::runtime_error("void Colocation::intSlave() : No pixel colocated");
	} else {
		m_meanPSF[iOrbitA] = sum_psfAB/(sum_psfA/sum_psfB);
	}
} // intSlave

std::ofstream& Colocation::openIPout(const std::string& fileIPout)
{
	m_fileIPout.open(fileIPout.c_str());
	return m_fileIPout;
}

std::ofstream& Colocation::closeIPout()
{
	if (m_fileIPout.is_open()) m_fileIPout.close();
	return m_fileIPout;
}


void Colocation::configure(const ConfigurationFlagIndex index, const bool state)
{
	if (state) m_config.set(index);
	else m_config.reset(index);
}

const Colocation::ConfigurationFlag& Colocation::config() const
{
	return m_config;
}

unsigned long Colocation::configUlong() const
{
	return config().to_ulong();
}

const Colocation::IntegrationRule& Colocation::integrationRule() const
{
	return m_integrationRule;
}

void Colocation::setConfig(const Colocation::ConfigurationFlag& config)
{
	m_config = config;
}

void Colocation::setIntegrationPointsPerB(const size_t n)
{
	m_IPpB = n;
}
void Colocation::setIntegrationPointsMin(const size_t n)
{
	m_IPmin = n;
	if (m_IPmax < m_IPmin) m_IPmax = m_IPmin;
}
void Colocation::setIntegrationPointsMax(const size_t n)
{
	m_IPmax = n;
	if (m_IPmax < m_IPmin) m_IPmin = m_IPmax;
}
void Colocation::setRootFindingMax(const unsigned int rootFindingMax)
{
	m_rootFindingMax = rootFindingMax;
}

void Colocation::setIntegrationRule(const IntegrationRule rule)
{
	m_integrationRule = rule;
}

void Colocation::setTimeThreshold(const double deltaTime)
{
	m_timeThreshold = deltaTime;
}
void Colocation::setZenithThreshold(double zen, const geo::unit_angle u)
{
	if (u == geo::RADIAN) zen /= M_RADEG;
	m_zenithThreshold = zen;
}
void Colocation::setAzimuthThreshold(double azi, const geo::unit_angle u)
{
	if (u == geo::RADIAN) azi /= M_RADEG;
	m_azimuthThreshold = azi;
}
void Colocation::setConeThreshold(double cone, const geo::unit_angle u)
{
	if (u == geo::RADIAN) cone /= M_RADEG;
	m_coneThreshold = cone;
}
void Colocation::setNightThreshold(double sza, const geo::unit_angle u)
{
	if (u == geo::RADIAN) sza /= M_RADEG;
	m_nightThreshold = sza;
}

void Colocation::setVerbose(const bool b)
{
	m_verbose = b;
}

void Colocation::setProgressionInterval(const double p)
{
	m_progression_interval = std::max(p, 0.);
}

double Colocation::duration() const
{
	return difftime(m_endTime, m_startTime);
}

std::string Colocation::version()
{
	return VERSION;
}

bool Colocation::coloc_ang(const Pixel *const pixelA, const Pixel *const pixelB) const
/******************************************************************************\
* AUTH: Nicolas Gif <nicolas.gif@lmd.polytechnique.fr>                         *
* DATE: mar. 08 juin 2010 11:00:07 CEST                                        *
* DESC: Code de colocalisation angulaire                                       *
* TYPE: Fonction                                                               *
* INPT: Orbit&, Orbit&, bitset<8>                                              *
* RTRN: bool                                                                   *
\******************************************************************************/
{
	double delta;
	bool coloc = true;

	// Test avec la pyramide
	if (!m_config.test(COLOC_CONE))
	{
		// Test sur le zenith angle
		if (m_config.test(COLOC_ZEN))
		{
			delta = geo::to_180_180(pixelA->viewingZenithAngle(geo::DEGREE) - pixelB->viewingZenithAngle(geo::DEGREE)); // en DEGRES
			coloc = std::abs(delta) <= m_zenithThreshold;
		}
	
		// Test sur l'azimuth angle
		if (coloc and m_config.test(COLOC_AZI))
		{
			// Suceptible de retourner une exception
			delta = geo::to_180_180(pixelA->viewingAzimuthAngle(geo::DEGREE) - pixelB->viewingAzimuthAngle(geo::DEGREE)); // en DEGRES
			coloc = std::abs(delta) <= m_azimuthThreshold;
		}
	} else { // Test avec le cone
		coloc = pixelA->scatterAngle(pixelB, geo::DEGREE) <= m_coneThreshold; // Dans Ixion, la comparaison est faite avec delta/2 (demi-ouverture du cone).
	}

	return coloc;

} // coloc_ang
