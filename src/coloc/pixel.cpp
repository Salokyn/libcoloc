#include <coloc/pixel.hpp>
#include <stdexcept>
#include <geo/geo.hpp>
#include <cmath>
#include <algorithm>
#include <iostream>
#include <stdexcept>

/******************************************************************************\
* NAME: Pixel                                                                  *
* AUTH: Nicolas Gif <nicolas.gif@lmd.polytechnique.fr>                         *
* DATE: mar. 08 juin 2010 11:00:07 CEST                                        *
* TYPE: Classe                                                                 *
* DESC: Classe Abstraite                                                       *
\******************************************************************************/

// Constructeurs
Pixel::Pixel():
m_time(0.),
m_coord(Coordinate(0.,0.)),
m_satCoord(Coordinate(0.,0.)),
m_cosalpha(1.),
m_azimuthAngle(NAN),
m_proceed(true),
m_weight(1.),
m_distortable(true),
m_symmetric(false)
{}

Pixel::Pixel(const Coordinate& coord):
m_time(0.),
m_coord(coord),
m_satCoord(coord),
m_azimuthAngle(NAN),
m_proceed(true),
m_weight(1.),
m_distortable(true),
m_symmetric(false)
{
	updateCosalpha();
}

Pixel::Pixel(const Coordinate& coord, const Coordinate& satCoord):
m_time(0.),
m_coord(coord),
m_satCoord(satCoord),
m_azimuthAngle(NAN),
m_proceed(true),
m_weight(1.),
m_distortable(true),
m_symmetric(false)
{
	updateCosalpha();
}

Pixel::Pixel(const double time, const Coordinate& coord):
m_time(time),
m_coord(coord),
m_satCoord(coord),
m_azimuthAngle(NAN),
m_proceed(true),
m_weight(1.),
m_distortable(true),
m_symmetric(false)
{
	updateCosalpha();
}

Pixel::Pixel(const double time, const Coordinate& coord, const Coordinate& satCoord):
m_time(time),
m_coord(coord),
m_satCoord(satCoord),
m_azimuthAngle(NAN),
m_proceed(true),
m_weight(1.),
m_distortable(true),
m_symmetric(false)
{
	updateCosalpha();
}

// Destructeur
Pixel::~Pixel() {}

// Getteurs
const std::vector<double>& Pixel::value() const { return m_value; }

const double& Pixel::value(size_t i) const
{
	if (i >= m_value.size()) throw std::out_of_range("double Pixel::value(size_t i) const : Given index is greater than m_value size !");
	return m_value[i];
}

double Pixel::lon(const geo::unit_angle u) const { return m_coord.lon(u); }
double Pixel::lat(const geo::unit_angle u) const { return m_coord.lat(u); }
const Coordinate& Pixel::coord() const { return m_coord; }

const double& Pixel::time() const { return m_time; }

const double& Pixel::relativeAltitude() const { return m_relativeAltitude; }
double Pixel::satLon(const geo::unit_angle u) const { return m_satCoord.lon(u); }
double Pixel::satLat(const geo::unit_angle u) const { return m_satCoord.lat(u); }
const Coordinate& Pixel::satCoord() const { return m_satCoord; }
const double& Pixel::cosalpha() const { return m_cosalpha; }
const bool& Pixel::distortable() const { return m_distortable; }
const bool& Pixel::symmetric() const { return m_symmetric; }

double Pixel::azimuthAngle(const geo::unit_angle u) const
{
	if (std::isnan(m_azimuthAngle))
	{
		if (symmetric()) throw std::logic_error("double Pixel::azimuthAngle(const geo::unit_angle u) const : Azimuth has not yet been calculated since the Pixel is symmetric. Consider using updateAzimuthAngle().");
		throw std::logic_error("double Pixel::azimuthAngle(const geo::unit_angle u) const : Azimuth is undefined !");
	}
	if (u == geo::RADIAN) return m_azimuthAngle / M_RADEG;
	return m_azimuthAngle;
}
double Pixel::altitude(const double r) const
{
	return (relativeAltitude() - 1.) * r;
}
const Pixel::ScanMode& Pixel::scanMode() const
{
	return m_scanMode;
}
const bool& Pixel::proceed() const
{
	return m_proceed;
}
const double& Pixel::weight() const
{
	return m_weight;
}
void Pixel::resizeValue(const size_t i)
{
	m_value.resize(i);
}
void Pixel::clearValue()
{
	m_value.clear();
}

// Setteurs
void Pixel::setValue(const double value, const size_t i)
{
	if (i >= m_value.size()) m_value.resize(i+1, NAN);
	m_value[i] = value;
}

void Pixel::setTime(const double time) { m_time = time; }
void Pixel::setTime(const Date& date) { m_time = date.julday(); }
void Pixel::setRelativeAltitude(const double relativeAltitude)
{
	m_relativeAltitude = relativeAltitude;
	updateCosalpha();
	try
	{
		updateDistortion();
	}
	catch(const std::exception& e)
	{
		if (distortable()) throw;
	}
}

void Pixel::setCoord(const Coordinate& coord)
{
	m_coord = coord;
	updateCosalpha();
	updateViewingAzimuthAngle();
	if (!symmetric()) updateAzimuthAngle();
	try
	{
		updateDistortion();
	}
	catch(const std::exception& e)
	{
		if (distortable()) throw;
	}
}

void Pixel::setCoord(const Coordinate& coord, const Coordinate& satCoord)
{
	m_coord = coord;
	m_satCoord = satCoord;
	updateCosalpha();
	updateViewingAzimuthAngle();
	if (!symmetric()) updateAzimuthAngle();
	try
	{
		updateDistortion();
	}
	catch(const std::exception& e)
	{
		if (distortable()) throw;
	}
}

void Pixel::setSatCoord(const Coordinate& coord)
{
	m_satCoord = coord;
	updateCosalpha();
	updateViewingAzimuthAngle();
	if (!symmetric()) updateAzimuthAngle();
	try
	{
		updateDistortion();
	}
	catch(const std::exception& e)
	{
		if (distortable()) throw;
	}
}
void Pixel::setAzimuthAngle(double azimuthAngle, const geo::unit_angle u)
{
	if (u == geo::RADIAN) azimuthAngle *= M_RADEG; // conversion en DEGREE
	m_azimuthAngle = geo::to_180_180(azimuthAngle);
}
void Pixel::setAltitude(const double altitude, const double r)
{
	setRelativeAltitude((altitude+r)/r);
}
void Pixel::setDistortable(const bool b)
{
	m_distortable = b;
}
void Pixel::setScanMode(const ScanMode& m)
{
	m_scanMode = m;
	
	if (!symmetric()) updateAzimuthAngle();
}
void Pixel::setProceed(const bool proceed)
{
	m_proceed = proceed;
}
void Pixel::setWeight(const double weight)
{
	m_weight = weight;
}

// Méthodes
double Pixel::K() const
{
	if (!distortable()) return 1.;
	return m_K;
}
double Pixel::L() const
{
	if (!distortable()) return 1.;
	return m_L;
}
void Pixel::updateDistortion()
{
	m_K = geo::distortionK(cosalpha(), relativeAltitude());
	m_L = geo::distortionL(cosalpha(), relativeAltitude());
}
double Pixel::viewingAzimuthAngle(const geo::unit_angle u) const
{
	if (u == geo::RADIAN) return m_viewingAzimuthAngle / M_RADEG;
	else return m_viewingAzimuthAngle;
}
double Pixel::updateViewingAzimuthAngle(const geo::unit_angle u)
{
	if (cosalpha() == 1.)
	{
		m_viewingAzimuthAngle = NAN;
	}
	else
	{
		try
		{
			geo::map_2points(coord(), satCoord(), &m_viewingAzimuthAngle, geo::DEGREE);
		}
		catch (const std::exception& e)
		{
			return m_viewingAzimuthAngle = NAN;
		}
	}
	
	return viewingAzimuthAngle(u);
}
const double& Pixel::updateCosalpha()
{
	return m_cosalpha = geo::cosalpha(coord(), satCoord());
}
double Pixel::updateAzimuthAngle(const geo::unit_angle u)
{
	switch (m_scanMode)
	{
	case XT:
		setAzimuthAngle(viewingAzimuthAngle() - 90.);
		break;
	case CONICAL:
		setAzimuthAngle(viewingAzimuthAngle());
		break;
	default:
		setAzimuthAngle(0.);
	}
	
	return m_azimuthAngle;
}
double Pixel::viewingZenithAngle(const geo::unit_angle u) const
{
	if (cosalpha() == 1.) return 0.;
	return geo::zenangle(cosalpha(), relativeAltitude(), u);
}
double Pixel::swathAngle(const geo::unit_angle u) const
{
	if (cosalpha() == 1.) return 0.;
	return geo::swath(cosalpha(), relativeAltitude(), u);
}
double Pixel::satDistance(const double r) const
{
	const double& eta = relativeAltitude();
	return r*sqrt(eta*eta-2.*eta*cosalpha()+1);
}
double Pixel::solarZenithAngle(const geo::unit_angle u, double* azimuthAngle) const
{
	return geo::sol_zenangle(time(), coord(), azimuthAngle, u);
}

double Pixel::solarAzimuthAngle(const geo::unit_angle u, double* zenithAngle) const
{
	double azi;
	double zen = geo::sol_zenangle(time(), coord(), &azi, u);
	
	if (sin(zen) == 0.) throw std::logic_error("double Pixel::solarAzimuthAngle(const geo::unit_angle u, double* zenithAngle) const : Azimuth is undefined !");
	
	if (zenithAngle != NULL) *zenithAngle = zen;
	
	return azi;
}

double Pixel::relativeAzimuthAngle(const geo::unit_angle u) const
{
	double raa = viewingAzimuthAngle(geo::RADIAN) - solarAzimuthAngle(geo::RADIAN) - M_PI;
	raa -= 2.*M_PI*round(raa/(2.*M_PI));
	if (u == geo::DEGREE) raa *= M_RADEG;
	
	return raa;
}

double Pixel::scatterAngle(const Pixel *const pixel, const geo::unit_angle u) const
{
	if (pixel == NULL) throw std::runtime_error("double Pixel::scatterAngle(const Pixel *const pixel, const geo::unit_angle u) const : Given Pixel is NULL");
	
	const double zenithA  = viewingZenithAngle(geo::RADIAN);
	const double zenithB  = pixel->viewingZenithAngle(geo::RADIAN);
	
	double azimuthA = 0., azimuthB = 0.;
	if (sin(zenithA)*sin(zenithB) != 0.)
	{
		azimuthA = viewingAzimuthAngle(geo::RADIAN);
		azimuthB = pixel->viewingAzimuthAngle(geo::RADIAN);
	}

	double cosinus = cos(zenithA)*cos(zenithB) + sin(zenithA)*sin(zenithB)*cos(azimuthA - azimuthB);
	if (std::abs(cosinus) > 1.) cosinus = copysign(1., cosinus); // Il arrive que le cos sorte de [-1;1]
	
	double result = acos( cosinus );
	if (u == geo::DEGREE) result *= M_RADEG;
	
	return result;
}

bool Pixel::night(const double threshold, const geo::unit_angle u) const
{
	return solarZenithAngle(u) >= threshold;
}

bool Pixel::sunglint(double threshold, const geo::unit_angle u) const
{
	// conversion du seuil en radians
	if (u == geo::DEGREE) threshold /= M_RADEG;

	// calcul de l'angle zenithal et de l'azimut du soleil à l'endroit du pixel CERES
	double sol_azi = 0., sol_zen;
	
	try
	{
		sol_azi = solarAzimuthAngle(geo::RADIAN, &sol_zen);
	}
	catch(const std::exception& e)
	{
		sol_zen = solarZenithAngle(geo::RADIAN);
	}
	
	const double zen = viewingZenithAngle(geo::RADIAN);
	
	double azi = 0.;
	if (sin(zen)*sin(sol_zen) != 0.)
	{
		azi = viewingAzimuthAngle(geo::RADIAN);
	}
	
	double cosinus = cos(zen)*cos(sol_zen) + sin(zen)*sin(sol_zen)*cos(azi - sol_azi + M_PI); // +pi car le satellite doit etre en face du soleil par rapport au pixel
	if (std::abs(cosinus) > 1.) cosinus = copysign(1., cosinus); // Il arrive que le cos sorte de [-1;1]
	const double delta = acos( cosinus );

	return delta <= threshold;
}

bool Pixel::operator>(const Pixel& pixel) const
{
	if (time() > pixel.time())
		return true;
	else if (time() == pixel.time() and lon() > pixel.lon())
		return true;
	else if (lon() == pixel.lon() and lat() > pixel.lat())
		return true;
	
	return false;
}

bool Pixel::operator<(const Pixel& pixel) const
{
	if (time() < pixel.time())
		return true;
	else if (time() == pixel.time() and lon() < pixel.lon())
		return true;
	else if (lon() == pixel.lon() and lat() < pixel.lat())
		return true;
	
	return false;
}

std::complex<double>& Pixel::rotate_and_distort(std::complex<double>& z) const
{
	if (std::abs(z) == 0.) return z;
	
	const std::complex<double> zi(0.,1.);
	double angle;
	
	if (distortable() and cosalpha() != 1.)
	{
		// rotation du point dans le repère KxL
		angle = -viewingAzimuthAngle(geo::RADIAN); // angle entre le repère du pixel et le repère KxL
		z *= std::exp(zi*angle);
	
		// application des facteurs de déformation
		z = std::complex<double>(std::real(z) / K(), std::imag(z) / L());
	
		// rotation du point du repère KxL vers le repère du axc
		angle = viewingAzimuthAngle(geo::RADIAN);
		if (!symmetric())
		{
			angle += -azimuthAngle(geo::RADIAN) + M_PI/2.; // angle entre les repères KxL et axc
		}
		z *= std::exp(zi*angle);
	}
	else if(!symmetric())
	{
		angle = -azimuthAngle(geo::RADIAN) + M_PI/2.; // le +pi/2 permet de déplacer l'origine qui est le nord vers l'origine "trigo"
		z *= std::exp(zi*angle);
	}
	
	return z;
}


// Constructeurs
PixelBox::PixelBox(const double size):
Pixel(),
m_size(size)
{
	m_relativeAltitude = INFINITY;
	m_scanMode = default_scanMode;
	m_symmetric = default_symmetric;
	m_K = m_L = 1.;
	setAzimuthAngle(0.);
	setDistortable(false);
}

PixelBox::PixelBox(const double size, const Coordinate& coord):
Pixel(coord),
m_size(size)
{
	m_relativeAltitude = INFINITY;
	m_scanMode = default_scanMode;
	m_symmetric = default_symmetric;
	m_K = m_L = 1.;
	setAzimuthAngle(0.);
	setDistortable(false);
}

PixelBox::PixelBox(const double size, const Coordinate& coord, const Coordinate& satCoord):
Pixel(coord, satCoord),
m_size(size)
{
	m_relativeAltitude = INFINITY;
	m_scanMode = default_scanMode;
	m_symmetric = default_symmetric;
	m_K = m_L = 1.;
	setAzimuthAngle(0.);
	setDistortable(false);
}

PixelBox::PixelBox(const double size, const double time, const Coordinate& coord):
Pixel(time, coord),
m_size(size)
{
	m_relativeAltitude = INFINITY;
	m_scanMode = default_scanMode;
	m_symmetric = default_symmetric;
	m_K = m_L = 1.;
	setAzimuthAngle(0.);
	setDistortable(false);
}

PixelBox::PixelBox(const double size, const double time, const Coordinate& coord, const Coordinate& satCoord):
Pixel(time, coord, satCoord),
m_size(size)
{
	m_relativeAltitude = INFINITY;
	m_scanMode = default_scanMode;
	m_symmetric = default_symmetric;
	m_K = m_L = 1.;
	setAzimuthAngle(0.);
	setDistortable(false);
}

PixelBox* PixelBox::clone() const
{
	return new PixelBox(*this);
}

// Méthodes
double PixelBox::psf(const Coordinate& coord) const
{
	if (this->coord() == coord) return 1.;
	
	double deltalon = geo::to_180_180(coord.lon()-this->lon());
	
	if ( std::abs(deltalon) > m_size/2. or std::abs(coord.lat()-lat()) > m_size/2. ) return 0.;
	return 1.;
}

double PixelBox::radius() const
{
	return m_size/2.*geo::r_earth/M_RADEG*sqrt(2.); // rayon en m au nadir
}

double PixelBox::surface() const
{
	double height = geo::r_earth/M_RADEG*m_size;
	double width = height/cos(lat(geo::RADIAN));
	return height*width;
}

// Constructeurs
PixelCircle::PixelCircle(const double radius, const double relativeAltitude):
Pixel(),
m_radius(radius)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = relativeAltitude;
	m_symmetric = true;
}

PixelCircle::PixelCircle(const double radius, const double relativeAltitude, const Coordinate& coord):
Pixel(coord),
m_radius(radius)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = relativeAltitude;
	m_symmetric = true;
}

PixelCircle::PixelCircle(const double radius, const double relativeAltitude, const Coordinate& coord, const Coordinate& satCoord):
Pixel(coord, satCoord),
m_radius(radius)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = relativeAltitude;
	m_symmetric = true;
	updateViewingAzimuthAngle();
	updateDistortion();
}

PixelCircle::PixelCircle(const double radius, const double relativeAltitude, const double time, const Coordinate& coord):
Pixel(time, coord),
m_radius(radius)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = relativeAltitude;
	m_symmetric = true;
}

PixelCircle::PixelCircle(const double radius, const double relativeAltitude, const double time, const Coordinate& coord, const Coordinate& satCoord):
Pixel(time, coord, satCoord),
m_radius(radius)
{
	m_scanMode = default_scanMode;
	m_relativeAltitude = relativeAltitude;
	m_symmetric = true;
	updateViewingAzimuthAngle();
	updateDistortion();
}

PixelCircle* PixelCircle::clone() const
{
	return new PixelCircle(*this);
}

// Méthodes
double PixelCircle::psf(const Coordinate& coord) const
{
	if (this->coord() == coord) return 1.;
	
	double dist, azi = 0.;
	dist = geo::map_2points(this->coord(), coord, &azi, geo::RADIAN);
	
	// si le point est en dehors du rayon
	if (dist > radius()*K())
	{
		return 0.;
	}
	
	std::complex<double> z = std::polar(dist, azi);
	rotate_and_distort(z);

	// si le point est en dehors du pixel
	if (std::abs(z) > radius())
	{
		return 0.;
	}

	return 1.;
}

double PixelCircle::radius() const
{
	return m_radius; // rayon en m au nadir
}

double PixelCircle::surface() const
{
	return radius()*K()*radius()*L()*M_PI;
}

// Constructeurs
PixelPinchCos::PixelPinchCos(const double radius, const double relativeAltitude):
PixelCircle(radius, relativeAltitude)
{
	m_scanMode = default_scanMode;
}

PixelPinchCos::PixelPinchCos(const double radius, const double relativeAltitude, const Coordinate& coord):
PixelCircle(radius, relativeAltitude, coord)
{
	m_scanMode = default_scanMode;
}

PixelPinchCos::PixelPinchCos(const double radius, const double relativeAltitude, const Coordinate& coord, const Coordinate& satCoord):
PixelCircle(radius, relativeAltitude, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

PixelPinchCos::PixelPinchCos(const double radius, const double relativeAltitude, const double time, const Coordinate& coord):
PixelCircle(radius, relativeAltitude, time, coord)
{
	m_scanMode = default_scanMode;
}

PixelPinchCos::PixelPinchCos(const double radius, const double relativeAltitude, const double time, const Coordinate& coord, const Coordinate& satCoord):
PixelCircle(radius, relativeAltitude, time, coord, satCoord)
{
	m_scanMode = default_scanMode;
}

PixelPinchCos* PixelPinchCos::clone() const
{
	return new PixelPinchCos(*this);
}

// Méthodes
double PixelPinchCos::psf(const Coordinate& coord) const
{
	if (this->coord() == coord) return 1.;
	
	double dist, azi;
	dist = geo::map_2points(this->coord(), coord, &azi, geo::RADIAN);
	
	// si le point est en dehors du rayon
	if (dist > radius()*K())
	{
		return 0.;
	}
	
	std::complex<double> z = std::polar(dist, azi);
	rotate_and_distort(z);
	
	const double r = std::abs(z);
	
	if (r > radius())
	{
		return 0.;
	}
	
	return (cos(M_PI*r/radius())+1.)/2.;
}

double PixelPinchCos::surface() const
{
	return PixelCircle::surface()/4.;
}
