#include <caldat/caldat.hpp>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <algorithm>
#include <stdexcept>
#include <limits>

Date::Date()
{
	*this = Date(std::time(NULL));
}

Date::Date(const time_t time)
{
	*this = Date(julday(1970, 1, 1) + (double)time/86400.);
}

Date::Date(const tm* ptm):
m_julday(julday(1900+ptm->tm_year, ptm->tm_mon, ptm->tm_mday, ptm->tm_hour, ptm->tm_min, ptm->tm_sec)),
m_year(1900+ptm->tm_year),
m_month(ptm->tm_mon),
m_day(ptm->tm_mday),
m_hour(ptm->tm_hour),
m_minute(ptm->tm_min),
m_second(ptm->tm_sec)
{
}

Date::Date(const double julday):
m_julday(julday)
{
	caldat(m_julday, &m_year, &m_month, &m_day, &m_hour, &m_minute, &m_second);
}

Date::Date(const int year, const int month, const int day, const int hour, const int minute, const double second):
m_julday(julday(year, month, day, hour, minute, second))
{
	caldat(m_julday, &m_year, &m_month, &m_day, &m_hour, &m_minute, &m_second);
}

const int& Date::year() const { return m_year; }

const int& Date::month() const { return m_month; }

const int& Date::day() const { return m_day; }

const int& Date::hour() const { return m_hour; }

const int& Date::minute() const { return m_minute; }

const double& Date::second() const { return m_second; }

const double& Date::julday() const { return m_julday; }

double Date::yday() const
{
	return (julday() - julday(year(),1,0));
}

double Date::dec_hour() const
{
	return fmod(julday()-.5, 1.) * 24.;
}

Date Date::local(const double lon, const double current) const
{
	double delta = lon - current;
	if (std::abs(delta) > 180.) delta -= 360*round(delta/360.);
	return julday()+(delta/15.)/24.;
}

time_t Date::time(time_t* time) const
{
	time_t result = round((julday()-julday(1970, 1, 1))*86400.);
	if (time != NULL) *time = result;
	return result;
}

void Date::caldat(double julian, int* year, int* month, int* day, int* hour, int* minute, double* second)
{
	const int min_julian = -1095;
	const int max_julian = 1827933925;
	const int igreg = 2299161;          // Beginning of Gregorian calendar
	
	if (julian < min_julian or julian > max_julian) throw std::domain_error("Value of Julian date is out of allowed range.");

	int jul = int(julian + 0.5);
	
	int ja;
	if (jul >= igreg) // Gregorian
	{ 
		int jalpha = int(((jul - 1867216) - 0.25) / 36524.25);
		ja = jul + 1 + jalpha - int(0.25 * jalpha);
	} else {
		ja = jul;
	}

	int jb = ja + 1524;
	int jc = int(6680. + ((jb-2439870)-122.1)/365.25);
	int jd = int(365. * jc + (0.25 * jc));
	int je = int((jb - jd) / 30.6001);
	
	if (day != NULL)
	{
		*day = jb - jd - int(30.6001 * je);
	}
	*month = je - 1;
	*month = (*month - 1)%12 + 1;
	
	*year = jc - 4715;
	if (*month > 2) *year -= 1;
	if (*year <= 0) *year -= 1;

// see if we need to do hours, minutes, seconds
	if (hour != NULL)
	{
		double fraction = julian + 0.5 - jul;
		double eps = 10.*std::max(std::numeric_limits<double>::epsilon(), std::numeric_limits<double>::epsilon()*abs(jul));
		*hour = std::min(std::max((int) floor(fraction * 24. + eps),0),23);
		if (minute != NULL)
		{
			fraction = fraction-*hour/24.;
			*minute = std::min(std::max((int) floor(fraction*1440. + eps),0),59);
			if (second != NULL)
			{
				*second = std::max((fraction - *minute/1440.)*86400, 0.);
			}
		}
	}
}

double Date::julday(int year, int month, int day, int hour, int minute, double second)
{
	const int min_calendar = -4716;
	const int max_calendar = 5000000;
	const int greg = 2299171;
	
	if (year < min_calendar or year > max_calendar) throw std::domain_error("Value of Julian date is out of allowed range.");
	
	if (year == 0) throw std::domain_error("There is no year zero in the civil calendar.");
	
	bool bc = year < 0;
	bool inJanFeb = month <= 2;
	
	int JY = year;
	if (bc) JY++;
	if (inJanFeb) JY--;
	int JM = month+1;
	if (inJanFeb) JM += 12;
	
	int JUL = int(floor(365.25 * JY)) + int(floor(30.6001*JM)) + day + 1720995;
	
	if (JUL >= greg)
	{
		int JA = (int) (0.01 * JY);
		JUL += 2 - JA + (int)(0.25 * JA);
	}
	// Add a small offset so we get the hours, minutes, & seconds back correctly
	// if we convert the Julian dates back. This offset is proportional to the
	// Julian date, so small dates (a long, long time ago) will be "more" accurate.
	double eps = std::max(std::numeric_limits<double>::epsilon(), std::numeric_limits<double>::epsilon()*abs(JUL));
	// For Hours, divide by 24, then subtract 0.5, in case we have unsigned ints.
	double julday = JUL + ( (hour/24. - 0.5) + minute/1440. + second/86400. + eps );
	
	return julday;
}
Date Date::operator+(const Date& date) const
{
	return Date(julday() + date.julday());
}
Date Date::operator-(const Date& date) const
{
	return Date(julday() - date.julday());
}
Date Date::operator+(const double day) const
{
	return Date(julday() + day);
}
Date Date::operator-(const double day) const
{
	return Date(julday() - day);
}
bool Date::operator==(const Date& date) const
{
	return julday() == date.julday();
}
bool Date::operator!=(const Date& date) const
{
	return julday() != date.julday();
}
bool Date::operator>(const Date& date) const
{
	return julday() > date.julday();
}
bool Date::operator<(const Date& date) const
{
	return julday() < date.julday();
}
bool Date::operator>=(const Date& date) const
{
	return julday() >= date.julday();
}
bool Date::operator<=(const Date& date) const
{
	return julday() <= date.julday();
}
Date& Date::operator+=(const Date& date)
{
	*this = *this + date;
	return *this;
}
Date& Date::operator-=(const Date& date)
{
	*this = *this - date;
	return *this;
}
Date& Date::operator+=(const double day)
{
	*this = *this + day;
	return *this;
}
Date& Date::operator-=(const double day)
{
	*this = *this - day;
	return *this;
}

Date::operator const double&() const
{
	return julday();
}

Date::operator time_t() const
{
	return time();
}

std::ostream& std::operator<<(std::ostream& out, const Date& date)
{
	std::ios::fmtflags flags = out.flags();
	out << std::fixed << std::setfill('0') << std::setprecision(3);
	out << date.year() << "-" << std::setw(2) << date.month() << "-" << std::setw(2) << date.day() << " " << std::setw(2) << date.hour() << ":" << std::setw(2) << date.minute() << ":" << std::setw(6) << date.second();
	out.flags(flags);
	return out;
}
