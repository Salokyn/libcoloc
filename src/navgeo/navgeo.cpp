#include <navgeo/navgeo.hpp>
#include <stdexcept>
#include <cmath>

namespace navgeo
{
	NavigationGeo::NavigationGeo(const long cfac, const long rfac, const long coff, const long roff, const double ssp, const bool rotated):
	m_cfac(cfac),
	m_rfac(rfac),
	m_coff(coff),
	m_roff(roff),
	m_ssp(ssp),
	m_rotated(rotated)
	{}
	
	void NavigationGeo::geo2pix(const Coordinate& coord, size_t& col, size_t& row) const
	{
		double cfac = m_cfac * NAVCONV;
		double rfac = m_rfac * NAVCONV;
		
		if (m_rotated)
		{
			cfac *= -1.;
			rfac *= -1.;
		}
		
		const double ssp = m_ssp/M_RADEG;
		const double lat = coord.lat(geo::RADIAN);
		const double lon = coord.lon(geo::RADIAN);
		const double coslt = cos(lat);
		const double sinlt = sin(lat);
		const double cosln = cos(lon-ssp);
		const double sinln = sin(lon-ssp);
		const double r2pol = geo::rpol_earth*geo::rpol_earth;
		const double r2eq  = geo::req_earth*geo::req_earth;
		
		const double re = geo::rpol_earth / sqrt(1. - (r2eq-r2pol)/r2eq * coslt*coslt);
		const double r1 = geoAltitude - re*coslt*cosln;
		const double r2 = -re*coslt*sinln;
		const double r3 = re*sinlt;
		const double rn = sqrt(r1*r1 + r2*r2 + r3*r3);
		
		const double dotprod = r1*(re*coslt*cosln) - r2*r2 - r3*r3*r2eq/r2pol;
		
		if (dotprod <= 0 )
		{
			throw std::out_of_range("Point is out of sight.");
		}
		
		const double x = atan(-r2/r1);
		const double y = asin(-r3/rn);
		
		col =  m_coff + (size_t)round(x * cfac);
		row =  m_roff + (size_t)round(y * rfac);
	}
	
	Coordinate NavigationGeo::pix2geo(const int col, const int row) const
	{
		double cfac = m_cfac * NAVCONV;
		double rfac = m_rfac * NAVCONV;
		
		if (m_rotated)
		{
			cfac *= -1.;
			rfac *= -1.;
		}
		
		const double x = (col - m_coff) / cfac;
		const double y = (row - m_roff) / rfac;
		
		const double ssp = m_ssp/M_RADEG;
		const double cosx = cos(x);
		const double sinx = sin(x);
		const double cosy = cos(y);
		const double siny = sin(y);
		const double r2pol = geo::rpol_earth*geo::rpol_earth;
		const double r2eq  = geo::req_earth*geo::req_earth;
		const double q2 = r2eq/r2pol;
		const double d2 = geoAltitude*geoAltitude-r2eq;
		
		const double a = geoAltitude*cosx*cosy;
		const double b = cosy*cosy + q2*siny*siny;
		
		const double sa = a*a - b*d2;
		
		if (sa <= 0.)
		{
			throw std::out_of_range("Point is out of sight.");
		}
		
		const double sd = sqrt(sa);
		
		const double sn = (a-sd)/b;
		
		const double s1 = geoAltitude - sn * cosx * cosy;
		const double s2 = sn*sinx*cosy;
		const double s3 = -sn * siny;
		
		const double sxy = sqrt(s1*s1 + s2*s2);
		
		const double lon = atan(s2/s1) + ssp;
		const double lat = atan(q2*s3/sxy);
		
		return Coordinate(lon, lat, geo::RADIAN);
	}
	
	double NavigationGeo::ssp(const geo::unit_angle u) const
	{
		if (u == geo::RADIAN) return m_ssp/M_RADEG;
		return m_ssp;
	}
	
	void NavigationGeo::setSsp(double ssp, const geo::unit_angle u)
	{
		if (u == geo::RADIAN) ssp *= M_RADEG;
		m_ssp = ssp;
	}
	
	SEVIRI::SEVIRI(const double ssp, bool rotated):NavigationGeo(default_cfac, default_rfac, default_coff, default_roff, ssp, rotated)
	{}
	
	GOES::GOES(const double ssp, bool rotated):NavigationGeo(default_cfac, default_rfac, default_coff, default_roff, ssp, rotated)
	{}
	
	GOES10::GOES10(const bool rotated):GOES(default_ssp, rotated)
	{}
	
	GOES11::GOES11(const bool rotated):GOES(default_ssp, rotated)
	{}
	
	GOES12::GOES12(const bool rotated):GOES(default_ssp, rotated)
	{}
	
	MTSAT::MTSAT(bool rotated):NavigationGeo(default_cfac, default_rfac, default_coff, default_roff, default_ssp, rotated)
	{}
	
	MVIRI::MVIRI(const double ssp, bool rotated):NavigationGeo(default_cfac, default_rfac, default_coff, default_roff, ssp, rotated)
	{}
	
	FY2C::FY2C(bool rotated):NavigationGeo(default_cfac, default_rfac, default_coff, default_roff, default_ssp, rotated)
	{}
}
