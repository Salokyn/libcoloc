#include <ixionlite/legendre.hpp>
#include <stdexcept>

namespace Legendre
{
	double P(int n, double x)
	{
		switch(n)
		{
			case 0:
			return P0(x);
			break;
			
			case 1:
			return P1(x);
			break;
			
			case 2:
			return P2(x);
			break;
			
			case 3:
			return P3(x);
			break;
			
			case 4:
			return P4(x);
			break;
			
			case 5:
			return P5(x);
			break;
			
			default:
			throw std::range_error("double P(int n, double x) : Degree not definded.");
		}
	}
	
	double P0(double x)
	{
		return 1.;
	}
	
	double P1(double x)
	{
		return x;
	}
	
	double P2(double x)
	{
		return (3.*x*x-1)/2.;
	}
	
	double P3(double x)
	{
		return (5.*x*x*x-3.*x)/2.;
	}
	
	double P4(double x)
	{
		return (35.*x*x*x*x-30.*x*x+3.)/8.;
	}
	
	double P5(double x)
	{
		return (63.*x*x*x*x*x-70.*x*x*x+15.*x)/8.;
	}
}
