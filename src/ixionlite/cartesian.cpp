#include <ixionlite/cartesian.hpp>
#include <cmath>
#include <geo/geo/geo.hpp>

CartesianCoordinate::CartesianCoordinate(double xin, double yin, double zin):x(xin),y(yin),z(zin)
{}

CartesianCoordinate::operator Coordinate() const
{
	double r = this->r();
	
	double lat = asin(z/r);
	double lon = copysign(acos(x/(r*cos(lat))), y);
	
	return Coordinate(lon, lat, geo::RADIAN);
}

CartesianCoordinate::CartesianCoordinate(const Coordinate& coord, double rin)
{
	double lon = coord.lon(geo::RADIAN);
	double lat = coord.lat(geo::RADIAN);
	
	x = rin*cos(lat)*cos(lon);
	y = rin*cos(lat)*sin(lon);
	z = rin*sin(lat);
}

CartesianCoordinate& CartesianCoordinate::fromEuler(double E1, double E2, double E3, double rin)
{
	x = rin * (cos(E1)*cos(E3)-sin(E1)*sin(E3)*cos(E2));
	y = rin * (sin(E1)*cos(E3)+cos(E1)*sin(E3)*cos(E2));
	z = rin * (sin(E3)*sin(E2));
	
	return *this;
}

double CartesianCoordinate::r() const
{
	return sqrt(x*x+y*y+z*z);
}

CartesianCoordinate CartesianCoordinate::operator*(double a) const
{
	return CartesianCoordinate(a*x, a*y, a*z);
}

CartesianCoordinate& CartesianCoordinate::operator*=(double a)
{
	x *= a;
	y *= a;
	z *= a;
	return *this;
}

CartesianCoordinate CartesianCoordinate::operator/(double a) const
{
	return CartesianCoordinate(a/x, a/y, a/z);
}

CartesianCoordinate& CartesianCoordinate::operator/=(double a)
{
	x /= a;
	y /= a;
	z /= a;
	return *this;
}

CartesianCoordinate CartesianCoordinate::operator+(const CartesianCoordinate& c) const
{
	return CartesianCoordinate(x+c.x, y+c.y, z+c.z);
}

CartesianCoordinate& CartesianCoordinate::operator+=(const CartesianCoordinate& c)
{
	x += c.x;
	y += c.y;
	z += c.z;
	return *this;
}

CartesianCoordinate CartesianCoordinate::operator-(const CartesianCoordinate& c) const
{
	return CartesianCoordinate(x-c.x, y-c.y, z-c.z);
}

CartesianCoordinate& CartesianCoordinate::operator-=(const CartesianCoordinate& c)
{
	x -= c.x;
	y -= c.y;
	z -= c.z;
	return *this;
}
