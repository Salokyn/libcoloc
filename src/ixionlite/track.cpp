#include <ixionlite/track.hpp>

TrackElement::TrackElement():
time(0.),
coord(Coordinate(0.,0.)),
trackInclination(0.)
{
}

TrackElement::TrackElement(const double t, const Coordinate& c, const double i):
time(t),
coord(c),
trackInclination(i)
{
}

TrackElement::TrackElement(const Pixel& p):
time(p.time()),
coord(p.coord())
{
	try
	{
		trackInclination = p.azimuthAngle();
	}
	catch(const std::exception& e)
	{
		trackInclination = NAN;
	}
}

bool TrackElement::less(const TrackElement& a, const TrackElement& b)
{
	return a.time < b.time;
}
