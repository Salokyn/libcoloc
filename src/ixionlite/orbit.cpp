#include <ixionlite/orbit.hpp>
#include <ixionlite/legendre.hpp>
#include <cmath>
#include <iostream>

OrbitGenerator::OrbitGenerator(const Planet& planet):
m_planet(planet),
m_radius(m_planet.equatorialRadius()),
m_inclination(0.),
m_eccentricity(0.)
{
}

OrbitGenerator::OrbitGenerator(const double radius, const double inclination, const double eccentricity, const Planet& planet):
m_planet(planet),
m_radius(radius),
m_inclination(inclination),
m_eccentricity(eccentricity)
{
}

OrbitGenerator::OrbitGenerator(const Track& track, const double inclination, const Planet& planet):
m_planet(planet)
{
	if (!isnan(inclination))
	{
		setInclination(inclination);
	} else {
		double max = std::abs(track[0].coord.lat(geo::RADIAN));
		for (size_t i=1; i < track.size(); i++)
		{
			double lat = std::abs(track[i].coord.lat(geo::RADIAN));
			
			if (lat > max)
			{
				max = lat;
			}
		}
		
		double inclination =  M_PI/2.-copysign(M_PI/2.-max, geo::to_180_180(track[1].coord.lon()-track[0].coord.lon())/(track[1].time-track[0].time));
		setInclination(inclination);
	}
	
	// Recherche d'un passage à l'équateur
	size_t i=1;
	for (i=1; i < track.size(); i++)
	{
		if (track[i].coord.lat()*track[i-1].coord.lat() <= 0.) break;
	}
	
	double xA = track[i-1].coord.lon();
	double xB = track[i].coord.lon();
	double yA = track[i-1].coord.lat();
	double yB = track[i].coord.lat();
	double tA = track[i-1].time;
	double tB = track[i].time;
	
	double lonAN = (xB*yA-xA*yB)/(yA-yB);
	double timeAN = (tB*yA-tA*yB)/(yA-yB);
	
	// Recherche de l'altitude
	i++;
	double n = asin(sin(track[i].coord.lat(geo::RADIAN))/sin(this->inclination()))/((track[i].time-timeAN)*86400.);
	double a = pow(this->planet().geocentricGravitationalConstant(),1./3.)/pow(n,2./3.);
	setRadius(a);
}

const double& OrbitGenerator::inclination() const
{
	return m_inclination;
}

const double& OrbitGenerator::radius() const
{
	return m_radius;
}

const double& OrbitGenerator::eccentricity() const
{
	return m_eccentricity;
}

double OrbitGenerator::relativeRadius() const
{
	return m_radius/planet().equatorialRadius();
}

const Planet& OrbitGenerator::planet() const
{
	return m_planet;
}

double OrbitGenerator::keplerianMeanMotion() const
{
	return sqrt( planet().geocentricGravitationalConstant()/pow(radius(),3.) );
}

double OrbitGenerator::meanMotionVariation() const
{
	return 3./4.*planet().J2() * pow(relativeRadius(),-2.)*(3.*cos(inclination())*cos(inclination())-1.);
}

double OrbitGenerator::trueMeanMotion() const
{
	return keplerianMeanMotion()*(1.+meanMotionVariation());
}

double OrbitGenerator::K0() const
{
	return 3./2.*planet().J2()*sqrt( planet().geocentricGravitationalConstant()/pow(planet().equatorialRadius(),3.) );
}

double OrbitGenerator::apsidalPrecessionRate() const
{
	const double cosi = cos(inclination());
	return 1./2. * K0() * pow(relativeRadius(),-7./2.) * (5.*cosi*cosi-1.);
}

double OrbitGenerator::nodalPrecessionRate() const
{
	return -K0()*pow(relativeRadius(),-7./2.)*cos(inclination());
}

double OrbitGenerator::keplerianPeriod() const
{
	return 2.*M_PI/keplerianMeanMotion();
}

double OrbitGenerator::anomalisticPeriod() const
{
	return 2.*M_PI/trueMeanMotion();
}

double OrbitGenerator::nodalPeriod() const
{
	return 2.*M_PI/(apsidalPrecessionRate()+trueMeanMotion());
}

double OrbitGenerator::dailyReccurenceFrequency() const
{
	return trueMeanMotion()/(planet().rotationSpeed()-nodalPrecessionRate());
}

double OrbitGenerator::dailyOrbitalFrequency() const
{
	return trueMeanMotion()*planet().meanDay()/(2.*M_PI);
}

double OrbitGenerator::potential(double latitude, const geo::unit_angle u) const
{
	if (u == geo::DEGREE) latitude /= M_RADEG;
	return planet().geocentricGravitationalConstant()/radius()*(1.-pow(relativeRadius(),-2.)*planet().J2()*Legendre::P2(sin(latitude)));
}

double OrbitGenerator::satelliteDistance(const double anomaly) const
{
	return radius()*(1.-eccentricity()*eccentricity())/(1.+eccentricity()*cos(anomaly));
}

double OrbitGenerator::apparentInclination() const
{
	return atan(sin(inclination())/(cos(inclination()) - 1./dailyReccurenceFrequency()));
}

double OrbitGenerator::trackInclination(double time, double timeAN) const
{
	const double n = trueMeanMotion();
	double E3 = n * (time-timeAN)*86400.;
	double Z = sin(E3)*sin(inclination());
	double latitude = asin(Z);
	double dZ = n*cos(E3)*sin(inclination());
	
	double i = -M_PI/2.+copysign(acos(cos(inclination())/cos(latitude)), dZ);
	i -= 2.*M_PI*round(i/(2.*M_PI)); // in [-pi;pi]
	return i;
}

double OrbitGenerator::dZ(double time, double timeAN) const
{
	const double n = trueMeanMotion();
	return radius()*sin(inclination())*n*cos(n*(time-timeAN)*86400.);
}

bool OrbitGenerator::ascending(double time, double timeAN) const
{
	return dZ(time, timeAN) > 0.;
}

void OrbitGenerator::setRadius(const double radius)
{
	m_radius = radius;
}

void OrbitGenerator::setAltitude(const double altitude)
{
	m_radius = altitude+planet().equatorialRadius();
}

void OrbitGenerator::setInclination(double inclination, const geo::unit_angle u)
{
	if (u == geo::DEGREE) inclination /= M_RADEG;
	m_inclination = inclination;
}

void OrbitGenerator::setEccentricity(const double eccentricity)
{
	m_eccentricity = eccentricity;
}

Track OrbitGenerator::getTrack(double startTime, double endTime, double resolution, double lonAN, double timeAN) const
{
	const double dOmega = nodalPrecessionRate();
	const double dOmegaT = planet().rotationSpeed();
	const double n = trueMeanMotion();
	
	const size_t size = size_t(std::abs((endTime-startTime)/(resolution/86400.)));
	Track out(size);
	
	const double E2 = inclination(); // θ=i
	
	#pragma omp parallel for schedule(dynamic)
	for (size_t i = 0; i < size; i++)
	{
		double time = startTime+i*resolution/86400.;
		
		double E1 = lonAN + (dOmega - dOmegaT) * (time-timeAN)*86400.; // ψ=Ω
		double E3 = n * (time-timeAN)*86400.; // χ=ω+υ
		
		CartesianCoordinate xyz;
		xyz.fromEuler(E1, E2, E3);
		
		double inc = trackInclination(time, timeAN);
		
		TrackElement element;
		element.time = time;
		element.coord = xyz;
		element.trackInclination = inc;
		
		out[i] = element;
	}
	
	return out;
}

CartesianCoordinate OrbitGenerator::getCartesianCoordinate(double time, double lonAN, double timeAN) const
{
	const double dOmega = nodalPrecessionRate();
	const double dOmegaT = planet().rotationSpeed();
	const double n = trueMeanMotion();
	const double r = radius();
	
	double E1 = lonAN + (dOmega - dOmegaT) * (time-timeAN)*86400.; // ψ=Ω
	const double E2 = inclination(); // θ=i
	double E3 = n * (time-timeAN)*86400.; // χ=ω+υ
	
	CartesianCoordinate out;
	
	return out.fromEuler(E1, E2, E3, r);
}

Coordinate OrbitGenerator::getCoordinate(double time, double lonAN, double timeAN) const
{
	return getCartesianCoordinate(time, lonAN, timeAN);
}

