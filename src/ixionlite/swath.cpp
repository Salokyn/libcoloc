#include <ixionlite/swath.hpp>
#include <stdexcept>

SwathGenerator::SwathGenerator(const Pixel& pixel):
m_pixel(&pixel),
m_size(2)
{
	try
	{
		setInclination(pixel.viewingAzimuthAngle(geo::RADIAN), geo::RADIAN);
	}
	catch(const std::exception& e)
	{}
	setSwathAngle(pixel.swathAngle(geo::RADIAN), geo::RADIAN);
}

SwathGenerator::~SwathGenerator()
{}

void SwathGenerator::setSize(const size_t i)
{
	m_size = i;
}

void SwathGenerator::setInclination(double i, const geo::unit_angle u)
{
	if (u == geo::DEGREE) i /= M_RADEG;
	m_inclination = i;
}

void SwathGenerator::setViewingZenithAngle(double z, const geo::unit_angle u)
{
	if (u == geo::DEGREE) z /= M_RADEG;
	m_swathAngle = asin(1./m_pixel->relativeAltitude() * sin(z));
}

void SwathGenerator::setSwathAngle(double f, const geo::unit_angle u)
{
	if (u == geo::DEGREE) f /= M_RADEG;
	m_swathAngle = f;
}

SwathGenerator::operator Orbit() const
{
	return get();
}

PlanarSwathGenerator::PlanarSwathGenerator(const Pixel& pixel):
SwathGenerator(pixel)
{}

PlanarSwathGenerator::~PlanarSwathGenerator()
{}

Orbit PlanarSwathGenerator::get() const
{
	if (m_size < 2) throw std::length_error("Orbit PlanarSwathGenerator::get() const: The Swath must contain at least 2 Pixels");
	
	Orbit out(m_size);
	
	const double delta = 2.*m_swathAngle/(m_size-1);
	const Coordinate satCoord = m_pixel->satCoord();
	
	#pragma omp parallel for schedule(dynamic)
	for (size_t i = 0; i < m_size; i++)
	{
		double currentSwathAngle = -m_swathAngle+i*delta;
		double currentAlpha = -currentSwathAngle + asin(m_pixel->relativeAltitude() * sin(currentSwathAngle));
		
		Coordinate coord = geo::equidistantCircle(satCoord, currentAlpha, m_inclination, geo::RADIAN); // ATTENTION AU - DEVANT L'ANGLE
		
		out[i] = m_pixel->clone();
		out[i]->setCoord(coord, satCoord);
		out[i]->setAzimuthAngle(m_inclination, geo::RADIAN);
	}
	
	return out;
}

ConicalSwathGenerator::ConicalSwathGenerator(const Pixel& pixel):
SwathGenerator(pixel),
m_halfApertureAngle(0.)
{}

ConicalSwathGenerator::~ConicalSwathGenerator()
{}

void ConicalSwathGenerator::setHalfApertureAngle(double a, const geo::unit_angle u)
{
	if (u == geo::DEGREE) a /= M_RADEG;
	m_halfApertureAngle = a;
}

Orbit ConicalSwathGenerator::get() const
{
	if (m_size < 2) throw std::length_error("Orbit ConicalSwathGenerator::get() const: The Swath must contain at least 2 Pixels");
	
	Orbit out(m_size);
	
	const double delta = 2.*m_halfApertureAngle/(m_size-1);
	const double alpha = -m_swathAngle + asin(m_pixel->relativeAltitude() * sin(m_swathAngle));
	const Coordinate satCoord = m_pixel->satCoord();
	
	#pragma omp parallel for schedule(dynamic)
	for (size_t i = 0; i < m_size; i++)
	{
		double currentAzimuthAngle = m_inclination-m_halfApertureAngle+i*delta-M_PI/2.;
		
		Coordinate coord = geo::equidistantCircle(satCoord, alpha, currentAzimuthAngle, geo::RADIAN); // ATTENTION AU - DEVANT L'ANGLE
		
		out[i] = m_pixel->clone();
		out[i]->setCoord(coord, satCoord);
		out[i]->setAzimuthAngle(currentAzimuthAngle, geo::RADIAN);
	}
	
	return out;
}

