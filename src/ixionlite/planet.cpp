#include <ixionlite/planet.hpp>
#include <cmath>
#include <ixionlite/constants.hpp>

Planet::Planet()
{}

const double& Planet::equatorialRadius() const
{
	return m_equatorialRadius;
}

const double& Planet::flattening() const
{
	return m_flattening;
}

const double& Planet::meanDay() const
{
	return m_meanDay;
}

const double& Planet::sideralYear() const
{
	return m_sideralYear;
}

const double& Planet::mass() const
{
	return m_mass;
}

double Planet::geocentricGravitationalConstant() const
{
	return AstronomicalConstants::G*mass();
}

double Planet::polarRadius() const
{
	return (1.-flattening())*equatorialRadius();
}

double Planet::revolutionSpeed() const
{
	return 2.*M_PI/(meanDay()*sideralYear());
}

double Planet::rotationSpeed() const
{
	return 2.*M_PI/meanDay() * (sideralYear()+1.)/sideralYear();
}

double Planet::eccentricity() const
{
	return sqrt(1.-pow(1.-flattening(),2.));
}

const double& Planet::J2() const { return m_J2; }
const double& Planet::J3() const { return m_J3; }
const double& Planet::J4() const { return m_J4; }

Earth::Earth()
{
	m_equatorialRadius = default_equatorialRadius;
	m_flattening = default_flattening;
	m_meanDay = default_meanDay;
	m_sideralYear = default_sideralYear;
	m_mass = default_mass;
	
	m_J2 = default_J2;
	m_J3 = default_J3;
	m_J4 = default_J4;
}

Mars::Mars()
{
	m_equatorialRadius = default_equatorialRadius;
	m_flattening = default_flattening;
	m_meanDay = default_meanDay;
	m_sideralYear = default_sideralYear;
	m_mass = default_mass;
	
	m_J2 = default_J2;
	m_J3 = default_J3;
	m_J4 = default_J4;
}
